---
layout: default
title:  Product profiles
parent: Product administration
grand_parent: Using Entgra IoT Server
nav_order: 4
---
# Product profiles

When a Entgra product starts, it starts all components, features and related artifacts bundled with it. The main functionality of Entgra IoT Server can be divided into six different profiles that each contain a bundle of specific components, features, and artifacts related to the profile. This section guides you on how to create a profile distribution in order to support a distributed setup. 

By default, each profile is started on a different port number. The following table lists out the profiles available in the Entgra IoT Server and their respective port numbers.

<table>
  <colgroup>
    <col>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Profile</th>
      <th>Port Numbers</th>
      <th>Description</th>
    </tr>
    <tr>
      <td>Device Manager Backend</td>
      <td>9444/9763</td>
      <td>This profile consists of the core services and device types that are used by the WSO2 IoT core framework and handles external traffic for the backend services. The Device Manager Backend profile does not consist of any Graphical User Interface (GUI) applications that need to be used directly by the end user.</td>
    </tr>
    <tr>
      <td>Device Manager</td>
      <td>9443/9763</td>
      <td>
        <p>This profile mainly consists of the GUI for all applications that facilitate device management. These applications include the following:</p>
        <ul>
          <li>Device management application</li>
          <li>Application manager</li>
          <li>Publisher and store</li>
          <li>API store</li>
          <li>Dashboard Portal.</li>
        </ul>
        <p>Additionally, there are some exposed REST APIs for the publisher and store in WSO2 App Manager and WSO2 API Manager. These APIs can be called remotely and some actions, which can also be performed through the GUI applications, can be performed through the API itself.</p>
      </td>
    </tr>
    <tr>
      <td>Key Manager</td>
      <td>9447/9767</td>
      <td>
        <p>This profile acts as both a key manager, and an identity provider. The user operations done in the Device Manager profile are authenticated via this profile.</p>
      </td>
    </tr>
    <tr>
      <td>IoT Analytics</td>
      <td>9445/9764</td>
      <td>This profile receives various device information and analytics (real-time and batch analytics) on the events that are received. The events can be published to the analytics server directly or it can be pushed via a broker profile.</td>
    </tr>
    <tr>
      <td>Broker</td>
      <td>1886</td>
      <td>This profile acts as the MQTT broker that can be published and subscribed to from profiles such as the device backend profile and the analytics profile. &nbsp;</td>
    </tr>
  </tbody>
</table>

### Creating profile distributions

The distributions for the profiles are created by executing a script from the WSO2 IoT distribution. Follow the steps given below to create the respective profiles:

1.  [Download Entgra IoT Server](http://wso2.com/products/iot-server/). 
2.  Unzip the Entgra IoT Server distribution in any preferred location. This location is referred to as `<IOTS_HOME>` throughout this document. 
3.  Navigate to the `<IOTS_HOME>/bin` directory on a command prompt or terminal window, and execute the profile creator script. 

4.  Enter the profile number of the profile you want to create.   
    You can either enter the profile numbers separately to create multiple profiles or select the ‘All Profiles’ option (number 6) to create all the profiles in one go.  
    ![]({{site.baseurl}}/assets/images/352819946.png)

5.  Once the profile number is entered, the profiles archive is created in the `target` directory. The `target` directory resides in the same directory as `<IOTS_HOME>` . Copy the respective archives into the preferred machines and locations.



Multi-profiling



**Tip:** A particular profile contains only a subset of features bundled in the product. In order to identify what feature bundles apply to which profile, the product maintains a set of [`bundles.info`](http://bundles.info/) files. In Entgra IoT Server, these files can be found in the `<IOTS_HOME>/wso2/components/**<profile-name>**/configuration/org.eclipse.equinox.simpleconfigurator` directories. The [`bundles.info`](http://bundles.info/) files contain references to the actual bundles.

Note that the `<profile-name>` in the directory path refers to the name of the profile. For example, when there's a product profile named `device-backend`, references to all the feature bundles required for the device-backend profile to function, are in a [`bundles.info`](http://bundles.info/) file saved in the `<IOTS_HOME>/wso2/components/device-backend/configuration/org.eclipse.equinox.simpleconfigurator` directory.



