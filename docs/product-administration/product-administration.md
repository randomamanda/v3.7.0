---
layout: default
title: Product administration
parent: Using Entgra IoT Server
has_children: true
nav_order: 10
---
# Product administration


Entgra Internet of Things Server (Entgra IoT Server) is shipped with default configurations that will allow you to download, install and get started with your product instantly. However, when you go into production, it is recommended to change some of the default settings to ensure that you have a robust system that is suitable for your operational needs. Also, you may have specific use cases that require specific configurations to the server. **[Entgra Enterprise Mobility (Entgra EMM) capabilities are bundled with Entgra IoT Server](https://wso2.com/solutions/enterprise-mobility/enterprise-mobility-management/) **too.

If you are a product administrator, the following content will provide an overview of the administrative tasks that you need to perform when working with Entgra Internet of Things Server (Entgra IoT Server).

* * *

## Clustering Entgra IoT Server

For information on how to set up a Entgra IoT Server worker/manager separated cluster and how to configure a cluster with a third-party load balancer, see the [Clustering Guide](/doc/en/lb2/Clustering-Entgra-IoT-Server.html).

* * *

## Changing the default database

By default, Entgra products are shipped with an embedded H2 database, which is used for storing user management and registry data. We recommend that you use an industry-standard RDBMS such as Oracle, PostgreSQL, MySQL, MS SQL, etc. when you set up your production environment.  You can change the default database configuration by simply setting up a new physical database and updating the configurations in the product server to connect to that database. 

For instructions on setting up and configuring databases, see [Working with Databases](/doc/en/lb2/Working-with-Databases.html) in the Entgra IoT Server Guide.

* * *

## General Data Protection Regulation

Follow the steps given in [General Data Protection Regulation for Entgra IoT Server](/doc/en/lb2/General-Data-Protection-Regulation-for-WSO2-IoT-Server.html) to be compliant with the General Data Protection Regulation (GDPR).

* * *

## Configuring users, roles, and permissions

The user management feature in your product allows you to create new users and define the permissions granted to each user. You can also configure the user stores that are used for storing data related to user management.

*   For instructions on how to configure users, roles and permissions, see [User Management](/doc/en/lb2/User-Management.html) in the Entgra IoT Server Guide. 
*   For instructions on how to configure the user realm and user stores, see the following topics in the WSO2 Product Administration Guide:

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th><a class="unresolved" href="#">Configuring the System Administrator</a></th>
      <td>The&nbsp;<strong>admin</strong>&nbsp;user is the super tenant that will be able to manage all other users, roles and permissions in the system by using the management console of the product.&nbsp;Therefore, the user that should have admin permissions is required to be stored in the primary user store when you start the system for the first time. The documentation on setting up primary user stores will explain how to configure the administrator while configuring the user store. The information under this topic will explain the main configurations that are relevant to setting up the system administrator.</td>
    </tr>
    <tr>
      <th><a class="unresolved" href="#">Configuring the Authorization Manager</a></th>
      <td>According to the default configuration in Entgra products, the users, roles and permissions are stored in the same repository (i.e., the default, embedded H2 database). However, you can change this configuration in such a way that the users and roles are stored in one repository (user store) and the permissions are stored in a separate repository. A user store can be a typical RDBMS, an LDAP or an external Active Directory.&nbsp;<br>
        <p>The repository that stores permissions should always be an RDBMS. The Authorization Manager configuration in the user-mgt.xml file (stored in the&nbsp;<code>&lt;PRODUCT_HOME&gt;/conf/</code>&nbsp;directory) connects the system to this RDBMS. The information under this topic will instruct you through setting up and configuring the Authorization Manager. &nbsp;</p>
      </td>
    </tr>
    <tr>
      <th><a class="unresolved" href="#">Configuring User Stores</a></th>
      <td>
        <p>The user management feature in Entgra products allows you to maintain multiple user stores for your system that are used to store the users and their roles. The following topics guide you through configuring the user stores:</p>
        <ul>
          <li><a class="unresolved" href="#">Configuring the Primary User Store</a>
            <ul>
              <li><a href="https://docs.wso2.com/display/ADMIN44x/Configuring+a+JDBC+User+Store" class="external-link" rel="nofollow">Configuring a JDBC User Store</a></li>
              <li><a href="https://docs.wso2.com/display/ADMIN44x/Configuring+a+Read-Only+LDAP+User+Store" class="external-link" rel="nofollow">Configuring a Read-Only LDAP User Store</a></li>
              <li><a href="https://docs.wso2.com/display/ADMIN44x/Configuring+a+Read-Write+Active+Directory+User+Store" class="external-link" rel="nofollow">Configuring a Read-Write Active Directory User Store</a></li>
              <li><a href="https://docs.wso2.com/display/ADMIN44x/Configuring+a+Read-Write+LDAP+User+Store" class="external-link" rel="nofollow">Configuring a Read-Write LDAP User Store</a></li>
            </ul>
          </li>
          <li><a class="unresolved" href="#">Configuring Secondary User Stores</a></li>
          <li><a class="unresolved" href="#">Working with Properties of User Stores</a></li>
          <li><a class="unresolved" href="#">Writing a Custom User Store Manager</a></li>
        </ul>
      </td>
    </tr>
    <tr>
      <th><a href="https://docs.wso2.com/display/ADMIN44x/Configuring+Users" class="external-link" rel="nofollow">Configuring Users</a></th>
      <td><span>To enable users to log into the management console, you create user accounts and assign them roles, which are sets of permissions. You can add individual users or import users in bulk.</span></td>
    </tr>
  </tbody>
</table>

* * *

## Configuring security

After you install Entgra IoT Server, it is recommended to change the default security settings according to the requirements of your production environment. As Entgra IoT Server is built on top of the WSO2 Carbon Kernel, the main security configurations applicable to IoT Server are inherited from the Carbon kernel.

For instructions on configuring security on your server, see the following topics in the WSO2 Product Administration Guide.

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th width="20%">
        <p><strong><a class="unresolved" href="#">Configuring Transport-Level Security</a></strong></p>
      </th>
      <td>
        <p>The transport level security protocol of the Tomcat server is configured in the&nbsp;<code>&lt;PRODUCT_HOME&gt;/conf/tomcat/catalina-server.xml</code>&nbsp;file. Note that the ss<code>Lprotocol</code>&nbsp;attribute is set to "TLS" by default.&nbsp;<br>The following topics will guide you through the configuration options:</p>
        <ul>
          <li><a class="external-link" href="https://docs.wso2.com/display/ADMIN44x/Configuring+Transport+Level+Security#ConfiguringTransportLevelSecurity-DisablingSSL" rel="nofollow">Disabling SSL&nbsp;</a></li>
          <li><a class="external-link" href="https://docs.wso2.com/display/ADMIN44x/Configuring+Transport+Level+Security#ConfiguringTransportLevelSecurity-Disablingweakciphers" rel="nofollow">Disabling weak ciphers</a></li>
          <li><a class="external-link" href="https://docs.wso2.com/display/ADMIN44x/Configuring+Transport+Level+Security#ConfiguringTransportLevelSecurity-ConfiguringthePassThroughtransport" rel="nofollow">Configuring the&nbsp;PassThrough transport</a></li>
        </ul>
      </td>
    </tr>
    <tr>
      <th>
        <p><a class="unresolved" href="#">Using Asymmetric Encryption</a></p>
      </th>
      <td>
        <div class="content-wrapper">
          <p style="text-align: justify;">Entgra products use asymmetric encryption by default for the purposes of authentication and data encryption.&nbsp;In asymmetric encryption, keystores (with key pairs and certificates) are created and stored for the product. It is possible to have multiple keystores so that the keys used for different use cases are kept unique.&nbsp;The following topics explain more details on keystores&nbsp;and how you can create a new key store and configure it with Entgra IoT Server.</p>
          <div class="toc-macro client-side-toc-macro hidden-outline">
            <ul>
              <li><a href="https://docs.wso2.com/display/ADMIN44x/Creating+New+Keystores" class="external-link" rel="nofollow">Creating New Keystores</a></li>
              <li><a href="https://docs.wso2.com/display/ADMIN44x/Configuring+Keystores+in+WSO2+Products" class="external-link" rel="nofollow">Configuring Keystores in Entgra products</a></li>
              <li><a href="https://docs.wso2.com/display/ADMIN44x/Managing+Keystores+with+the+UI" class="external-link" rel="nofollow">Managing Keystores with the UI<br></a></li>
            </ul>
            <div class="confluence-information-macro confluence-information-macro-note"><span class="aui-icon aui-icon-small aui-iconfont-warning confluence-information-macro-icon"></span>
              <div class="confluence-information-macro-body">
                <p>Additionally, to the common configurations listed under <a href="https://docs.wso2.com/display/ADMIN44x/Configuring+Keystores+in+WSO2+Products" class="external-link" rel="nofollow">Configuring Keystores in Entgra products</a>, you need to configure Entgra IoT Server if you changed the keystore&nbsp;from the default WSO2 keystore and&nbsp;<a href="https://docs.wso2.com/display/IoTS300/Configuring+WSO2+IoT+Server+with+the+IP" class="external-link" rel="nofollow">changed the default WSO2 IP</a>.<br><br>For more information, see <a href="/doc/en/lb2/Configuring-Keystores-in-Entgra-IoT-Server.html" data-linked-resource-id="352821730" data-linked-resource-version="1" data-linked-resource-type="page">Configuring Keystores in Entgra IoT Server</a>.</p>
              </div>
            </div>
          </div>
        </div>
      </td>
    </tr>
    <tr>
      <th>
        <p><a class="unresolved" href="#">Using Symmetric Encryption</a></p>
      </th>
      <td>WSO2 Carbon-based products use&nbsp;<a href="https://docs.wso2.com/display/ADMIN44x/Using+Asymmetric+Encryption" class="external-link" rel="nofollow">asymmetric encryption</a>&nbsp;by default as explained in the previous section. From Carbon 4.4.3 onwards, you have the option of switching to symmetric encryption in your WSO2 product. Using symmetric encryption means that a single key will be shared for encryption and decryption of information.&nbsp;</td>
    </tr>
    <tr>
      <th>
        <p><a class="unresolved" href="#">Enabling Java Security Manager</a></p>
      </th>
      <td>The Java&nbsp;Security Manager is used to define various security policies that&nbsp;prevent untrusted code from manipulating your system.&nbsp;&nbsp;Enabling the Java Security Manager for Entgra products activates the Java permissions that are in the&nbsp;<code>&lt;PRODUCT_HOME&gt;/core/repository/conf/sec.policy</code>&nbsp;file. You modify this file to change the Java security permissions as required.</td>
    </tr>
    <tr>
      <th>
        <p><a class="unresolved" href="#">Securing Passwords in Configuration Files</a></p>
      </th>
      <td>
        <p>All WSO2 Carbon products contain some configuration files with sensitive information such as passwords. Let's take a look at how such plain text passwords in configuration files can be secured using the Secure Vault implementation that is built into Carbon products.</p>
        <p>The following topics will be covered under this section:</p>
        <ul class="childpages-macro">
          <li><a href="https://docs.wso2.com/display/ADMIN44x/Encrypting+Passwords+with+Cipher+Tool" class="external-link" rel="nofollow">Encrypting Passwords with Cipher Tool</a></li>
          <li><a href="https://docs.wso2.com/display/ADMIN44x/Resolving+Encrypted+Passwords" class="external-link" rel="nofollow">Resolving Encrypted Passwords</a></li>
          <li><a href="https://docs.wso2.com/display/ADMIN44x/Carbon+Secure+Vault+Implementation" class="external-link" rel="nofollow">Carbon Secure Vault Implementation</a></li>
        </ul>
      </td>
    </tr>
    <tr>
      <th>
        <p><a class="unresolved" href="#">Resolving Hostname Verification</a></p>
      </th>
      <td>Hostname verification is enabled in Entgra products by default, which means that when a hostname is being accessed by a particular client, it will be verified against the hostname specified in the product's SSL certificate. &nbsp;</td>
    </tr>
  </tbody>
</table>

* * *

## Configuring multitenancy

You can create multiple tenants in your product server, which will allow you to maintain tenant isolation in a single server/cluster. For instructions on configuring multiple tenants for your server, see [Tenant Management](/doc/en/lb2/Tenant-Management.html) in the Entgra IoT Server Guide.

* * *

## Configuring the registry

A  **registry** is a content store and a metadata repository for various artifacts such as services, WSDLs and configuration files. In Entgra products, all configurations pertaining to modules, logging, security, data sources and other service groups are stored in the registry by default. 

For instructions on setting up and configuring the registry for your server, see [Working with the Registry](about:blank#) in the WSO2 Product Administration Guide.

* * *

## Performance tuning

You can optimize the performance of your production server by configuring the appropriate OS settings, JVM settings etc. Most of these are server-level settings that will improve the performance of any WSO2 product. For instructions, see [Performance Tuning](about:blank#) in the WSO2 Product Administration Guide.  

Additionally, check out the following sections to further optimize the performance of Entgra IoT Server.

*   [Monitoring the Device Status](/doc/en/lb2/Monitoring-the-Device-Status.html)
*   [Scheduling the Push Notification Task](/doc/en/lb2/Scheduling-the-Push-Notification-Task.html)
*   [Configuring Caching to Improve Performance](/doc/en/lb2/Configuring-Caching-to-Improve-Performance.html)

*   [Archiving Operation Data to Improve Performance](/doc/en/lb2/Archiving-Operation-Data-to-Improve-Performance.html)

* * *

## Changing the default ports

When you run multiple Entgra products, multiple instances of the same product, or multiple WSO2 product clusters on the same server or virtual machines (VMs), you must change their default ports with an offset value to avoid port conflicts.

For instructions on configuring ports, see [Changing the Default Ports](/doc/en/lb2/Changing-the-Default-Ports.html) in the Entgra IoT Server Guide.

* * *

## Installing, uninstalling and managing product features

Each WSO2 product is a collection of reusable software units called features where a single feature is a list of components and/or other feature. By default, Entgra IoT Server is shipped with the features that are required for your main use cases. 

*   For information on installing new features, or removing/updating an existing feature, see [Working with Features](about:blank#) in the WSO2 Product Administration Guide.
*   For instructions on removing a sample device plugin in Entgra IoT Server, see [Removing a Sample Device Plugin on Entgra IoT Server](/doc/en/lb2/Removing-a-Sample-Device-Plugin-on-Entgra-IoT-Server.html) in the Entgra IoT Server Guide. 

* * *

## Configuring custom proxy paths

This feature is particularly useful when multiple Entgra products (fronted by a proxy server) are hosted under the same domain name. By adding a custom proxy path you can host all products under a single domain and assign proxy paths for each product separately. 

For instructions on configuring custom proxy paths, see  [Adding a Custom Proxy Path](about:blank#) in the WSO2 Product Administration Guide.  

* * *

## Customizing error pages

You can make sure that sensitive information about the server is not revealed in error messages, by customizing the error pages in your product. For instructions, see  [Customizing Error Pages](about:blank#) in the WSO2 Product Administration Guide.  

* * *

## Customizing the management console

Some of the Entgra products, such as WSO2 DSS consist of a web user interface named the management console. This allows administrators to configure, monitor, tune, and maintain the product using a simple interface. You can customize the look and feel of the management console for your product.

For instructions, see  [Customizing the Management Console](about:blank#) in the WSO2 Product Administration Guide.

* * *

## Applying patches

For information on updating Entgra IoT Server with the latest available patches (issued by WSO2) using the WSO2 Update Manager (WUM), see [Updating Entgra products](https://docs.wso2.com/display/ADMIN44x/Updating+WSO2+Products) in the WSO2 Administration Guide.

* * *

## Updating Entgra products with WUM

The  [WSO2 Update Manager (WUM)](http://wso2.com/update/)  is a command-line utility that allows you to get the latest updates that are available for a particular product release. These updates include the latest bug fixes and security fixes that are released by WSO2 after a particular product version is released. Therefore, you do not need to wait and upgrade to the next product release to get these bug fixes.

For instructions on updating Entgra IoT Server with WUM, see [Getting Started with WUM](about:blank#) in the WSO2 Product Administration Guide. 

* * *

## Working with certificates in IoT Server

See [Certificate Management](/doc/en/lb2/Certificate-Management.html) for information and instructions on establishing a trust relationship between the IoT server and the client using mutual SSL authentication.

* * *

## Monitoring

Monitoring is an important part of maintaining a product server. Listed below are the monitoring capabilities that are available for Entgra IoT Server.

*   **JMX-based monitoring:** For information on monitoring your server using JMX, see [JMX-based monitoring](about:blank#) in the WSO2 Product Administration Guide.
*   **Device monitoring:** For information on configuring the monitoring frequency of devices enrolled with Entgra IoT Server, see [General Platform Configurations](/doc/en/lb2/General-Platform-Configurations.html).
*   **Monitoring server logs: ** A properly configured logging system is vital for identifying errors, security threats and usage patterns in your production server.   
    For instructions on monitoring the server logs, see the following topics in the WSO2 Product Administration Guide. 

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th width="20%"><a class="unresolved" href="#">Monitoring Logs using Management Console</a></th>
      <td>
        <div class="content-wrapper">
          <p><a href="https://docs.wso2.com/display/ADMIN44x/Monitoring+Logs" class="external-link" rel="nofollow">Monitoring logs</a>&nbsp;using the management console of your product is possible with the Logging Management feature. To use this feature, see the following topics:</p>
          <ul class="childpages-macro">
            <li>
              <p><a href="https://docs.wso2.com/display/ADMIN44x/Configuring+Log4j+Properties" class="external-link" rel="nofollow">Configuring Log4j Properties<br></a></p>
              <div class="confluence-information-macro confluence-information-macro-information"><span class="aui-icon aui-icon-small aui-iconfont-info confluence-information-macro-icon"></span>
                <div class="confluence-information-macro-body">
                  <p>The Entgra IoT Server's Log4j property files can be found here:</p>
                  <div class="table-wrap">
                    <table>
                      <tbody>
                        <tr>
                          <th>Core profile</th>
                          <td><code>&lt;IOTS_HOME&gt;/conf/log4j.properties</code></td>
                        </tr>
                        <tr>
                          <th>Analytics profile</th>
                          <td><code>&lt;IOTS_HOME&gt;/wso2/analytics/conf/log4j.properties</code></td>
                        </tr>
                        <tr>
                          <th>Broker profile</th>
                          <td><code>&lt;IOTS_HOME&gt;/wso2/broker/conf/log4j.properties</code></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <p><a href="https://docs.wso2.com/display/ADMIN44x/Configuring+the+Log+Provider" class="external-link" rel="nofollow">Configuring the Log Provider</a></p>
              <div class="confluence-information-macro confluence-information-macro-information"><span class="aui-icon aui-icon-small aui-iconfont-info confluence-information-macro-icon"></span>
                <div class="confluence-information-macro-body">
                  <p>The Entgra IoT Server's carbon log files can be found here:</p>
                  <div class="table-wrap">
                    <table>
                      <tbody>
                        <tr>
                          <th>Core profile</th>
                          <td><code>&lt;IOTS_HOME&gt;/repository/logs</code></td>
                        </tr>
                        <tr>
                          <th>Analytics profile</th>
                          <td><code>&lt;IOTS_HOME&gt;/wso2/analytics/repository/logs</code></td>
                        </tr>
                        <tr>
                          <th>Broker profile</th>
                          <td><code>&lt;IOTS_HOME&gt;/wso2/broker/repository/logs</code></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </li>
            <li><a href="https://docs.wso2.com/display/ADMIN44x/View+and+Download+Logs" class="external-link" rel="nofollow">View and Download Logs</a></li>
          </ul>
        </div>
      </td>
    </tr>
    <tr>
      <th><a class="unresolved" href="#">HTTP Access Logging</a></th>
      <td>&nbsp;HTTP&nbsp;Requests/Responses are logged in the access log(s) and are helpful to monitor your application's usage activities, such as the persons who access it, how many hits it receives, what the errors are etc. This information is useful for troubleshooting. As the runtime of Entgra products is based on Apache Tomcat, you can use the&nbsp;<code>Access_Log_Valve</code>&nbsp;variable in Tomcat 7, as explained in this topic, to configure HTTP access logs in Entgra products.<br><br>You can also customize the&nbsp;access logs based on the&nbsp;supported&nbsp;<a class="external-link" href="http://tomcat.apache.org/tomcat-7.0-doc/config/valve.html#Access_Log_Valve/Attributes" rel="nofollow">Access Log Valve&nbsp;attributes</a>.&nbsp;&nbsp;</td>
    </tr>
  </tbody>
</table>

* * *

## Configuring with Other Products

Want to configure Entgra IoT Server with WSO2 API Manager, WSO2 Identity Server, WSO2 Data Analytics Server or a third party MQTT broker? Check out the sub sections given below:

*   [Configuring Entgra IoT Server with WSO2 API Manager](null/pages/createpage.action?spaceKey=IoTS340&title=Configuring+WSO2+IoT+Server+with+WSO2+API+Manager)
*   [Integrating a Third-Party Identity Provider for Access Token Management](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822861/Integrating+a+Third-Party+Identity+Provider+for+Access+Token+Management)
*   [Configuring Entgra IoT Server with WSO2 Data Analytics Server](/doc/en/lb2/Configuring-Entgra-IoT-Server-with-Entgra-Data-Analytics-Server.html)
*   [Configuring Entgra IoT Server with WSO2 Message Broker](https://entgra.atlassian.net/wiki/spaces/IoTS340/pages/28278981/Configuring+WSO2+IoT+Server+with+WSO2+Message+Broker)
*   [Configuring Entgra IoT Server with a Third Party MQTT Broker](/doc/en/lb2/Configuring-Entgra-IoT-Server-with-a-Third-Party-MQTT-Broker.html)

* * *

## Required ports for Entgra IoT Server

The following ports need to be opened for Entgra IoT Server, and Android and iOS devices so that it can connect to Google Cloud Messaging (GCM)/Firebase Cloud Messaging (FCM) and APNS (Apple Push Notification Service), and enroll to Entgra IoT Server.

### Default Ports

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>
        <p>8243</p>
      </th>
      <td>HTTPS gateway port.</td>
    </tr>
    <tr>
      <th>9443</th>
      <td>HTTPS port for the core profile.</td>
    </tr>
    <tr>
      <th>8280</th>
      <td>
        <p>HTTP gateway port.</p>
      </td>
    </tr>
    <tr>
      <th>9763</th>
      <td>HTTP port for the core profile.</td>
    </tr>
    <tr>
      <th>1886</th>
      <td>Default MQTT port.</td>
    </tr>
    <tr>
      <th>9445</th>
      <td>HTTPS port for the analytics profile.</td>
    </tr>
    <tr>
      <th>9765</th>
      <td>HTTP port for the analytics profile.</td>
    </tr>
    <tr>
      <th>7713</th>
      <td>
        <p>HTTPS port to publish thrift data in the analytics profile</p>
      </td>
    </tr>
    <tr>
      <th>7613</th>
      <td>
        <p>HTTP port to publish thrift data in the analytics profile</p>
      </td>
    </tr>
    <tr>
      <th>9446</th>
      <td>HTTPS port for the broker profile.</td>
    </tr>
    <tr>
      <th>9766</th>
      <td>HTTP port for the broker profile.</td>
    </tr>
  </tbody>
</table>

## Ports required for mobile devices to communicate with the server and the respective notification servers.

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th><br></th>
      <th>Android</th>
    </tr>
    <tr>
      <th>
        <p>5228</p>
        <p>5229</p>
        <p>5230</p>
      </th>
      <td>The ports to open are 5228, 5229 and 5230\. Google Cloud Messaging (GCM) and Firebase Cloud Messaging (FCM) typically only use 5228, but it sometimes uses 5229 and 5230.<br>GCM/FCM does not provide specific IPs, so it is recommended to allow the firewall to accept outgoing connections to all IP addresses contained in the IP blocks listed in Google's ASN of 15169.&nbsp;</td>
    </tr>
    <tr>
      <th><br></th>
      <th>iOS</th>
    </tr>
    <tr>
      <th>5223</th>
      <td>Transmission Control Protocol (TCP) port&nbsp;used by devices to communicate to APNs servers.</td>
    </tr>
    <tr>
      <th>2195</th>
      <td>TCP port&nbsp;used to send notifications to APNs.</td>
    </tr>
    <tr>
      <th>2196</th>
      <td>TCP port&nbsp;&nbsp;used by the APNs feedback service</td>
    </tr>
    <tr>
      <th>443</th>
      <td>
        <p>TCP port&nbsp;used as a fallback on Wi-Fi, only when devices are unable to communicate to APNs on port 5223</p>
        <p>The APNs servers use load balancing. The devices will not always connect to the same public IP address for notifications. The entire&nbsp;17.0.0.0/8&nbsp;address block is assigned to Apple, so it is best to allow this range in the firewall settings.&nbsp;</p>
      </td>
    </tr>
  </tbody>
</table>