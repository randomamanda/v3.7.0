---
layout: default
title: Changing the Super Administrator Username and Password  
parent: Product administration
grand_parent: Using Entgra IoT Server
nav_order: 15
---

# Changing the Super Administrator Username and Password

Entgra IoT Server has the admin user created by default. This user is also known as the super administrator as it has all the permissions assigned to it. The default username and password of this user is **`admin`** . In a production environment, if you are using the admin user for administration purposes, it is recommended to change the username and password. 

### Changing the username and password using the script

This script automatically changes the username and password of the super administrator users. This method is recommended because manually changing the username and password includes many steps and if you miss out on a step you will run into errors.





If you are running the script, you need to have started Entgra IoT Server atleast once. Therefore, if you are running Entgra IoT Server for the first time, follow the steps given below:







1.  Start the core profile of Entgra IoT Server.

    ```
    cd <IOTS_HOME>/bin
    ./iot-server.sh
    ```

2.  Stop the server and restart the server again. You can stop the profiles using `CTRL+C` respectively.
3.  Run the script that is given below.









1.  Navigate to the `<IOTS_HOME>/scripts` directory.
2.  Run the `change-superadmin-credentials.sh` script.  
    Example: Running the script on a Mac or Linux OS.

    `./change-superadmin-credentials.sh`

    1.  Enter the old super administrator username and password.  
        If you are changing the username or password for the first time, the old username is `admin` and the old password is `admin`.
    2.  Enter the new super administrator username and password
3.  Restart the three profiles in Entgra IoT Server in the following order. You can stop the profiles using `CTRL+C` respectively.

    1.  Restart the broker profile:

        ```
        cd <IOTS_HOME>/broker/bin
        ./broker.sh
        ```

    2.  Restart the core profile:

        ```
        cd <IOTS_HOME>/broker/bin
        ./iot-server.sh
        ```

    3.  Restart the analytics profile:

        ```
        cd <IOTS_HOME>/analytics/bin
        ./analytics.sh
        ```

4.  Clear the browser cache and sign in to the device management console (`https://<IOTS_HOST>:9443/devicemgt`) with new admin credentials.

### Changing the username and password manually

Follow the steps given below to manually change the username or password of the admin user:





If you only want to change the admin password and not the admin username, make sure to only change the password properties.





1.  Open the `<IOTS_HOME>/conf/app-manager.xml` file and configure the username and password fields that have `admin` assigned to it with the new password:

    ```
    <!--Admin username for the Authentication manager.-->
    <Username>{NEW_ADMIN_USERNAME}</Username>
    <!--Admin password for the Authentication manager.-->
    <Password>{NEW_ADMIN_PASSWORD}</Password>

    <!--Admin username for the API gateway.-->
    <Username>{NEW_ADMIN_USERNAME}</Username>
    <!--Admin password for the API gateway.-->
    <Password>{NEW_ADMIN_PASSWORD}</Password>

    <!--Administrator username to login to the remote DAS server.-->
    <DASUsername>{NEW_ADMIN_USERNAME}</DASUsername>
    <!--Administrator password to login to the remote DAS server.-->
    <DASPassword>{NEW_ADMIN_PASSWORD}</DASPassword>

    <Configurators>
       <Configurator>
          <name>wso2is</name>
          <version>5.0.0</version>
          <providerClass>org.wso2.carbon.appmgt.impl.idp.sso.configurator.IS500SAMLSSOConfigurator</providerClass>
          <parameters>
             <providerURL>https://${carbon.local.ip}:${mgt.transport.https.port}</providerURL>
             <username>{NEW_ADMIN_USERNAME}</username>
             <password>{NEW_ADMIN_PASSWORD}</password>
          </parameters>
       </Configurator>
    </Configurators>

    <EntitlementServiceConfiguration>
       <Parameters>
          <ServerUrl>https://${carbon.local.ip}:${mgt.transport.https.port}</ServerUrl>
          <Username>{NEW_ADMIN_USERNAME}</Username>
          <Password>{NEW_ADMIN_PASSWORD}</Password>
       </Parameters>
    </EntitlementServiceConfiguration>

    <MDM name="WSO2MDM" bundle="org.wso2.carbon.appmgt.mdm.restconnector">
       <Property name="ImageURL">/store/extensions/assets/mobileapp/resources/models/%s.png</Property>
       <Property name="ServerURL">https://localhost:9443/mdm-admin</Property>
       <Property name="TokenApiURL">https://localhost:9443/oauth2/token</Property>
       <Property name="ClientKey">WjLm24IxBVLF0oz0VJfmtJbjJbka</Property>
       <Property name="ClientSecret">v3KkIQXkJ1SDp_Bf8uUQxu5p7TQa</Property>
       <Property name="AuthUser">{NEW_ADMIN_USERNAME}</Property>
       <Property name="AuthPass">{NEW_ADMIN_PASSWORD}</Property>
    </MDM>

    <!-- Old EMM, Calls EMM using REST API -->
    <MDM name="WSO2EMM" bundle="org.wso2.carbon.appmgt.mdm.wso2emm">
       <Property name="ServerURL">https://localhost:9443</Property>
       <Property name="ImageURL">https://localhost:9443/emm/assets/wso2mobile/img/models/%s.png</Property>
       <Property name="AuthUser">{NEW_ADMIN_USERNAME}</Property>
       <Property name="AuthPass">{NEW_ADMIN_PASSWORD}</Property>
    </MDM>
    ```

2.  Change the `admin` username and password in the `<IOTS_HOME>/conf/iot-api-config.xml` file.

    ```
    <!--Admin username/password - this is to use for oauth token generation-->
        <Username>{NEW_ADMIN_USERNAME}</Username>
        <Password>{NEW_ADMIN_PASSWORD}</Password>
    ```

3.  Change the `admin` username and password in the `<IOTS_HOME>/conf/apim-integration.xml` file.

4.  Change the `admin` username and password in the `analytics-data-config.xml` file. This file can be found in the following locations:

    *   `<IOTS_HOME>/conf/analytics`

    *   `<IOTS_HOME>/wso2/analytics/conf/analytics`

    ```
    <!-- A valid user name which have necessary permissions to use the remote services -->
    <Username>{NEW_ADMIN_USERNAME}</Username>

    <!--A valid password of the user provided above. You also can use secure vault, if you don't want to configure -->
    <!--the plain text password here.-->
    <Password>{NEW_ADMIN_PASSWORD}</Password>
    ```

5.  Change the all the instances that have `admin` as the username and password in the `<IOTS_HOME>/wso2/analytics/conf/input-event-adapters.xml` file.

6.  Change the `admin` username and password in the `user-mgt.xml` file. The file can be found in the following locations.  
    Make sure to do the changes in all the files.

    *   `<IOTS_HOME>/wso2/analytics/conf` 

    *   `<IOTS_HOME>/wso2/broker/conf`

    *   `<IOTS_HOME>/conf`

    ```
    <AdminUser>
    	<UserName>{NEW_ADMIN_USERNAME}</UserName>
    	<Password>{NEW_ADMIN_PASSWORD}</Password>
    </AdminUser>
    ```

7.  Change the `admin` username and password in the `metrics.xml` file. The file can be found in the following locations.  
    Make sure to do the changes in all the files.

    *   `<IOTS_HOME>/conf`

    *   `<IOTS_HOME>/wso2/analytics/conf`

    *   `<IOTS_HOME>/wso2/broker/conf`

    ```
    <Username>{NEW_ADMIN_USERNAME}</Username>
    <Password>{NEW_ADMIN_PASSWORD}</Password>
    ```

8.  Change the `admin` username and password in the `<IOTS_HOME>/wso2/broker/conf/broker.xml` file.

    ```
    <authenticator class="org.wso2.carbon.andes.authentication.andes.OAuth2BasedMQTTAuthenticator">
       <property name="hostURL">https://localhost:9443/services/OAuth2TokenValidationService</property>
       <property name="username">{NEW_ADMIN_USERNAME}</property>
       <property name="password">{NEW_ADMIN_PASSWORD}</property>
       <property name="maxConnectionsPerHost">10</property>
       <property name="maxTotalConnections">150</property>
    </authenticator>

    <authorizer class="org.wso2.carbon.andes.extensions.device.mgt.mqtt.authorization.DeviceAccessBasedMQTTAuthorizer">
       <!--<property name="connectionPermission">/permission/admin/device-mgt/user</property>-->
       <!--topic subscription permissions which are assigned through grouping-->
       <property name="subscriberPermission">/permission/device-mgt/user/groups/device_events</property>
       <!--topic publisher permissions which are assigned through grouping-->
       <property name="publisherPermission">/permission/device-mgt/user/groups/device_operation</property>
       <property name="username">{NEW_ADMIN_USERNAME}</property>
       <property name="password">{NEW_ADMIN_PASSWORD}</property>
       <property name="tokenEndpoint">https://localhost:8243</property>
       <!--offset time from expiry time to trigger refresh call - seconds -->
       <property name="tokenRefreshTimeOffset">100</property>
       <property name="deviceMgtServerUrl">https://localhost:8243</property>
    </authorizer>
    ```

9.  Configure the files given below to update the super administrator username:

    

    

    When adding the admin username, make sure to append the super tenant domain name to the username. The default super tenant domain name is `carbon.super`.

    

    

    1.  Change the admin user name and the owner that is under the `oauthProvider` property in the `<IOT_HOME>/repository/deployment/server/jaggeryapps/devicemgt/app/conf/config.json` file.

        ```js
        "adminUser": "{NEW_ADMIN_USERNAME}@carbon.super"
        "owner": "{NEW_ADMIN_USERNAME}@carbon.super",
        ```

    2.  

        *   

        *   

            `"adminUser": "{NEW_ADMIN_USERNAME}@carbon.super"`

10.  Restart the three profiles in Entgra IoT Server in the following order. You can stop the profiles using `CTRL+C` respectively.

    1.  Restart the broker profile:

        ```
        cd <IOTS_HOME>/broker/bin
        ./broker.sh
        ```

    2.  Restart the core profile:

        ```
        cd <IOTS_HOME>/broker/bin
        ./iot-server.sh
        ```

    3.  Restart the analytics profile:

        ```
        cd <IOTS_HOME>/analytics/bin
        ./analytics.sh
        ```

11.  Clear the browser cache and sign in to the device management console (`https://<IOTS_HOST>:9443/devicemgt`) with new admin credentials.
