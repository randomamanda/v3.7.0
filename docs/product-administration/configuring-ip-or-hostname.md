---
layout: default
title: Configuring IP or Hostname
parent: Product administration
grand_parent: Using Entgra IoT Server
nav_order: 1
---
# Configuring IP or Hostname
 
Entgra IoT Server is configured via localhost as the product has SSO enabled by default. In a production environment or if you want to make Entgra IoT Server IP or hostname based instead of localhost, you need to configure it accordingly. Let's take a look at the steps you need to follow to configure Entgra IoT Server with the IP:


*   If you are configuring Entgra IoT Server for iOS, follow the steps given in [Configuring Entgra IoT Server with the IP or Hostname for iOS](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352824187/Configuring+Entgra+IoT+Server+with+the+IP+or+Hostname+for+iOS).
*   If you want to configure the ports of Entgra IoT Server, see [Changing the Default Ports](/doc/en/lb2/Changing-the-Default-Ports.html).


If you are changing the IP or hostname and if you already have tenants created in Entgra IoT Server, you will not able to access them after changing the IP because of the changes that are being made to the keystore. Therefore, **it is recommended to keep a backup of the tenants** when changing the IP or hostname.

Further, if you try to access an old tenant after changing the IP or hostname, you are directed to the following page. Immediately afterwards, if you try to access a new tenant that was created after changing the IP or hostname, you are still directed the following page. To overcome this issue and to be able to access the newly created tenant, you need to clear the browser cookies.  
![]({{site.baseurl}}/assets/images/352819833.png)


Before you begin


In a production environment, you need to open the ports listed under **[Required ports for Entgra IoT Server](/doc/en/lb2/Product-Administration.html#ProductAdministration-RequiredportsforWSO2IoTServer)** for Entgra IoT Server to connect to Google Cloud Messaging (GCM)/Firebase Cloud Messaging (FCM) and Apple Push Notification Service (APNS), and enroll devices.


## Configuring the IP or hostname using the script

This script automatically configures the IP and creates the required SSL certificates for the IP or hostname. This method is recommended because manually configuring the IP address includes many steps and if you miss out on a step you will run into errors.

1.  Navigate to the `<IOTS_HOME>/scripts` directory.
2.  Run the `change-ip` script.  
    Example: Running the script on a Mac or Linux OS.

    `./change-ip.sh`



    **Tip:** The script finds and replaces the IP address given in `argument1` (`localhost`) with the IP address given as `argument2` (`10.10.10.14`), in the necessary configuration files. 



    1.  Change the current IP address of the IoT Server core, broker, and analytics profile.

    2.  Enter the values for IoT Server core SSL certificate.

        Enter the requested information when prompted.

        <table>
          <colgroup>
            <col>
            <col>
          </colgroup>
          <thead>
            <tr>
              <th>
                <div class="tablesorter-header-inner">Field</div>
              </th>
              <th>
                <div class="tablesorter-header-inner">Usage/Purpose</div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Country</td>
              <td>The name of your country. Enter the two digit code for your country.</td>
            </tr>
            <tr>
              <td>State</td>
              <td>The state your organization is at.</td>
            </tr>
            <tr>
              <td>Location</td>
              <td>The city your organization is located at.</td>
            </tr>
            <tr>
              <td>
                <p>Organization</p>
              </td>
              <td>
                <p>The name of your organization. For this scenario, we entered wso2.</p>
              </td>
            </tr>
            <tr>
              <td>Organization Unit</td>
              <td>Defined the Team ID as the organization unit.</td>
            </tr>
            <tr>
              <td>
                <p>Email</p>
              </td>
              <td>
                <p>The email is used to identify the existing users. For this scenario, we entered&nbsp;chris@wso2.com&nbsp;as the email.</p>
              </td>
            </tr>
            <tr>
              <td>
                <p>Commonname</p>
              </td>
              <td>
                <p>Fully qualified domain name of your server.</p>
              </td>
            </tr>
          </tbody>
        </table>

        ![]({{site.baseurl}}/assets/images/352819827.png)




If you run the `change-ip.sh` script once and access the device management console using the Chrome browser, it works perfectly. But, if you run the `change-ip.sh` script for the second time and access the device management console via the Chrome browser, you will run into an error.

**To avoid this,** you can try out any of the options given below**:**

*   Option 1:  
    Use another browser, such as Fire Fox, instead of the Chrome browser.
*   Option 2:  
    Open the `change-ip.sh` script, change the value defined for the `set_serial` property in the command given below to a different 8 digit number, and save the file. You need to do this after running the script for the first time.

    **Note**: Make sure to have a unique serial number each time you run the script.

    `openssl x509 -req -days 730 -in ./tmp/c.csr -signkey ./tmp/c.key -set_serial 044324884 -out ./tmp/c.crt`



Are you getting the following error message after running the `change-ip.sh` script?

```
[IoT-Core] ERROR - {org.apache.synapse.transport.passthru.TargetHandler} I/O error: Host name verification failed for host : {IoT_SERVER_HOSTNAME}
javax.net.ssl.SSLException: Host name verification failed for host : {IoT_SERVER_HOSTNAME}
```

**To avoid this**, add the following host entry into the `/etc/hosts` file.

`127.0.0.1	{IoT_SERVER_HOSTNAME}`


## Configuring the IP or hostname manually

1.  Open the `<IOTS_HOME>/conf/carbon.xml` file and configure the `<HostName>` and `<MgtHostName>` attributes with the {`IoT_SERVER_HOSTNAME}`.

    ```
    <HostName>{IoT_SERVER_IP/HOSTNAME}</HostName>
    <MgtHostName>{IoT_SERVER_IP/HOSTNAME}</MgtHostName>
    ```

2.  Open the `<IOTS_HOME>/conf/identity/sso-idp-config.xml` file, and find and replace `localhost` with the `<IoT_SERVER_IP/HOSTNAME` `>`.

3.  Open the `<IOTS_HOME>/conf/api-manager.xml` file and configure the `<DASServerURL>` attribute by replacing localhost with the IoT Server IP or hostname.

    `<DASServerURL>{tcp://<IoT_SERVER_IP/HOSTNAME>t:7613}</DASServerURL>`

4.  Open the `<IOTS_HOME>/conf/etc/webapp-publisher-config.xml` file, and set `true` as the value for `<EnabledUpdateApi>`.

    ```
    <!-- If it is true, the APIs of this instance will be updated when the webapps are redeployed -->
    <EnabledUpdateApi>true</EnabledUpdateApi>
    ```


    If you have not started Entgra IoT Server previously, you don't need this configuration. When the server starts for the first time it will update the APIs and web apps with the new server IP.



    Make sure to configure this property back to `false` if you need to restart the server again after the configuring the IP.  

    By enabling the update API property, the APIs and the respective web apps get updated when the server restarts. This takes some time. Therefore, if you need to restart the server many times after this configuration or when in a production environment, you need to revert back to the default setting.


5.  Open the `<IOT_HOME>/repository/deployment/server/jaggeryapps/api-store/site/conf/site.json` file, and configure the `identityProviderUrl` attribute by replacing localhost with the IoT Server IP or hostname.

    `"identityProviderURL" : "https://<IoT_SERVER_IP/HOSTNAME>:9443/samlsso",`

6.  Open the `<IOT_HOME>/wso2/analytics/repository/deployment/server/jaggeryapps/portal/configs/designer.json` file, and configure the `identityProviderUrl`, `acs`, and `host` attributes by replacing `localhost` with the IoT Server IP or hostname and the respective profiles port.

    ```js
    "identityProviderURL": "https://<IoT_SERVER_IP/HOSTNAME>:9443/samlsso",
    "acs": "https://<IoT_SERVER_IP/HOSTNAME>:9445/portal/acs",
    "host": {
      "hostname": "<IoT_SERVER_IP/HOSTNAME>",
      "port": "",
      "protocol": ""
    },
    ```


    The default port of the Entgra IoT Server profiles are as follows:

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Entgra IoT Server core profile</th>
          <td>9443</td>
        </tr>
        <tr>
          <th>Entgra IoT Server analytics profile</th>
          <td>9445</td>
        </tr>
        <tr>
          <th>Entgra IoT Server broker profile</th>
          <td>9446</td>
        </tr>
      </tbody>
    </table>

    Therefore, the analytics portal needs to be assigned the 9445 port.



7.  Open the `<IOTS_HOME>/bin/iot-server.sh` file and configure the following properties by replacing localhost with the `<IoT_SERVER_IP/HOSTNAME>`. If you are running on Windows, you need to configure the `iot-server.bat` file.

    ```
    -Diot.analytics.host="<IoT_SERVER_IP/HOSTNAME>" \
    -Diot.manager.host="<IoT_SERVER_IP/HOSTNAME>" \
    -Dmqtt.broker.host="<IoT_SERVER_IP/HOSTNAME>" \
    -Diot.core.host="<IoT_SERVER_IP/HOSTNAME>" \
    -Diot.keymanager.host="<IoT_SERVER_IP/HOSTNAME>" \
    -Diot.gateway.host="<IoT_SERVER_IP/HOSTNAME>" \
    -Diot.apimpublisher.host="<IoT_SERVER_IP/HOSTNAME>" \
    -Diot.apimstore.host="<IoT_SERVER_IP/HOSTNAME>" \
    ```

8.  Open the `<IOTS_HOME>/wso2/analytics/bin/wso2.server.sh` file and configure the following properties by replacing localhost with the `<IoT_SERVER_IP/HOSTNAME>`. If you are running on Windows, you need to configure the `wso2server.bat` file.

    ```
    -Dmqtt.broker.host="<IoT_SERVER_IP/HOSTNAME>" \
    -Diot.keymanager.host="<IoT_SERVER_IP/HOSTNAME>" \
    -Diot.gateway.host="<IoT_SERVER_IP/HOSTNAME>" \
    ```

9.  Open the `<IOTS_HOME>/wso2/broker/conf/broker.xml` file and configure the following properties by replacing localhost with the `<IoT_SERVER_IP/HOSTNAME>`:

    ```
    <authenticator class="org.wso2.carbon.andes.authentication.andes.OAuth2BasedMQTTAuthenticator">
       <property name="hostURL">https://<IoT_SERVER_IP/HOSTNAME>:9443/services/OAuth2TokenValidationService</property>
       <property name="username">admin</property>
       <property name="password">admin</property>
       <property name="maxConnectionsPerHost">10</property>
       <property name="maxTotalConnections">150</property>
    </authenticator>

    <authorizer class="org.wso2.carbon.andes.extensions.device.mgt.mqtt.authorization.DeviceAccessBasedMQTTAuthorizer">
       <property name="username">admin</property>
       <property name="password">admin</property>
       <property name="tokenEndpoint">https://<IoT_SERVER_IP/HOSTNAME>t:8243</property>
       <!--offset time from expiry time to trigger refresh call - seconds -->
       <property name="tokenRefreshTimeOffset">100</property>
       <property name="deviceMgtServerUrl">https://<IoT_SERVER_IP/HOSTNAME>t:8243</property>
    </authorizer>
    ```

10.  Optionally, if you are using the [Entgra Android auto-enrollment feature](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352823121/Auto+Enrolling+an+Android+Device), you need to replace all the `localhost` references to the IP or hostname in the following files that are in the

    `<IOTS_HOME>/repository/deployment/server/synapse-configs/default/api` directory.
    *   `admin--Android-Mutual-SSL-Event-Receiver.xml`
    *   `admin--Android-Mutual-SSL-Device-Management.xml`
    *   `admin--Android-Mutual-SSL-Configuration-Management.xml`
11.  If you are using the hostname instead of the IP, open the `<IOTS_HOME>/repository/deployment/server/jaggeryapps/devicemgt/app/conf/config.json` file and configure the `androidAgentDownloadURL` property.

    `"androidAgentDownloadURL": "https://%iot.manager.host%:%iot.manager.https.port%/android-web-agent/public/mdm.page.enrollments.android.download-agent/asset/android-agent.apk",`

12.  Run the following commands so that the self-signed certificate refers to the IP you just configured instead of `localhost`.


    This step is required if your devices are accessing Entgra IoT Server from outside the server.



    Because of the changes made to the keystore, you will not able to access the tenants that are already created in Entgra IoT Server. Therefore, **it is recommended to keep a backup of the tenants** when changing the IP or hostname.


    1.  Navigate to the `<IOTS_HOME>/repository/resources/security` directory and run the following commands to create the `client-truststore.jks` and `wso2carbon.jks` files with the new IP or hostname. If ask for key

        ```
        keytool -delete -alias wso2carbon -keystore wso2carbon.jks -storepass wso2carbon -keypass wso2carbon

        keytool -genkey -alias wso2carbon -keyalg RSA -keysize 2048 -keystore wso2carbon.jks -dname "CN=<IOT_SERVER_IP/HOSTNAME>,
        OU=Home,O=Home,L=SL,S=WS,C=LK" -storepass wso2carbon -keypass wso2carbon

        keytool -delete -alias wso2carbon -keystore client-truststore.jks -storepass wso2carbon -keypass wso2carbon

        keytool -export -alias wso2carbon -keystore wso2carbon.jks -file wso2carbon.pem -storepass wso2carbon -keypass wso2carbon

        keytool -import -alias wso2carbon -file wso2carbon.pem -keystore client-truststore.jks -storepass wso2carbon
        ```

    2.  Update the Identity Provider (IDP) with the new certificate:

        1.  Export wso2carbon.pem certificate that is in the binary DER format to the ASCII PEM format.

            `openssl x509 -inform DER -outform PEM -in wso2carbon.pem -out server.crt`

        2.  Open the `server.crt` file you just generated and copy the content that is between the `BEGIN CERTIFICATE` and `END CERTIFICATE`.


            Make sure to remove the new lines that are there in the certificate. Else, the JWT validation fails.


        3.  Open the `<IOTS_HOME>/conf/identity/identity-providers/iot_default.xml` file and replace the content that is under the `<Certificate>` property with the content you just copied.

    3.  Copy the `client-truststore.jks` and `wso2carbon.jks` files that you created in step **13.a** to the following locations.


        Make sure to only copy the files. Don't remove it from the `<IOTS_HOME>/repository/resources/security` directory.


        *   `<IOTS_HOME>/wso2/broker/repository/resources/security`

        *   `<IOTS_HOME>/wso2/analytics/repository/resources/security`

13.  Once you are done with the above steps, restart or start the message broker, IoT Server core, and the analytics profiles in the given order. For more information, see [Starting the Server](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819593/Running+the+Product#RunningtheProduct-StartingtheServer).