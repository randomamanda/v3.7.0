---
layout: default
title: General Platform COnfigurations
parent: Product administration
grand_parent: Using Entgra IoT Server
nav_order: 11
---


# General Platform COnfigurations

Configure how often the devices enrolled with Entgra IoT Server need to be monitored via the general platform configurations and deploy the geo analytics artifacts required for location based services in a multi tenant environment using the general platform configurations.

Follow the instructions below to configure the general platform settings:

1.  [Sign in to the Entgra IoTS device management console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles#AccessingtheEntgraIoTServerConsoles-AccessingtheDeviceManagementConsole) and click the menu icon.  
    ![]({{site.baseurl}}/assets/images/352821220.png)
2.  Click **Platform Configurations**.  
    ![]({{site.baseurl}}/assets/images/352821214.png)
3.  Define the **Monitoring Frequency** in seconds, to monitor the enforced policies on the devices, and click  **SAVE**.  
    ![]({{site.baseurl}}/assets/images/352821230.png)
4.  The geo analytics artifacts are deployed by default in the Entgra IoT Server super tenant. However, if you are setting up geofencing or location based services in a multi-tenant environment, you have to deploy the geo analytics artifacts in each tenant. 

    

    

    For more information, see [Monitoring Devices Using Location Based Services](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822607/Monitoring+Devices+Using+Location+Based+Services).

    

    

    1.  Log in to the device management console using the tenant credentials.

    2.  Click on ![]({{site.baseurl}}/assets/images/2.png), and select** Configuration Management > Platform Configurations**.

    3.  Click the **Deploy Geo Analytics Artifacts** button. You can also use this button to re-deploy the geo analytics artifacts in super tenant mode if required.   
        ![]({{site.baseurl}}/assets/images/352821235.png)