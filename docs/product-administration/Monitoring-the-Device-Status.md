---
layout: default
title: Monitoring the Device Status  
parent: Product administration
grand_parent: Using Entgra IoT Server
nav_order: 17
---

# Monitoring the Device Status


Due to various reasons, the devices registered with Entgra IoT Server might not be able to communicate with the server continuously. When the device is not actively communicating with the server, you need to know of it to take necessary actions, such as checking if the device has any malfunctions and repairing it or checking if the device was stolen.

To get a clear understanding, let's look at how this works in Entgra IoT Server.

*   If the device and the server are actively communicating, the device is shown as active.

    

    

    

    

    In the device management console, click **View** under Devices. You can see all your registered devices and their device statuses.

    ![]({{site.baseurl}}/assets/images/352821749.png)

    

    

    

*   If the server is unable to communicate with the device within a defined time period, the device is shown as unreachable.

    

    

    

    

    In the device management console, click **View** under Devices. You can see all your registered devices and their device statuses.

    ![]({{site.baseurl}}/assets/images/352821760.png)

    

    

    

*   If the server is still unable to communicate with the device after a defined time period, the device is shown as inactive.

    

    

    

    

    In the device management console, click **View** under Devices. You can see all your registered devices and their device statuses.

    ![]({{site.baseurl}}/assets/images/352821743.png)

    

    

    

*   If the device starts to communicate with the device after some time, the device status is updated back to active.

The device's status has the following lifecycle:

![]({{site.baseurl}}/assets/images/352821755.png)

The device monitoring task is not applicable for all IoT devices. Therefore, you can choose to enable or disable it for your device type. Let's take a look at how you can configure Entgra IoT Server and your device type to monitor the device status.

1.  Open the `<IOTS_HOME>/conf/cdm-config.xml` file and make sure the `DeviceStatusTaskConfig` is enabled. This configuration is enabled by default.

    

    

    If the `DeviceStatusTaskConfig` is enabled (or enabled on a node that is in a clustered setup) it will run the status monitoring task in the server. If the configuration is disabled, the server will not monitor the status of the devices.

    

    

    ```
    <DeviceStatusTaskConfig>
    	<Enable>true</Enable>
    </DeviceStatusTaskConfig>
    ```

2.  Configure the device type to go into the unreachable state and then to the inactive state after a specified time.  
    Navigate to the `<IOTS_HOME>/repository/deployment/server/devicetype` directory, open the `<DEVICE_TYPE>.xml` file, and configure the fields given below:  
    The default configuration in the `android.xml` file is shown below:

    ```
    <DeviceStatusTaskConfig>
       <RequireStatusMonitoring>true</RequireStatusMonitoring>
       <Frequency>300</Frequency>
       <IdleTimeToMarkUnreachable>300</IdleTimeToMarkUnreachable>
       <IdleTimeToMarkInactive>600</IdleTimeToMarkInactive>
    </DeviceStatusTaskConfig>
    ```

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>
            
          </th>
          <td>
            
              <p>If the value is set to true, it enables the status monitoring task for the device type. Else it will not monitor the status of the device type.</p>
              
                
                  <p>For the task to run on the specified device type it must be <a href="#MonitoringtheDeviceStatus-step1">enabled on the server as shown in step 1 above</a>.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <th>
            
          </th>
          <td>Define how often this task should run for the specified device type. The value needs to be given in seconds.
              <p>Make sure to have the value above 60 seconds.</p>
            
          </td>
        </tr>
        <tr>
          <th>
            
          </th>
          <td>Define after how long the device needs to be marked as unreachable. The value needs to be given in seconds.</td>
        </tr>
        <tr>
          <th>
            
          </th>
          <td>Define after how long the device needs to be marked as inactive. The value needs to be given in seconds.</td>
        </tr>
      </tbody>
    </table>





Additionally to the above configurations, for the device monitoring task to actively function, you need to have pending operations on the device end. When there are pending operations the device communicates with the server to send the operation details to the server and through it, the server keeps track that the device is active.



