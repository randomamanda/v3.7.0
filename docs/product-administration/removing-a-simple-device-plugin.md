---
layout: default
title: Removing Simple Device Plugin From Entgra IoT Server
parent: Product administration
grand_parent: Using Entgra IoT Server
nav_order: 8
---

# Removing Simple Device Plugin From Entgra IoT Server


1.  [Sign in to the Entgra IoTS Management Console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles#AccessingtheEntgraIoTServerConsoles-AccessingtheWSO2IoTSManagementConsole).  

    *   Accessing the console via HTTP:``http://<HTTP_HOST>:<HTTP_PORT>/carbon/`` Example:` http://localhost:9763/carbon/`
    *   Accessing the console via HTTPS:   
        `https://<HTTPS_HOST>:<HTTPS_PORT>/carbon/` Example:` https://localhost:9443/carbon/`
2.  On the **Configure** menu, click **Features**.  
    The **Feature Management** page will appear.   
    ![]({{site.baseurl}}/assets/images/352820936.png)
3.  Click the **Installed Features** tab.  
    The **Installed Features** page allows you to browse through the list of installed features.
4.  Select the features that you need to uninstall. If you wish to uninstall all the features, select the **Select all in this page** option.
5.  Click **Uninstall**.  
    A page will appear containing details of the features to be uninstalled.  
    Example: Uninstalling the Arduino sample device type.  
    ![]({{site.baseurl}}/assets/images/352820942.png)
6.  Verify the information, and click **Next**.   
    If the uninstallation is successful, a success message will appear.

    

    

    If there are other features that depend on the feature that needs to be uninstalled, those dependent sub features need to be uninstalled first, before attempting to uninstall the main feature.

    

    

7.  Click **RESTART NOW** to apply the changes.
