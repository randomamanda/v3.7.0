---
layout: default
title: Configuring Keystores in Entgra IoT Server
parent: Product administration
grand_parent: Using Entgra IoT Server
nav_order: 16
---


# Configuring Keystores in Entgra IoT Server

 If you [changed the keystore from the default Entgra keystore](https://docs.wso2.com/display/ADMIN44x/Configuring+Keystores+in+WSO2+Products), you need to configure the following files:

1.  Change the `wso2carbon` keystore alias to the new keystore alias in the following files.

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th><code>designer.json</code></th>
          <td>
            
              <p>Configure the <code>identityalias</code> in the <code>&lt;IOTS_HOME&gt;/repository/deployment/server/jaggeryapps/portal/configs/designer.json</code> file.</p>
              
                
                  
                
              
            
          </td>
        </tr>
        <tr>
          <th><code>app-conf.json</code></th>
          <td>
            
              <p>Configure the <code>identityAlias</code> in the <code>&lt;IOTS_HOME&gt;/repository/deployment/server/jaggeryapps/devicemgt/app/conf/app-conf.json</code> file.</p>
              
                
                  
                
              
            
          </td>
        </tr>
        <tr>
          <th><code>webapp-authenticator-config.xml</code></th>
          <td>
            
              <p>Configure the <code>&lt;IOTS_HOME&gt;/conf/etc/webapp-authenticator-config.xml</code> file by replacing <code>wso2carbon</code> with the new key store alias.<br>You will come across 3 references as shown below.</p>
              
                
                  &lt;Parameters&gt;
       &lt;!--Issuers list and corresponding cert alias--&gt;
       &lt;Parameter Name="wso2.org/products/am"&gt;{NEW_KEYSTORE_ALIAS}&lt;/Parameter&gt;
       &lt;Parameter Name="wso2.org/products/iot"&gt;{NEW_KEYSTORE_ALIAS}&lt;/Parameter&gt;
       &lt;Parameter Name="wso2.org/products/analytics"&gt;{NEW_KEYSTORE_ALIAS}&lt;/Parameter&gt;
    &lt;/Parameters&gt;
                
              
            
          </td>
        </tr>
      </tbody>
    </table>

2.  [If you added a public certificate,](https://docs.wso2.com/display/ADMIN44x/Creating+New+Keystores#CreatingNewKeystores-Addingthepublickeytoclient-truststore.jks) update the Identity Provider (IDP) with the new certificate. This is needed as Entgra IoT Server uses the JWT token for the servers to communicate with each other.  
    Follow the steps given below to update the IDP.

    1.  If your public certificate is not in the `.pem` format, export it to the `.pem` format using the command given below:

        `openssl x509 -inform DER -outform PEM -in {YOUR_CERTIFICATE_NAME} -out server.crt.pem`

    2.  Open the `server.crt.pem`  you just generated and copy the content between **`BEGIN CERTIFICATE`** and **`END CERTIFICATE`**.

    3.  Open the `<IOTS_HOME>/conf/identity/identity-providers/iot_default.xml` file and replace the content that is under the `<Certificate>` property with the content you just copied.
