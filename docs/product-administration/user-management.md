---
layout: default
title: User Management
parent: Product administration
grand_parent: Using Entgra IoT Server
nav_order: 7
---


# User Management
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

User management is a mechanism which involves defining and managing users, roles and their access levels in a system. A typical user management implementation involves a wide range of functionality such as adding/deleting users, controlling user activity through permissions, managing user roles, defining authentication policies, resetting user passwords etc.

Please note the following before you begin:

*   Your product has a primary user store where the users/roles that you create using the management console are stored by default. It's default `RegEx` configurations are as follows. `RegEx` configurations ensure that parameters like the length of a user name/password meet the requirements of the user store.

    ```
    PasswordJavaRegEx-------- ^[\S]{5,30}$
    PasswordJavaScriptRegEx-- ^[\S]{5,30}$
    UsernameJavaRegEx-------- ^~!#$;%*+={}\\{3,30}$
    UsernameJavaScriptRegEx-- ^[\S]{3,30}$
    RolenameJavaRegEx-------- ^~!#$;%*+={}\\{3,30}$
    RolenameJavaScriptRegEx-- ^[\S]{3,30}$
    ```

    When creating users/roles, if you enter a username, password etc. that does not conform to the `RegEx` configurations, the system throws an exception. You can either change the `RegEx` configuration or enter values that conform to the `RegEx`. If you [change the default user store](about:blank#), configure the `RegEx` accordingly under the user store manager configurations in `<IoTS_HOME>/conf/user-mgt.xml` file.

This section covers the following topics:

Collapse all

# Managing Roles

Entgra IoTS is shipped with a set of default roles. However, if required, tenant administrators will be able to create new customized roles. Tenant administrators can use roles to manage the users and their devices. While end-users allocated with device operation permissions can manage their own devices via the Entgra IoTS Console. Administrators can create roles, assign them to a user or a group of users, and edit or delete existing roles.

# Adding a role and permissions 


Follow the instructions below to add a role:

1.  [Sign in to the Entgra IoT Server console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles#AccessingtheEntgraIoTServerConsoles-AccessingtheDeviceManagementConsole).

    

    

    If you want to try out Entgra IoT Server as an admin, use **admin** as the username and the password.

    

    

2.  You can navigate to the **ADD ROLE** page via the following methods: 
    1.  Method 01: Click **menu** icon ![]({{site.baseurl}}/assets/images/1.png) > **USER MANAGEMENT **> **ROLES** > **ADD ROLE** button.  
        ![]({{site.baseurl}}/assets/images/352820159.png)
    2.  Method 02: Click **Add** icon on the **ROLES** tile.  
        ![]({{site.baseurl}}/assets/images/352820255.png)
3.  Provide the required details and click **Add Role**.
    *   **Domain**: Provide the user store type from the list of items.
    *   **Role Name**: Provide the role name. 

    *   **User Lis**t: Define the users belonging to the respective role. Type the first few characters of the username and Entgra IoT Server will provide the list of users having the same characters. You can then select the user/s you wish to add.![]({{site.baseurl}}/assets/images/352820286.png)
4.  Define the permissions that need to be associated with the role you created by selecting the permissions from the permission tree. As the permissions are categorized, when the main permission category is selected, all its sub-permissions will get selected automatically. 

    

    

    Make sure to select the **Login** permission. Without this permission, the users are unable to log in to Entgra IoT Server.

    

    

    

    

    For more information on the APIs associated with the permissions, see [Permission APIs](/doc/en/lb2/Configuring-Role-Permissions.html#ConfiguringRolePermissions-PermissionAPIs).

    

    

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Permissions</th>
          <th>Description</th>
        </tr>
        <tr>
          <td>
            
              <p><strong>Applications management</strong><br><span class="confluence-embedded-file-wrapper"><img class="confluence-embedded-image" src="352820204.png" data-image-src="attachments/352820123/352820204.png" data-unresolved-comment-count="0" data-linked-resource-id="352820204" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="Manage-App-Permissions.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="6d7487af-c07a-41a7-85a8-1d290cf2e0e5" data-media-type="file"></span></p>
            
          </td>
          <td>
            
              <p>You can install applications on devices registered with Entgra IoT Server via the App Store or you can install applications via the internal REST APIs that is available on Entgra IoT Server. This permission ensures that a user is able to install and uninstall applications via the internal APIs that are available in Entgra IoT Server.</p>
              
                
                  <p>For more information on installing applications via the App Store, see <a href="https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352813484/Installing+an+Application+on+a+Device" data-linked-resource-id="352813484" data-linked-resource-version="1" data-linked-resource-type="page">Installing Mobile Apps</a>.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            
              <p><strong>Certificate management</strong></p>
              <p><strong><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image" width="250" src="352820142.png?width=250" data-image-src="attachments/352820123/352820142.png" data-unresolved-comment-count="0" data-linked-resource-id="352820142" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="RolePermissions_CertificateManagement.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="67a5dfe5-3dfc-4733-b0c2-794e17eeee61" data-media-type="file"></span><br></strong></p>
            
          </td>
          <td>
            
              <p>Entgra IoT Server supports mutual SSL, where the client verifies that the server can be trusted and the server verifies that the client can be trusted by using digital signatures. Following permissions grant access to client-side mutual SSL certificates:</p>
              <ul>
                <li><strong>device-mgt &gt; certificates &gt; manage</strong>: This permission enables to create certificates and access own certificates.</li>
                <li><strong>device-mgt &gt; admin &gt; certificates</strong>: These permissions ensure that a user is able to access all available certificates. Users with these permissions can:<ul>
                    <li>View all certificates in a list view and in a detailed view</li>
                    <li>Create and remove certificates</li>
                    <li>Verify certificates: This allows an authorized user to authenticate and authorize a device by implementing on-behalf-of authentication.</li>
                  </ul>
                </li>
              </ul>
              
                
                  <p>For more information on managing certificates with the Entgra IoT Server console, see <a href="/doc/en/lb2/Managing-Client-Side-Mutual-SSL-Certificates.html" data-linked-resource-id="352821129" data-linked-resource-version="1" data-linked-resource-type="page">Managing Client Side Mutual SSL Certificates</a>.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>
            
              <p>The monitoring frequency is configured under the general platform configurations in Entgra IoT Server. The IoT server uses this parameter to determine how often the devices enrolled with Entgra IoT Server need to be monitored.</p>
              <p>This permission enables users to configure, update and view the general platform configurations in Entgra IoT Server. In the general platform configurations, you need to define the monitoring frequent, which is how often the IoT server communicates with the device agent.</p>
              
                
                  <p>For more information, see <a href="/doc/en/lb2/General-Platform-Configurations.html" data-linked-resource-id="352821197" data-linked-resource-version="1" data-linked-resource-type="page">General Platform Configurations</a>.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            
              <p><strong>Manage devices</strong></p>
              <p><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image" width="310" src="352820165.png?width=310" data-image-src="attachments/352820123/352820165.png" data-unresolved-comment-count="0" data-linked-resource-id="352820165" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="RolePermissions_DeviceManagement.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="3b31d63b-6ef4-48a2-b7cf-0b4257862381" data-media-type="file"></span></p>
            
          </td>
          <td>
            <ul>
              <li><strong>device-mgt &gt; any-device &gt; permitted-actions-under-owning-device</strong>: This permission enables you to view and manage all the devices shared with you.</li>
              <li><strong>device-mgt &gt; devices &gt; owning-device</strong>: These permissions enable users to:<ul>
                  <li>Enroll and disenroll devices</li>
                  <li>Publish events received by the device client, to the analytics profile</li>
                  <li>Setup geofencing alerts</li>
                  <li>Modify device details such as name and description</li>
                  <li>Retrieve analytics for devices</li>
                </ul>
              </li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>This permission enables you to disenroll or unregister Android and Windows devices.</td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>This permission enables you to enroll or register Android, iOS and Windows devices with Entgra IoT Server.</td>
        </tr>
        <tr>
          <td>
            
              <p><strong>Device status</strong></p>
              <p><strong><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="190" src="352820183.png?width=190" data-image-src="attachments/352820123/352820183.png" data-unresolved-comment-count="0" data-linked-resource-id="352820183" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="RolePermissions_ChangeDeviceStatus.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="8fc809a3-4006-43ad-9c1e-b0efc27a89d5" data-media-type="file"></span><br></strong></p>
            
          </td>
          <td>This permission enables you to change a device status.</td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>Entgra IoT Server offers various device operations based on the mobile platform. This permission enables users to view and carry out device operations on their devices. Expand the preferred platform and select the operations that need to be enabled for users that belong to the role&nbsp;you are creating.</td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>
            
              <p>In Entgra IoT Server the settings can be customized for each platform. This permission enables you to maintain and customize the notification type, notification frequency, and the End User License Agreement (EULA) to suit the requirement of Android, iOS, and Windows mobile platform.</p>
              
                
                  <p>For more information, see <a href="https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822931/Android+Platform+Configurations" data-linked-resource-id="352822931" data-linked-resource-version="1" data-linked-resource-type="page">Android platform settings</a>, <a href="https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352824279/iOS+Platform+Configurations" data-linked-resource-id="352824279" data-linked-resource-version="1" data-linked-resource-type="page">iOS platform settings</a> and <a href="https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352824790/Windows+Platform+Configurations" data-linked-resource-id="352824790" data-linked-resource-version="1" data-linked-resource-type="page">Windows platform settings</a>.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>
            
              <p>The failure to carry out operations will be notified to the Entgra IoT Server administrator and the device owner. This permission enables you to view the notifications that were sent.</p>
              
                
                  <p>For more information on how it works, see <a href="/doc/en/lb2/Checking-Notifications.html" data-linked-resource-id="352821258" data-linked-resource-version="1" data-linked-resource-type="page">Checking Notifications</a>.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>
            
              <p>In Entgra IoT Server, you can define policies, which include a set of configurations. Entgra IoT Server policies are enforced on the Entgra IoT Server users' devices when new users register with the Entgra IoT Server. The Entgra IoT Server&nbsp;policy settings will vary based on the mobile OS type.</p>
              <p>This permission enables you to add, modify, view, publish, unpublish and remove policies.</p>
              
                
                  <p>For more information on working with policies, see Managing Policies.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>
            
              <p>Entgra IoT Server allows you to create new customized roles.&nbsp;This permission enables you to add, modify, view and remove roles.</p>
              
                
                  <p>For more information on working with roles, see <a href="/doc/en/lb2/Managing-Roles.html" data-linked-resource-id="352820118" data-linked-resource-version="1" data-linked-resource-type="page">Managing Roles</a>.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>
            
              <p>Entgra IoT Server allows you to create and manage users. This permission enables you to add, modify, view and remove users.</p>
              
                
                  <p>For more information on working with users, see <a href="/doc/en/lb2/Managing-Users.html" data-linked-resource-id="352820590" data-linked-resource-version="1" data-linked-resource-type="page">Managing Users</a>.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            
              <p><strong>Manage groups</strong></p>
              <p><strong><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image" width="220" src="352820171.png?width=220" data-image-src="attachments/352820123/352820171.png" data-unresolved-comment-count="0" data-linked-resource-id="352820171" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="RolePermissions_GroupManagement.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="e2db1705-d224-4640-97f6-52e8c226ed4b" data-media-type="file"></span><br></strong></p>
            
          </td>
          <td>
            <p>These permissions enable you to manage groups pertaining to devices and user roles. The user role related permission enables viewing all user roles available in Entgra IoT Server. The device related permissions enable you to:</p>
            <ul>
              <li>Create and remove device groups</li>
              <li>Assign devices to a group</li>
              <li>Remove devices from a group</li>
              <li>View the list of groups attached to a device</li>
              <li>View the list of roles that have access to a group</li>
              <li>View the groups accessible by the logged in user</li>
            </ul>
          </td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>
            
              <p>You are able to create mobile apps in the App Publisher that is available in Entgra IoT Server. In order to create, publish, delete, install and update mobile applications the required permissions must be selected.</p>
              <p>To enable users to subscribe to applications and install an application on a device via the App Store you need to select Subscribe that is under the Web App permissions.</p>
              
                
                  <p>For more information see the sections given below:</p>
                  <ul>
                    <li><a href="https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352813326/Mobile+Application+Management" data-linked-resource-id="352813326" data-linked-resource-version="1" data-linked-resource-type="page">Creating Mobile Applications.</a></li>
                    <li><a href="https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352813484/Installing+an+Application+on+a+Device" data-linked-resource-id="352813484" data-linked-resource-version="1" data-linked-resource-type="page">Installing Mobile Apps</a>.</li>
                  </ul>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            
              <p><strong>Device type management</strong></p>
              <p><strong><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image" width="270" src="352820148.png?width=270" data-image-src="attachments/352820123/352820148.png" data-unresolved-comment-count="0" data-linked-resource-id="352820148" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="RolePermissions_DeviceTypeManagement.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="b1475769-f8cd-44f7-be1c-324180b449b0" data-media-type="file"></span><br></strong></p>
            
          </td>
          <td>
            
              <p>Following permissions enable managing device types:</p>
              <ul>
                <li><strong>device-mgt &gt; device-type &gt; add</strong>: This enables the ability to add or delete event definitions for device types.</li>
                <li><strong>device-mgt &gt; devicetype &gt; deploy</strong>: This enables deploying device type components via API. It is recommended to grant this permission to device admin users.</li>
              </ul>
              
                
                  <p>For more information on event definitions, see <a href="https://docs.wso2.com/display/IoTS310/Creating+a+New+Event+Stream+and+Receiver" class="external-link" rel="nofollow">Creating a New Event Stream and Receiver</a>.</p>
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            
              <p><strong>Authorization management</strong></p>
              <p><strong><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="300" src="352820177.png?width=300" data-image-src="attachments/352820123/352820177.png" data-unresolved-comment-count="0" data-linked-resource-id="352820177" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="RolePermissions_AuthorizationManagement.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352820123" data-linked-resource-container-version="1" data-media-id="5b8f6bad-9ae6-416d-a85f-53351b6fdaca" data-media-type="file"></span></strong></p>
            
          </td>
          <td>Users with this permission can check whether a user has the permission to access and manage a device. It is recommended to grant this permission to device admin users.</td>
        </tr>
      </tbody>
    </table>

5.  Click **Update Role Permission**.


# Configuring Role Permissions



This section provides details on how to configure permissions by defining permissions to an API and the permissions associated with the APIs.

### Defining permissions for APIs

If you wish to create additional permission, follow the steps given below:

1.  Navigate to the JAX-RS web application that of your device types API folder. For more information, see the [permission XML file of the virtual fire-alarm.](https://github.com/wso2/carbon-device-mgt-plugins/blob/v4.0.55/components/device-types/virtual-fire-alarm-plugin/org.wso2.carbon.device.mgt.iot.virtualfirealarm.api/src/main/webapp/META-INF/permissions.xml)
2.  Define the new permission using the `@permission` annotation.  
    The `scope` defines to whom the API is limited to and the `permission` that is associated with a given API.  
    Example:

    `@Permission(scope = "virtual_firealarm_user", permissions = {"/permission/admin/device-mgt/user/operations"})`

3.  Restart Entgra IoT Server and you will see the new permission created in the permission tree.  
    Now only users who have this specific permission assigned to them will be able to control the buzzer of the fire-alarm.

### Permission APIs

Let's take a look at the default permissions associated with the APIs.


# Default Roles and Permissions


By default, Entgra IoTS includes a set of roles. These default roles and permissions have been explained in the following subsections.

### Default user roles

The following roles are available by default in Entgra IoTS:

*   **admin **- Role assigned to the super tenant administrator by default.

    

    

    If you are defining the permissions for an IoTS administrator who needs to perform operations and configure policies, make sure to select **admin**. The **admin** permission allows the user to perform operations and configure policies for devices.

    

    

    

     If you wish to create a user with administrative permission other than the default administrator in Entgra IoTS, follow the steps given below:

    1.  [Add a new a role](/doc/en/lb2/Adding-a-Role-and-Permissions.html).
    2.  Configure role permissions by specifically selecting the **admin** permission.

    

    

    

    

*   **internal-devicemgt-user** - This is a system reserved role with the minimum set of permissions to carry out operations. When a user creates an account before accessing the device management console the user is assigned the internal-device-mgt role by default.

### Permissions associated with user roles

<table>
  <tbody>
    <tr>
      <th>User role</th>
      <th>Allows Actions</th>
    </tr>
    <tr>
      <td>admin</td>
      <td>The super tenant administrator belongs to this role. By default, a super tenant administrator will have full control on all the device management consoles.</td>
    </tr>
    <tr>
      <td>devicemgt-user</td>
      <td>
        <p>Carryout external operations on a device based on the permissions assigned via the permission tree.</p>
        <p>Example: getting device details, registering a device control the buzzer and many more.</p>
      </td>
    </tr>
  </tbody>
</table>

# Removing a Role


Follow the instructions below to update a role:

1.  [Sign in to the IoTS device management console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles#AccessingtheEntgraIoTServerConsoles-AccessingtheDeviceManagementConsole) and click the menu icon.  
    ![]({{site.baseurl}}/assets/images/352820436.png)
2.  Click **Role Management**.  
    ![]({{site.baseurl}}/assets/images/352820431.png)
3.  Click **Remove** on the role you wish to remove.  
    ![]({{site.baseurl}}/assets/images/352820451.png)Click **REMOVE** to confirm that you want to remove the role.  
    ![]({{site.baseurl}}/assets/images/352820457.png)



# Searching, Filtering and Soring Roles


### Searching for users

Follow the instructions given below to search for roles:

1.  [Sing in to the Entgra IoTS device management console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles#AccessingtheEntgraIoTServerConsoles-AccessingtheDeviceManagementConsole), and click the menu icon.  
    ![]({{site.baseurl}}/assets/images/352820480.png)

2.  Click **Role Management**.  
    ![]({{site.baseurl}}/assets/images/352820490.png)
3.  Search for roles using the search bar.  
    ![]({{site.baseurl}}/assets/images/352820485.png)

### Filtering users

Follow the instructions below to filter roles:

1.  [Sing in to the Entgra IoTS device management console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles#AccessingtheEntgraIoTServerConsoles-AccessingtheDeviceManagementConsole), and click the menu icon.  
    ![]({{site.baseurl}}/assets/images/352820480.png)

2.  Click **Role Management**.  
    ![]({{site.baseurl}}/assets/images/352820490.png)
3.  Filter the roles by the role name.  
    ![]({{site.baseurl}}/assets/images/352820495.png)

### Sorting roles

Follow the instructions below to sort roles:

1.  [Sing in to the Entgra IoTS device management console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles#AccessingtheEntgraIoTServerConsoles-AccessingtheDeviceManagementConsole), and click the menu icon.  
    ![]({{site.baseurl}}/assets/images/352820480.png)

2.  Click **Role Management**.  
    ![]({{site.baseurl}}/assets/images/352820490.png)
3.  Click the sort icon and click **By Role Name **to sort the roles in the alphabetical order or vice versa.  
    **![]({{site.baseurl}}/assets/images/352820500.png)**


# Updating a role 


Follow the instructions below to update a role:

1.  Sign in to the Entgra IoTS device management console and click the menu icon.  
    ![]({{site.baseurl}}/assets/images/352820520.png)
2.  Click **Role Management**.  
    ![]({{site.baseurl}}/assets/images/352820531.png)
3.  Click **Edit** on the role you wish to update.  
    ![]({{site.baseurl}}/assets/images/352820525.png)
4.  Update the required filed and click Update Role.  

    *   **Domain**: Provide the user store type from the list of items.
    *   **Role Name**: Provide the role name.

        ![]({{site.baseurl}}/assets/images/352820536.png)



# Updating Role Permissions


Follow the instructions below to configure the role permissions:

1.  [Sign in to the IoTS device management console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles#AccessingtheEntgraIoTServerConsoles-AccessingtheDeviceManagementConsole) and click the menu icon.![]({{site.baseurl}}/assets/images/1.png)
2.  Click **Role Management**.  
    ![]({{site.baseurl}}/assets/images/352820572.png)
3.  Click **Edit Permissions** on the role you wish to configure.  
    ![]({{site.baseurl}}/assets/images/352820561.png)

4.  Select or remove the permissions as required.   
    As the permissions are categorized, when the main permission category is selected, all its sub-permissions will get selected automatically. 

    

    

    If you are defining the permission for an administrator, make sure to select **admin**.

    

    

    

    

    For more information on the APIs associated with the permissions, see [Permission APIs](/doc/en/lb2/Configuring-Role-Permissions.html#ConfiguringRolePermissions-PermissionAPIs). 

    

    

    ![]({{site.baseurl}}/assets/images/352820567.png)

5.  click **Update Role Permissions**.


# Using email Address as the username


Entgra products can be configured to authenticate users using their attributes. For example, you can use attributes such as email or mobile number to log in instead of logging in using the username. This topic provides instructions on how to set up Entgra IoT Server to authenticate users using their email address. 

1.  Open the <`IOTS_HOME>/conf/carbon.xml` file.
2.  Find and uncomment the `EnableEmailUserName` configuration to enable email authentication.

    `<EnableEmailUserName>true</EnableEmailUserName>`

3.  Open the`<IOTS_HOME>/conf/``identity/`identity-mgt.properties`` file and set the following property to `true` to enable hashed usernames. 

    

    

    This step is required due to a known issue that prevents the confirmation codes from being removed after they are used, when email usernames are enabled. This occurs because the '@' character and some special characters are not allowed in the registry. To overcome this issue, enable hashed usernames when saving the confirmation codes by configuring the properties below.

    

    

    `UserInfoRecovery.UseHashedUserNames=true`

    Optionally, configure the following property to determine which hash algorithm to use.

    `UserInfoRecovery.UsernameHashAlg=SHA-1`

4.  Open the<`IOTS_HOME>/conf/user-mgt.xml` file and add the following property under the relevant user store manager tag. This property specifies the username validation that will be enforced when the `EnableEmailUserName` option is enabled, using regex (regular expression). 

    

    

    **Tip:** The`user-mgt.xml`file consists of configurations for the primary user store. To configure this for a secondary user store, modify the relevant user store configuration file found in the `<IOTS_HOME>/deployment/server/userstores`directory instead.

    

    

    **Sample regex**

    `<Property name="UsernameWithEmailJavaScriptRegEx">^[\S]{3,30}$</Property>`

5.  

    Configure the following parameters in the `user-mgt.xml` file under the relevant user store manager tag, depending on the type of user store you are connected to (LDAP/Active Directory/ JDBC).  
    For more information on how to configure a user store, see [Configuring User Stores](about:blank#).  

    

    JDBC User Store

    

    Note the following when configuring the user store manager:

    *   The properties listed below are not available by default for the `JDBCUserStoreManager`. Therefore, if you are using a JDBC-based user store, you only need to replace the following property value under the user store tag. This property allows you to add special characters such as "@" in the username.

        `<Property name="UsernameJavaScriptRegEx">^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$</Property>`

        For any other type of user store, configure the properties listed below accordingly.

    *   If the `UsernameWithEmailJavaScriptRegEx` property has a regular expression including the "@" symbol, it is not necessary to configure the `UsernameJavaRegEx` and `UsernameJavaScriptRegEx` properties. The priority order to configure username regular expression properties, are as follows:

        1.  `UsernameJavaRegEx`
        2.  `UsernameWithEmailJavaScriptRegEx`

    

    

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th>Parameter</th>
          <th>Description</th>
        </tr>
        <tr>
          <td>
            <p><code>UserNameAttribute</code></p>
            <p><br></p>
          </td>
          <td>
            
              <p>Set the mail attribute of the user.</p>
              
                
                  
                
              
            
          </td>
        </tr>
        <tr>
          <td><code>UserNameSearchFilter</code></td>
          <td>
            
              <p>Use the mail attribute of the user instead of <code>cn</code> or <code>uid</code>.</p>
              
                
                  
                
              
            
          </td>
        </tr>
        <tr>
          <td><code>UserNameListFilter</code></td>
          <td>
            
              <p>Use the mail attribute of the user.</p>
              
                
                  
                
              
            
          </td>
        </tr>
        <tr>
          <td><code>UserDNPattern</code></td>
          <td>
            
              <p>This parameter is used to speed up the LDAP search operations. You can comment out this config.</p>
              
                
                  
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            
            <p><br></p>
          </td>
          <td>
            
              <p>Change this property under the relevant user store manager tag as follows. This property allows you to add special characters like "@" in the username.</p>
              
                
                  
                
              
            
          </td>
        </tr>
        <tr>
          <td>
            
          </td>
          <td>
            
              <p>This is a regular expression to validate usernames. By default, strings have a length of 5 to 30\. Only non-empty characters are allowed. You can provide ranges of alphabets, numbers and also ranges of ASCII values in the RegEx properties.</p>
              
                
                  
                
              
            
          </td>
        </tr>
        <tr>
          <td>Realm configurations</td>
          <td>
            
              <p>The <code>AdminUser</code> username must use the email attribute of the admin user.</p>
              
                
                  &lt;AdminUser&gt;
             &lt;UserName&gt;admin@wso2.com&lt;/UserName&gt;
             &lt;Password&gt;admin&lt;/Password&gt;
    &lt;/AdminUser&gt;
                
              
            
          </td>
        </tr>
      </tbody>
    </table>

    

### Try it out

To try out this scenario, create a new user with email address as the username and attempt to login to the Entgra IoT Server device management application using the new user's credentials. 

1.  Start the Entgra IoT Server and login to the [device management console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles#AccessingtheEntgraIoTServerConsoles-AccessingtheDeviceManagementConsole) via [https://localhost:9443/devicemgt.](https://localhost:9443/devicemgt.) 
2.  Click **Add** under **Users**. 
3.  Enter the required user details and click **Add User**.  
    Use an email address as the username when creating the new user, as seen below.   
    ![]({{site.baseurl}}/assets/images/352820903.png)
4.  Login to the management console via [https://localhost:9443/carbon.](https://localhost:9443/carbon.) 
5.  Click on **List** under **Users and Roles** on the **Main** tab, and then click **Users**. 
6.  The new user you created will be listed. Click **Change Password** to set a new password for the user. ![]({{site.baseurl}}/assets/images/6.png)
7.  Enter a new password and click **Finish**. 
8.  Access the device management console again and login using the new user's credentials:
    *   Username: [kimwso2@gmail.com](mailto:kimwso2@gmail.com)
    *   Password: kimwso2123![]({{site.baseurl}}/assets/images/2123.png)  
    You have successfully configured using email address as the username, and logged in using an email address.
