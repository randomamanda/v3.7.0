---
layout: default
title: Configuring Entgra IoT Server with Entgra Data Analytics Server  
parent: Product administration
grand_parent: Using Entgra IoT Server
nav_order: 19
---

# Configuring Entgra IoT Server with Entgra Data Analytics Server


The Entgra IoT Server is packaged with an analytics profile to handle device monitoring and analytics. The following section provides an alternative to using the analytics profile, and guides you through using WSO2 Data Analytics Server (WSO2 DAS) for analytics with the Entgra IoT Server device management profile. This is useful in a production environment where a WSO2 DAS node is already in use and you want to use the Entgra IoT Server device management profile with it. 

Follow the steps below to configure Entgra IoT Server with WSO2 DAS for analytics. 



Prerequisite



This option is only available for the **WUM-updated** WSO2 DAS 3.1.0 pack or a later version as it requires some fixes that are missing in the vanilla pack.

1.  Download [WSO2 Data Analytics Server 3.1.0 or a later version](http://wso2.com/smart-analytics#download).

2.  Update WSO2 DAS using WUM. For more information on how to do this, see [Getting Started with WUM](about:blank#).

3.  Extract the WUM-updated DAS pack found in your WUM update repository. The unzipped file will be referred to as `<DAS_HOME>` throughout this documentation.





1.  Download the following [feature installation pom file](attachments/352821791/352821804.pom) and place it inside the `<DAS_HOME>` directory.

2.  Navigate to `<DAS_HOME>` on the terminal and execute the following command. This will install the required features to the Entgra IoT Server pack. 

    `mvn clean install -f analytics-feature-installation.pom`

3.  Open the `wso2server.sh` file found in the `<DAS_HOME>/bin` directory and add the following environment variables to the end of the file. These environment variables are used as a global configuration to specify the IP addresses of the different entities (the message broker, device manager and key manager) that need to be connected. 

    ```
    -Dmqtt.broker.host="localhost" \
    -Dmqtt.broker.port="1886" \
    -Diot.keymanager.host="localhost" \
    -Diot.keymanager.https.port="9443" \
    -Diot.gateway.host="localhost" \
    -Diot.gateway.https.port="8243" \
    ```

4.  Using WSO2 DAS for IoT analytics requires communication with different servers. To enable server-to-server communication, open the authenticators.xml file found in the <DAS_HOME>/repository/conf/security directory and add the SignedJWTAuthenticator inside the <Authenticators> tag.  

    ```
    <Authenticator name="SignedJWTAuthenticator" disabled="false">
        <Priority>5</Priority>
    </Authenticator>
    ```

5.  Next, configure the input and output adapters that are used to handle the protocols required for collaboration between devices and WSO2 DAS, as well as between WSO2 DAS and Entgra IoT Server. 

    1.  Open the `input-event-adapters.xml` file found in the `<DAS_HOME>/repository/conf` directory and add the following adapter configuration inside the `<inputEventAdaptersConfig>` tag.

        ```
        <adapterConfig type="oauth-http">
            <property key="minThread">8</property>
            <property key="maxThread">100</property>
            <property key="keepAliveTimeInMillis">20000</property>
            <property key="jobQueueSize">10000</property>
            <property key="maximumHttpConnectionPerHost">2</property>
            <property key="maximumTotalHttpConnection">100</property>
            <property key="tokenValidationUrl">https://${iot.keymanager.host}:${iot.keymanager.https.port}</property>
            <property key="username">admin</property>
            <property key="password">admin</property>
            <property key="tokenUrl">https://${iot.gateway.host}:${iot.gateway.https.port}/token</property>
            <property key="deviceMgtServerUrl">https://${iot.gateway.host}:${iot.gateway.https.port}</property>
            <property key="tokenRefreshTimeOffset">100</property>
        </adapterConfig>
        <adapterConfig type="oauth-mqtt">
            <!-- Thread Pool Related Properties -->
            <property key="minThread">8</property>
            <property key="maxThread">100</property>
            <property key="keepAliveTimeInMillis">20000</property>
            <property key="jobQueueSize">10000</property>
            <property key="connectionKeepAliveInterval">60</property>
            <property key="dcrUrl">https://${iot.keymanager.host}:${iot.keymanager.https.port}/client-registration/v0.11/register</property>
            <property key="url">tcp://${mqtt.broker.host}:${mqtt.broker.port}</property>
            <property key="username">admin</property>
            <property key="password">admin</property>
            <property key="contentValidator">iot-mqtt</property>
            <property key="cleanSession">true</property>
        </adapterConfig>
        ```

    2.  Open the `output-event-adapters.xml` file found in the `<DAS_HOME>/conf` directory and add the following adapter configuration inside the `<outputEventAdaptersConfig>` tag.

        ```
        <adapterConfig type="oauth-mqtt">
            <!-- Thread Pool Related Properties -->
            <property key="minThread">8</property>
            <property key="maxThread">100</property>
            <property key="keepAliveTimeInMillis">20000</property>
            <property key="jobQueueSize">10000</property>
            <property key="connectionKeepAliveInterval">60</property>
            <property key="dcrUrl">https://${iot.keymanager.host}:${iot.keymanager.https.port}/client-registration/v0.11/register</property>
            <property key="tokenUrl">https://${iot.gateway.host}:${iot.gateway.https.port}/token</property>
            <property key="url">tcp://${mqtt.broker.host}:${mqtt.broker.port}</property>
            <property key="username">admin</property>
            <property key="password">admin</property>
            <property key="qos">2</property>
            <property key="clearSession">true</property>
        </adapterConfig>
        <adapterConfig type="secured-websocket">
            <!-- Thread Pool Related Properties -->
            <property key="minThread">8</property>
            <property key="maxThread">100</property>
            <property key="keepAliveTimeInMillis">20000</property>
            <property key="jobQueueSize">10000</property>
            <property key="authenticator">org.wso2.carbon.device.mgt.output.adapter.websocket.authentication.OAuthAuthenticator</property>
            <property key="tokenValidationUrl">https://${iot.keymanager.host}:${iot.keymanager.https.port}</property>
            <property key="maximumHttpConnectionPerHost">2</property>
            <property key="maximumTotalHttpConnection">100</property>
            <property key="authorizer">org.wso2.carbon.device.mgt.output.adapter.websocket.authorization.DeviceAuthorizer</property>
            <property key="tokenUrl">https://${iot.gateway.host}:${iot.gateway.https.port}/token</property>
            <property key="deviceMgtServerUrl">https://${iot.gateway.host}:${iot.gateway.https.port}</property>
            <property key="username">admin</property>
            <property key="password">admin</property>
            <property key="tokenRefreshTimeOffset">100</property>
        </adapterConfig>
        ```

6.  The default keystore used in Entgra IoT Server has been updated from version 3.1.0 onwards. Since all servers must use the same certificate, do the following to update the keystore in WSO2 DAS.

    1.  Navigate to the `<IOTS_HOME>/repository/resources/security` folder and copy the `wso2carbon.jks` file. 
    2.  Paste it in the `<DAS_HOME>/repository/resources/security` folder and replace the existing `wso2carbon.jks` file. 
7.  If you have changed the default port offset of the analytics profile from 2, do the following changes in the Entgra IoT Server.

    1.  Open the `iotserver.sh` file found in the` <IOT_HOME>/bin` directory and update the following environment variables to the relevant endpoints. 

        ```
        -Diot.analytics.host="localhost" \
        -Diot.analytics.https.port="9445" \
        ```

    2.  Open the `analytics-data-config.xml` file found in the `<IOT_HOME>/conf/analytics` directory and point to the relevant DAS endpoint. 

        `<URL>http://localhost:9765</URL>`

    3.  Open the `device-analytics-config.xml` file found in the `<IOT_HOME>/conf/etc` directory and update the receiver URL.

        `<ReceiverServerUrl>tcp://localhost:7613</ReceiverServerUrl>`

8.  Use a port offset to start the WSO2 DAS server. For instance, you can run WSO2 DAS with `portOffset=2`.

    

    

    If you have changed the default port offset of the analytics profile from 2, use the updated port offset value to start the server accordingly.

    

    

    `./wso2server.sh -DportOffset=2`
