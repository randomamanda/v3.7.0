---
layout: default
title: Scheduling the Push Notification Task
parent: Product administration
grand_parent: Using Entgra IoT Server
nav_order: 18
---


# Scheduling the Push Notification Task

Push notifications are used to send messages to devices. If you have configured Entgra IoT Server to push notifications to the devices via [Firebase Cloud Messaging (FCM)](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822967/Android+Notification+Methods) or [Apple Push Notification (APNS)](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352824340/iOS+Notification+Method), the notification is sent to the devices, the devices request for the pending operations, and the server returns the list of pending operations, which the devices then execute.  

When many devices communicate with the server at the same time, the server crashes. To avoid this situation Entgra IoT Server implemented a task to manage the number of devices communicating with the server at a given time. The task sends the notifications to the devices in batches. This way at a given time only a defined number of devices will communicate with the server.  

Let's take a look at how you can configure the push notification scheduling task:

1.  Open the `<IOTS_HOME>/conf/cdm-config.xml` file and make sure the `SchedulerTaskEnabled` that is under `PushNotificationConfiguration` is enabled. This configuration is enabled by default.

    

    

    In a clustered environment make sure to enable this task only in the manager node and not the worker nodes. Else, the server crashes when the worker nodes start pushing notifications along with the manager node. 

    

    

    ```
    <PushNotificationConfiguration>
       <SchedulerBatchSize>1000</SchedulerBatchSize>
       <SchedulerBatchDelayMills>60000</SchedulerBatchDelayMills>
       <SchedulerTaskInitialDelay>60000</SchedulerTaskInitialDelay>
       <SchedulerTaskEnabled>true</SchedulerTaskEnabled>
       <PushNotificationProviders>
          <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.fcm.FCMBasedPushNotificationProvider</Provider>
          <!--<Provider>org.wso2.carbon.device.mgt.mobile.impl.ios.apns.APNSBasedPushNotificationProvider</Provider>-->
          <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.mqtt.MQTTBasedPushNotificationProvider</Provider>
          <Provider>org.wso2.carbon.device.mgt.extensions.push.notification.provider.xmpp.XMPPBasedPushNotificationProvider</Provider>
       </PushNotificationProviders>
    </PushNotificationConfiguration>
    ```

    <table>
      <colgroup>
        <col>
        <col>
      </colgroup>
      <tbody>
        <tr>
          <th><code>SchedulerBatchSize</code></th>
          <td><span style="color: rgb(34,34,34);">Define how many devices or the device batch size a notification needs to be sent at a given time.<br>By default, a notification is sent to 200 devices at a given time.&nbsp;If you have less than 200 devices registered with Entgra IoT Server, the notification will be sent to all those devices.</span></td>
        </tr>
        <tr>
          <th><code>SchedulerBatchDelayMills</code></th>
          <td><span style="color: rgb(34,34,34);">Define after how many milliseconds the notifications needs to be sent to the next batch&nbsp;of devices.<br></span>By default, the next batch of devices will get the notification after 60,000 milliseconds, which is in a minute.</td>
        </tr>
        <tr>
          <th><code>SchedulerTaskInitialDelay</code></th>
          <td><span style="color: rgb(34,34,34);">The server will not send notifications to the devices soon as it starts. Therefore, define when the push notification schedule task needs to start. The value needs to be defined in milliseconds.<br></span>By default, the task starts 60,000 milliseconds or one minute after the server starts.</td>
        </tr>
        <tr>
          <th><code>SchedulerTaskEnabled</code></th>
          <td>Enable the push notification scheduler task. If it is is not enabled, Entgra IoT Server will send the push notifications to all the registered devices in one go. This might result in the server crashing as explained above.</td>
        </tr>
        <tr>
          <th><code>PushNotificationProviders</code></th>
          <td><span style="color: rgb(34,34,34);">The default push notifications that are supported and implemented in Entgra IoT Server.</span></td>
        </tr>
      </tbody>
    </table>

2.  Configure the device type to run the push notification scheduler task.  
    Navigate to the `<IOTS_HOME>/repository/deployment/server/devicetype` directory, open the `<DEVICE_TYPE>.xml` file, and configure the field given below:  
    The default configuration in the `android.xml` file is shown below:

    `<PushNotificationProvider type="FCM" isScheduled="false">--> <!--</PushNotificationProvider>`

    

    

    The task for the FCM push notification method is disabled for the Android device type by default. Therefore, all the Android devices registered with Entgra IoT Server will receive the push notifications at once and the devices will communicate with the server at the same time. If you want to change this default behavior, enable the scheduler task for the device type.

    `<PushNotificationProvider type="FCM" isScheduled="true">--> <!--</PushNotificationProvider>`

     **NOTE**: In Entgra IoT Server, the Android devices are configured to send notifications via the [local polling notification method](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822967/Android+Notification+Methods#AndroidNotificationMethods-Local) by default.

    

    
