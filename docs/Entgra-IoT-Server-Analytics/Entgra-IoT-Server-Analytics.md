---
layout: default
title: Entgra IoT Server Analytics
parent: Using Entgra IoT Server
has_children: true
nav_order: 1

---

# Entgra IoT Server Analytics
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}



The following sections describe how you can use Entgra IoT Server analytics.

*   **[Understanding the Entgra IoT Server Analytics Framework](https://docs.wso2.com/display/IOTS320/Understanding+the+WSO2+IoT+Server+Analytics+Framework)**
*   **[Monitoring Devices Using Location Based Services](https://docs.wso2.com/display/IOTS320/Monitoring+Devices+Using+Location+Based+Services)**
*   **[Publishing Operation response to Analytics](https://docs.wso2.com/display/IOTS320/Publishing+Operation+response+to+Analytics)**