---
layout: default
title: Monitoring Devices Using Location Based Services
parent: Entgra IoT Server Analytics
grand_parent: Using Entgra IoT Server
nav_order: 1
---
## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

# Monitoring Devices Using Location Based Services
{: .no_toc }


Location-based services (LBS) uses real-time location or geo-data from devices to monitor and track devices. The geo extension in Entgra IoT Server provides real-time information about geospatial objects. It processes spatial data from an external source of events and analyzes/manipulates this data to produce meaningful information to end users. You can interact with it to generate a variety of alerts and warnings. 

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Alert type</th>
      <th>Description</th>
    </tr>
    <tr>
      <td><a href="#MonitoringDevicesUsingLocationBasedServices-Geofenceexitalert">Exit fence alert</a></td>
      <td>You can specify a geo area and if the configured device leaves the specified area, an alert is generated.</td>
    </tr>
    <tr>
      <td><a href="#MonitoringDevicesUsingLocationBasedServices-Geofenceenteralert">Within alert (Enter fence alert)</a></td>
      <td>You can specify a geo area and if the configured device comes into the specified area, an alert is generated.</td>
    </tr>
    <tr>
      <td><a href="#MonitoringDevicesUsingLocationBasedServices-Stationaryalert">Stationery alert&nbsp;</a></td>
      <td>You can specify a geo area, a fluctuation radius, and a time, which generates an alert if the configured device is located in that area during the specified time. The fluctuation radius is used to minimize the fluctuation effect of the device.</td>
    </tr>
    <tr>
      <td><a href="#MonitoringDevicesUsingLocationBasedServices-Speedalert">Speed alert</a></td>
      <td>You can specify a maximum speed limit for the movement of the device. If the device exceeds the specified speed, an alert is generated.</td>
    </tr>
  </tbody>
</table>

The following sections guide you through installing the geo extension, using geofencing, and setting up alerts. 

### Setting up geofencing

1.  [Download the Entgra IoT Server](http://wso2.com/iot).

    

    

    If you are running Entgra IoT Server, stop the server before proceeding to the next step.

    

    

2.  Navigate to the `<IOTS_HOME>/wso2/analytics/scripts` directory via the terminal and run the following command. This installs the geo extension feature to Entgra IoT Server.

    `mvn clean install -f siddhi-geo-extention-deployer.xml`

3.  Open the `<IOTS_HOME>/conf/`cdm-config.xml`` file and enable the `GeoLocationConfiguration` and `PublishLocationResponse` configurations.

    This enables the geo extension feature in Entgra IoT Server.

    ```
    <GeoLocationConfiguration>
        <Enabled>true</Enabled>
    </GeoLocationConfiguration>
    <OperationAnalyticsConfiguration>
        <PublishLocationResponse>true</PublishLocationResponse>
        <PublishDeviceInfoResponse>false</PublishDeviceInfoResponse>
        <PublishOperationResponse>
            <Enabled>false</Enabled>
            <Operations>
                <!-- Publish specific operation responses -->
                <!--
                    <Operation>BATTERY_LEVEL</Operation>
                    <Operation>CHECK_LOCK_STATUS</Operation>
                -->
                <!-- use wildcard '*' to publish all responses -->
                <Operation>*</Operation>
            </Operations>
        </PublishOperationResponse>
    </OperationAnalyticsConfiguration>
    ```

4.  Start the Entgra IoT Server core, and analytics profiles, and sign in to the Entgra IoT Server Device Management Console.

    

    

    

    

    1.  Start the Entgra IoT Server core profile.

        ```
        cd <IOTS_HOME>/bin
        sh iot-server.sh
        ```

    2.  Next, start the Entgra IoT Server analytics profile.

        ```
        cd <IOTS_HOME>/bin
        sh analytics.sh
        ```

    3.  Access the device management console.

        *   For access via secured HTTPS: `https://<IOTS_HTTPS_HOST>:<IOTS_HTTPS_PORT>/devicemgt/ `

        *   For example:` https://localhost:9443/devicemgt/ `
    4.  Enter the username and password, and sign in.

        

        

        The system administrator will be able to log in using `admin` for both the username and password. However, other users will have to first register with Entgra IoT Server before being able to log into the IoT Server device management console. For more information on creating a new account, see [Registering with Entgra IoT Server](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819690/Registering+with+Entgra+IoT+Server).

        

        

        ![]({{site.baseurl}}/assets/images/352822708.png)

    5.  Click **LOGIN**. The respective device management console will change, based on the permissions assigned to the user.  
        For example, the device management console for an administrator is as follows:

        ![]({{site.baseurl}}/assets/images/352822702.png)

    

    

    

    

    Deploying Analytics Artifacts

    

    The geo analytics artifacts needs to be deploy per tenant manually. 

    1.  Log in to the device management console using admin credentials.

    2.  Click the![]({{site.baseurl}}/assets/images/2.png) button and select **Configuration Management > Platform Configurations**.

    3.  Click the **Deploy Geo Analytics Artifacts** button. If required, you can use this button to re-deploy the geo analytics artifacts in the super tenant mode.  
        ![]({{site.baseurl}}/assets/images/352822690.png)

    

    

### Enroll devices with Entgra IoT Server

Follow the steps given below to enroll a device:

1.  Enroll a device. Let's enroll an Android device for this tutorial.

    

    

    For more information on how to enroll an Android device, see [Android](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352816232/Android).

    For more information on how to enroll other sample device types, see [Enrolling Devices](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352816226/Enrolling+Devices). 

    

    

2.  Access the geofencing map for the device you just enrolled:
    1.  Click the![]({{site.baseurl}}/assets/images/1.png) icon, and then click **Device Management**.
    2.  Click view on the device you just enrolled and select the **Location** tab. 

### Setting alerts

Geofencing alerts allow users to set pop-up alerts when a device enters or exits the defined geometry boundaries. You can set up the following alerts using the Entgra IoT Server geo extensions.

#### Geofence exit alert

A geofence exit alert notifies the user when the device leaves the specified geofence area. For example, a school administrator may set a geofence exit alert for the school laboratory's tablet devices so that when a device leaves the school premises, the admin is alerted and can disable the device. 

Follow the steps given below to define a geo area and set up a geofence exit alert. 

1.  Click on the **Add Geofence Exit Alert** button. 
2.  Define a geo area.

    

    

    

    

    There are two ways to define a geofence area.

    *   **Draw area** - Draw an area on the map to set the boundaries of the geofence.
    *   **Enter area** - Enter a `geoJSON` containing coordinates to set the boundaries of the geofence. You can import a geofence using this option.

    ##### Draw area

    1.  Click **Draw Area.**  
        **![]({{site.baseurl}}/assets/images/352822719.png)**
    2.  Select a shape (pentagon or square) on the drawing palette that is on the left corner of the map.  
        ![]({{site.baseurl}}/assets/images/352822637.png)
    3.  Select the points on the map to draw a pentagon geofence, or click and drag to draw a square geofence.

        ![]({{site.baseurl}}/assets/images/352822620.png)
    4.  Enter an appropriate name for the geofence, and do one of the following actions:

        1.  Click **Save** to save your geofence.

        2.  Click **Edit** to edit the `geoJSON` that is generated when you draw the geofence.
        3.  Click **Export** to export the geofence and download a `geoJSON` file. You can use this file to import the same geofence to a different device.

    ##### Enter area (Import area)

    To enter or import an area using a `geoJSON`, you have to first draw an area using the map, as shown above, and then export it.

    

    

    Entering an area using a `geoJSON` is useful when you want to import the exact same geofence for another device or another system. You can draw the geofence in the first device and then copy over the same fence with the exact coordinates to define the same geofence for a different device.

    

    

    1.  Draw an area using the map, as shown above.
    2.  Enter an appropriate name for the geofence and click **Export**. A `geoJSON` file downloads on to your machine.
    3.  Open the JSON file and copy the JSON code.
    4.  Click **Enter** **Area.** ![]({{site.baseurl}}/assets/images/4.png)
    5.  Do one of the following to import the geofence:

    1.  *   Click **Choose File** and select the JSON file you exported in step b. Click **Import**.

        *   Enter the JSON you copied in step c to define the geofence boundary and click **Import**.The code block below shows a sample JSON.

            ```js
            {
              "type": "Feature",
              "properties": {},
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      79.85129186650738,
                      6.910663750244661
                    ],
                    [
                      79.85129186650738,
                      6.911776767011602
                    ],
                    [
                      79.85373804112896,
                      6.911776767011602
                    ],
                    [
                      79.85373804112896,
                      6.910663750244661
                    ],
                    [
                      79.85129186650738,
                      6.910663750244661
                    ]
                  ]
                ]
              }
            }
            ```

    

    

    

#### Geofence enter alert

A geofence enter alert notifies the user when the device enters the specified geofence area. For example, an employer may set up a geofence enter alert to a room that contains files with highly classified information in them to notify him or prevent certain employees with restricted access from entering the room. 

Follow the steps below to define a geo area and set up a geofence enter alert. 

1.  Click on the **Add Geofence Enter Alert** button. 
2.  Define a geo area.

    

    

    

    

    There are two ways to define a geofence area.

    *   **Draw area** - Draw an area on the map to set the boundaries of the geofence.
    *   **Enter area** - Enter a `geoJSON` containing coordinates to set the boundaries of the geofence. You can import a geofence using this option.

    ##### Draw area

    1.  Click **Draw Area.**  
        **![]({{site.baseurl}}/assets/images/352822655.png)**
    2.  Select a shape (pentagon or square) on the drawing palette found on the left corner of the map.  
        ![]({{site.baseurl}}/assets/images/352822637.png)
    3.  Select the points on the map to draw a pentagon geofence, or click and drag to draw a square geofence.

        ![]({{site.baseurl}}/assets/images/352822620.png)
    4.  Enter an appropriate name for the geofence, and do one of the following actions:

        1.  Click **Save** to save your geofence.

        2.  Click **Edit** to edit the `geoJSON` that is generated when you draw the geofence.
        3.  Click **Export** to export the geofence and download a `geoJSON` file. You can use this file to import the same geofence to a different device.

    ##### Enter area (Import area)

    To enter or import an area using a `geoJSON`, you have to first draw an area using the map, as shown above, and then export it.

    

    

    Entering an area using a `geoJSON` is useful when you want to import the exact same geofence for another device or another system. You can draw the geofence in the first device and then copy over the same fence with the exact coordinates to define the same geofence for a different device.

    

    

    1.  Draw an area using the map, as shown above.
    2.  Enter an appropriate name for the geofence and click **Export**. A `geoJSON` file downloads on to your machine.
    3.  Open the JSON file and copy the JSON code.
    4.  Click **Enter** **Area.** ![]({{site.baseurl}}/assets/images/4.png)
    5.  Do one of the following to import the geofence:

    1.  *   Click **Choose File** and select the JSON file you exported in step b. Click **Import**.

        *   Enter the JSON you copied in step c to define the geofence boundary and click **Import**.The code block below shows a sample JSON.

            ```js
            {
              "type": "Feature",
              "properties": {},
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      79.85129186650738,
                      6.910663750244661
                    ],
                    [
                      79.85129186650738,
                      6.911776767011602
                    ],
                    [
                      79.85373804112896,
                      6.911776767011602
                    ],
                    [
                      79.85373804112896,
                      6.910663750244661
                    ],
                    [
                      79.85129186650738,
                      6.910663750244661
                    ]
                  ]
                ]
              }
            }
            ```

    

    

    

#### Stationary alert

Stationary alerts notify the users about the state changes of a device in a predefined geo area. If a device remains stationed within the defined geo area for a specific time period, an alert is generated. For example, you can use this alert to track a lost phone that you may have left in a taxi cab or a public bus. When the device stops moving, you can identify the taxi/bus location and retrieve your phone. 

1.  Click on the **Add Stationary Alert** button. 
2.  Define a geo area.

    

    

    

    

    There are two ways to define a geofence area.

    *   **Draw area** - Draw an area on the map to set the boundaries of the geofence.
    *   **Enter area** - Enter a `geoJSON` containing coordinates to set the boundaries of the geofence. You can also import a geofence using this option.

    ##### Draw area

    1.  Click **Draw Area.**  
        **![]({{site.baseurl}}/assets/images/352822643.png)**
    2.  Select a shape (pentagon, square, or circle) on the drawing palette found on the left corner of the map.  
        ![]({{site.baseurl}}/assets/images/352822649.png)
    3.  Select points on the map to draw a pentagon geofence, or click and drag to draw a square geofence.

        ![]({{site.baseurl}}/assets/images/352822667.png)

    4.  Enter the following details:

        1.  Fence name - Enter an appropriate name for the geo area.
        2.  Fluctuation radius - Specify a radius. The fluctuation radius is used to minimize the fluctuation effect of the device.
        3.  Time - The device should be stationary for the number of seconds specified in this field to generate a pop-up alert.
    5.  Do one of the following actions:

        1.  Click **Save** to save your geofence.

        2.  Click **Edit** to edit the `geoJSON` that is generated when you draw the geofence.
        3.  Click **Export** to export the geofence and download a `geoJSON` file. You can use this file to import the same geofence to a different device.

    ##### Enter area (Import area)

    To enter or import an area using a `geoJSON`, you have to first draw an area using the map, as shown above, and then export it.

    

    

    Entering an area using a `geoJSON` is particularly useful when you want to import the exact same geofence for another device or another system. You can draw the geofence in the first device and then copy over the same fence with the exact coordinates to define the same geofence for a different device.

    

    

    1.  Draw an area using the map as shown above.
    2.  Enter an appropriate name for the geofence and click **Export**. A `geoJSON` file will be downloaded on to your machine.
    3.  Open the JSON file and copy the JSON code.
    4.  Click **Enter** **Area.** ![]({{site.baseurl}}/assets/images/4.png)
    5.  Do one of the following to import the geofence:

    1.  *   Click **Choose File** and select the JSON file you exported in step b. Click **Import**.

        *   Enter the JSON you copied in step c to define the geofence boundary and click **Import**.The code block below shows a sample JSON.

            ```js
            {
              "type": "Feature",
              "properties": {},
              "geometry": {
                "type": "Polygon",
                "coordinates": [
                  [
                    [
                      79.85129186650738,
                      6.910663750244661
                    ],
                    [
                      79.85129186650738,
                      6.911776767011602
                    ],
                    [
                      79.85373804112896,
                      6.911776767011602
                    ],
                    [
                      79.85373804112896,
                      6.910663750244661
                    ],
                    [
                      79.85129186650738,
                      6.910663750244661
                    ]
                  ]
                ]
              }
            }
            ```

    

    

    

#### Speed alert

Speed alerts notify users when a device is traveling above the predefined speed limit. This alert does not require a geofence and is applied globally (wherever the device is located). For example, a system administrator for a taxi service may use this to monitor whether any of the taxi drivers are driving above the allowed speed limit. 

1.  Click on the **Set Speed Alert** button. 
2.  Specify a global speed limit (km/h).   
    ![]({{site.baseurl}}/assets/images/352822673.png)

## What's next?

*   You can customize the alert or set up the geo extension to send SMS/email alerts instead of pop-up alerts on the geo map. For more information on how to do this.
### Extending and Customizing Location Based Services

The geo extension feature that is used to monitor devices using location-based services can be extended or customized using output adapters available in WSO2 Data Analytics Server. The instructions below guide you through adding the SMS event publisher to your geofencing alert so that you can receive alerts to your phone via SMS instead of seeing alerts on the geofence UI screen. 





Before you begin:

*   [Set up geofencing in Entgra IoT Server.](/doc/en/lb2/Monitoring-Devices-Using-Location-Based-Services.html#MonitoringDevicesUsingLocationBasedServices-Settingupgeofencing)
*   [Set up an alert for a device.](/doc/en/lb2/Monitoring-Devices-Using-Location-Based-Services.html#MonitoringDevicesUsingLocationBasedServices-Settingalerts) (The steps below demonstrate setting up SMS alerts for a **GeoFence Exit Alert**.)





1.  Start the Entgra IoT Server core and analytics profiles in that order.

    

    

    

    1.  Start the Entgra IoT Server core profile.

        ```
        cd <IOTS_HOME>/bin
        sh iot-server.sh
        ```

    2.  Next, start the Entgra IoT Server analytics profile.

        ```
        cd <IOTS_HOME>/bin
        sh analytics.sh
        ```

    

    

2.  Access the Entgra IoT Server analytics management console by navigating to the following URL: [http://localhost:9445/carbon.](http://localhost:9445/carbon.)
3.  Click on **Event > Flow** under the **Manage** section of the **Main** tab. 
4.  You see an event flow diagram similar to the one shown below. Here you can see the event streams and event publishers of the alert you created.  
    You can add new custom output adapters or event publishers to the alert.   
    ![]({{site.baseurl}}/assets/images/352822791.png)
5.  Click **Event > Publishers** under the **Manage** section of the **Main** tab. 
6.  Click **Add Event Publisher** and fill in the required details for the SMS event publisher.  
    ![]({{site.baseurl}}/assets/images/352822781.png)
    1.  **Event Publisher Name -** `SMSAlerts`
    2.  **Event Source -**  `iot.per.device.stream.geo.AlertNotifications:`1.0.0`
    3.  **Output Event Adapter Type -** `sms`
    4.  **Phone No -** `<Enter_Your_Phone_Number>`
    5.  **Message Format -** `text`

        

        

        Click **Advanced** to define custom output mappings based on the **Message Format** you selected. For more information on custom output mapping types, see [Output Mapping Types](https://docs.wso2.com/display/DAS310/Output+Mapping+Types). This is a sample of a custom output mapping for the 'text' message format.

        `Note that this phone is out of range {{information}} {{timeStamp}}.`

        

        

7.  Click **Add Event Publisher. **You have successfully set up SMS alerts for your geofence. 
8.  Navigate to **Event > Flow** under the **Manage** section of the **Main** tab again. You will be able to see the SMS event publisher you added.   
    ![]({{site.baseurl}}/assets/images/352822786.png)  
    To try this out, take the enrolled device out of the geofence boundary. You will now receive an SMS alert for the geofence exit alert that you created. 





You can also add any other output adapters available through WSO2 Data Analytics Server such as the Email Event Publisher. For more information, see [Event Publisher Types](about:blank#) in the WSO2 Data Analytics documentation.




*   Try the [Android Sense](https://docs.wso2.com/display/IoTS310/Android+Sense) tutorial to connect an Android device to Entgra IoT Server and monitor sensor data.   