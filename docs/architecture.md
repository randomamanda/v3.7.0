---
layout: default
title: Architecture
nav_order: 2
---


---
# Architecture
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}
---

[Entgra IoT Server](http://wso2.com/iot) offers an extensible framework for:

*   Device manufacturers to write plugins for various types of devices.
*   Device owners to enroll their devices and start managing them.
*   Enterprises looking to manage employees’ mobile devices (enterprise mobility management). 

Entgra IoT Server is built on top of WSO2 Connected Device Management (CDM) Framework, which in turn is built on the WSO2 Carbon platform.  The following sub sections explain the Entgra IoT Server architecture and how it functions.

### Entgra IoT Server component architecture

A Entgra IoT Server deployment comprises of the following high-level components:

![]({{site.baseurl}}/assets/images/336560955.png)

The following table explains how each component in the Entgra IoT Server architecture functions as depicted in the above figure. 

<table>
  <colgroup>
    <col style="width: 107.0px;">
    <col style="width: 1255.0px;">
  </colgroup>
  <tbody>
    <tr>
      <th>Connected Device Management Core</th>
      <td>
        <div class="content-wrapper">
          <p>The Connected Device Management (CDM) core manages and controls all the functions of Entgra IoT Server. It is designed with many extensions. Therefore, you can extend and customize most of its functions. The CDM core consists of the following components.</p>
          <p><em><strong><strong>Click </strong></strong>on a component and navigate <em>to the respective documentation section </em>for more details.</em></p>
          <p><a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336563059/Device+Management" data-linked-resource-id="336563059" data-linked-resource-version="1" data-linked-resource-type="page"><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="150" src="{{site.baseurl}}/assets/images/336561019.png?width=150" data-image-src="{{site.baseurl}}/assets/images/336561019.png" data-unresolved-comment-count="0" data-linked-resource-id="336561019" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="WSO2-IoT-Core-Components-Device-Management.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="336560926" data-linked-resource-container-version="1" data-media-id="9c1a37c2-421a-4e2e-939c-c5b9b957d3f3" data-media-type="file"></span></a> <a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336561155/Key+Concepts#KeyConcepts-DeviceTypes" data-linked-resource-id="336561155" data-linked-resource-version="1" data-linked-resource-type="page"><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="150" src="{{site.baseurl}}/assets/images/336561025.png?width=150" data-image-src="{{site.baseurl}}/assets/images/336561025.png" data-unresolved-comment-count="0" data-linked-resource-id="336561025" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="WSO2-IoT-Core-Components-Device-Type-Management.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="336560926" data-linked-resource-container-version="1" data-media-id="22fba661-3eea-4135-a84f-01a5cb7fbf39" data-media-type="file"></span></a> <a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336567970/General+Platform+Configurations" data-linked-resource-id="336567970" data-linked-resource-version="1" data-linked-resource-type="page"><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="150" src="{{site.baseurl}}/assets/images/336561013.png?width=150" data-image-src="{{site.baseurl}}/assets/images/336561013.png" data-unresolved-comment-count="0" data-linked-resource-id="336561013" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="WSO2-IoT-Core-Components-Configuration-Management.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="336560926" data-linked-resource-container-version="1" data-media-id="5780625d-22ec-44ce-aa4f-56dbabf6a320" data-media-type="file"></span></a></p>
          <p><a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336565669/Policy+Management" data-linked-resource-id="336565669" data-linked-resource-version="1" data-linked-resource-type="page"><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="150" src="{{site.baseurl}}/assets/images/336561001.png?width=150" data-image-src="{{site.baseurl}}/assets/images/336561001.png" data-unresolved-comment-count="0" data-linked-resource-id="336561001" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="WSO2-IoT-Core-Components-Policy-Management.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="336560926" data-linked-resource-container-version="1" data-media-id="239b50d4-ea94-4fa4-9e23-4ddcb797ec7c" data-media-type="file"></span></a> <a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336561155/Key+Concepts#KeyConcepts-Operations" data-linked-resource-id="336561155" data-linked-resource-version="1" data-linked-resource-type="page"><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="150" src="{{site.baseurl}}/assets/images/336561007.png?width=150" data-image-src="{{site.baseurl}}/assets/images/336561007.png" data-unresolved-comment-count="0" data-linked-resource-id="336561007" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="WSO2-IoT-Core-Components-Operation-Management.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="336560926" data-linked-resource-container-version="1" data-media-id="72a83ff3-8df6-4aec-b38d-1702ff0e8d1d" data-media-type="file"></span></a> <a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336561155/Key+Concepts#KeyConcepts-UserManagement" data-linked-resource-id="336561155" data-linked-resource-version="1" data-linked-resource-type="page"><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="150" src="{{site.baseurl}}/assets/images/336561031.png?width=150" data-image-src="{{site.baseurl}}/assets/images/336561031.png" data-unresolved-comment-count="0" data-linked-resource-id="336561031" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="WSO2-IoT-Core-Components-User-Management.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="336560926" data-linked-resource-container-version="1" data-media-id="e2204817-6b73-43a0-8ea4-178297ae7273" data-media-type="file"></span></a></p>
          <p><a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336567883/Certificate+Management" data-linked-resource-id="336567883" data-linked-resource-version="1" data-linked-resource-type="page"><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="150" src="{{site.baseurl}}/assets/images/336560983.png?width=150" data-image-src="{{site.baseurl}}/assets/images/336560983.png" data-unresolved-comment-count="0" data-linked-resource-id="336560983" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="WSO2-IoT-Core-Components-Certificate-Management.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="336560926" data-linked-resource-container-version="1" data-media-id="403a62de-87b4-4c91-896d-65141880ac66" data-media-type="file"></span></a> <a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336565909/Mobile+Application+Management" data-linked-resource-id="336565909" data-linked-resource-version="1" data-linked-resource-type="page"><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="150" src="{{site.baseurl}}/assets/images/336560995.png?width=150" data-image-src="{{site.baseurl}}/assets/images/336560995.png" data-unresolved-comment-count="0" data-linked-resource-id="336560995" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="WSO2-IoT-Core-Components-Application-Management.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="336560926" data-linked-resource-container-version="1" data-media-id="cabbbfdd-e773-4ec0-b86e-02f2848b59f3" data-media-type="file"></span></a> <a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336565669/Policy+Management#PolicyManagement-Compliance" data-linked-resource-id="336565669" data-linked-resource-version="1" data-linked-resource-type="page"><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="150" src="{{site.baseurl}}/assets/images/336560977.png?width=150" data-image-src="{{site.baseurl}}/assets/images/336560977.png" data-unresolved-comment-count="0" data-linked-resource-id="336560977" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="WSO2-IoT-Core-Components-Compliance-Management.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="336560926" data-linked-resource-container-version="1" data-media-id="22d727e0-3e80-41d8-9692-e3ec9f23e36a" data-media-type="file"></span></a></p>
        </div>
      </td>
    </tr>
    <tr>
      <th>Device Management Plugins</th>
      <td>
        <div class="content-wrapper">
          <p>Entgra IoT Server is designed as a pluggable architecture. Therefore, it is easy to introduce new device types to Entgra IoT Server.</p>
          <p>A plugin is not mandatory when creating a new device type. You can connect most of the IoT devices to Entgra IoT Server without using an external plugin. A plugin is useful when you need to perform complex operations.</p>
          <p>You can write a new device type using any of the following methods:</p>
          <ul>
            <li>Want to create a device type in one go? You can now create your own device type that includes the APIs, transports, UI and more, using the device management console or APIs. For more information, see&nbsp;<strong> <a href="https://docs.wso2.com/display/IoTS310/Creating+a+New+Device+Type" class="external-link" rel="nofollow">Creating a New Device Type</a></strong>.</li>
            <li>Does your device have common functionalities similar to our sample device types? If yes, see&nbsp;<strong> <a href="https://docs.wso2.com/display/IoTS310/Writing+Device+Type+via+the+Template" class="external-link" rel="nofollow">Writing Device Type via the Template</a></strong>.</li>
            <li>Does your device require special functions and need to be customized to meet a given requirement? See&nbsp;<strong> <a href="https://docs.wso2.com/display/IoTS310/Writing+Device+Plugins+via+Java+Code" class="external-link" rel="nofollow">Writing Device Plugins via Java Code</a></strong>.</li>
          </ul>
        </div>
      </td>
    </tr>
    <tr>
      <th>Transports</th>
      <td>
        <p>Transports are required for the server to communicate with the devices and for the devices to communicate with the server. For example in Entgra IoT Server, the server communicates with the iOS devices via the APNS transport and the devices communicate with the server using the HTTP transport. For more information on how the transports work, see <strong><a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336568855/Writing+Transport+Extensions" data-linked-resource-id="336568855" data-linked-resource-version="1" data-linked-resource-type="page">Writing Transport Extensions</a></strong>.</p>
        <p>The server uses push notifications to communicate with the device. By default, Entgra IoT Server has implemented push notification providers for MQTT, XMPP,&nbsp;<a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336569693/Android+Notification+Methods#AndroidNotificationMethods-FCM" data-linked-resource-id="336569693" data-linked-resource-version="1" data-linked-resource-type="page">FCM</a>, and&nbsp;<a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336570963/iOS+Notification+Method#iOSNotificationMethod-APNS" data-linked-resource-id="336570963" data-linked-resource-version="1" data-linked-resource-type="page">APNS</a>. You can write your own push notification provider too. For more information, see <strong><a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336568926/Adding+a+Push+Notification+Provider" data-linked-resource-id="336568926" data-linked-resource-version="1" data-linked-resource-type="page">Adding a Push Notification Provider</a></strong>.</p>
      </td>
    </tr>
    <tr>
      <th>API</th>
      <td>
        <div class="content-wrapper">
          <p>Entgra IoT Server is fully API driven. All the APIs are equipped with industry standard swagger annotations. Therefore, stub or client generation can be done easily.</p>
          <p>The APIs are fully secured. If you want to carry out an operation on a device, t<span>wo levels of authorization take place:</span></p>
          <ul>
            <li>Checks if the user&nbsp;is authorized to access the APIs.</li>
            <li>Checks if the user is authorized to carry out the operation on the selected device.</li>
          </ul>
          <p>The diagram shown below lists out the REST APIs provided by default in Entgra IoT Server. That's not all, you are able to write your own APIs for your device type using Entgra IoT Server due to its pluggable architecture.</p>
          <p><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-content-image-border" width="900" src="{{site.baseurl}}/assets/images/336560971.png?width=900" data-image-src="{{site.baseurl}}/assets/images/336560971.png" data-unresolved-comment-count="0" data-linked-resource-id="336560971" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="WSO2-IoT-Server-Architecture-API-layer.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="336560926" data-linked-resource-container-version="1" data-media-id="7e1e808e-f36c-4e4d-a00e-8f7dcc71c7fa" data-media-type="file"></span></p>
          <ul>
            <li>Try out the <strong><a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336566149/Getting+Started+with+APIs" data-linked-resource-id="336566149" data-linked-resource-version="1" data-linked-resource-type="page">Getting Started with APIs</a></strong> tutorial to get a quick understanding on how the APIs function.</li>
            <li>For more information on the full list of APIs available in Entgra IoT Server, see<strong> <a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336569022/Device+Management+REST+APIs" data-linked-resource-id="336569022" data-linked-resource-version="1" data-linked-resource-type="page">Device Management REST APIs</a></strong>.</li>
            <li>Want to write your own APIs for your device type? See, <strong><a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336568848/Writing+Device+APIs" data-linked-resource-id="336568848" data-linked-resource-version="1" data-linked-resource-type="page">Writing Device APIs</a></strong>.</li>
          </ul>
        </div>
      </td>
    </tr>
    <tr>
      <th>Authentication and Authorization</th>
      <td>
        <div class="content-wrapper">
          <p>When using Entgra IoT Server in your enterprise, security, and integration are important as you need to store all the user data and device data in the server, and the devices store confidential information about your business. Further, security is a must when integrating Entgra IoT server with other applications, user interfaces, and when exposing its capability to the outside world.</p>
          <p>Entgra IoT Server provides the following security protocols for authenticating and authorizing users and devices:</p>
          <p><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-content-image-border" width="600" src="{{site.baseurl}}/assets/images/336561049.png?width=600" data-image-src="{{site.baseurl}}/assets/images/336561049.png" data-unresolved-comment-count="0" data-linked-resource-id="336561049" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="iot-architecture-authentication-authorization.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="336560926" data-linked-resource-container-version="1" data-media-id="0a65d3ec-89b9-49dc-a73e-64a712c3b740" data-media-type="file"></span></p>
          <p>Further, to authenticate and authorize the APIs, Entgra IoT Server uses scopes. A scope has a permission assigned to it. For more information, see <strong><a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336566187/Device+Management+API+Scopes" data-linked-resource-id="336566187" data-linked-resource-version="1" data-linked-resource-type="page">Device Management API Scopes</a></strong>.</p>
        </div>
      </td>
    </tr>
    <tr>
      <th>Analytics and Analytics Plugin</th>
      <td>
        <p>Analysing the data gathered by your devices is important for you to identify patterns and predict future trends. Therefore, by default Entgra IoT Server combines real-time, batch, interactive, and predictive (via machine learning) analysis of data into one integrated platform to support the multiple demands of the IoT solutions. Using Entgra IoT Server, you can write your own methods to gather the data from your device and analyze the data that was gathered.</p>
        <p>Entgra IoT Server has the capability to analyze the data gathered from the device on the device itself. This is achieved via the edge computing capability in Entgra IoT Server.</p>
        <ul>
          <li>For more information on the Entgra IoT Server's Analytics framework, see <strong><a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336569193/Understanding+the+Entgra+IoT+Server+Analytics+Framework" data-linked-resource-id="336569193" data-linked-resource-version="1" data-linked-resource-type="page">Understanding the Entgra IoT Server Analytics Framework</a></strong>.</li>
          <li>Want to write your own methods to gather and analyze data? See, <strong><a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336568968/Writing+Analytics" data-linked-resource-id="336568968" data-linked-resource-version="1" data-linked-resource-type="page">Writing Analytics</a></strong>.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <th>Applications</th>
      <td>
        <p>Applications and external system applications are able to connect with Entgra IoT Server without any hassle as all its functions are exposed via secured APIs. It also provides secure communication between the devices and the applications through the authentication methods, such as SSO, SAML, and XACML, when connected with WSO2 Identity Server.</p>
        <ul>
          <li>Try out the <strong><a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336565909/Mobile+Application+Management" data-linked-resource-id="336565909" data-linked-resource-version="1" data-linked-resource-type="page">Mobile Application Management</a> </strong>tutorial to get a quick understanding of how to create, manage and install applications using Entgra IoT Server.</li>
          <li>Want to know more about how to install an application on many devices at once and the mobile application life cycle, see <strong><a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336569049/Managing+Mobile+Applications" data-linked-resource-id="336569049" data-linked-resource-version="1" data-linked-resource-type="page">Managing Mobile Applications</a></strong>.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <th>Devices and SDKs</th>
      <td>
        <p>Entgra IoT Server provides SDK support to build and connect your device with the Entgra IoT platform. As all of the functionalities are exposed through APIs, the SDK support makes it easier to create new agent applications that run on the device. All the basic functionalities required to connect the device to the server are supported in the SDK itself.</p>
      </td>
    </tr>
  </tbody>
</table>


### Entgra IoT Server user scenarios

To understand how Entgra IoT Server functions, let's take a look at two real world scenarios.

#### Sensor data processing

To understand the concept clearly consider the scenario where your fire alarm is connected to Entgra IoT Server.

1.  The fire alarm sensor detects the outbreak of a fire and notifies the Device API.
2.  The Device API calls for the respective operation needed to be carried out from the fire alarm device type plugin.  
    Example: Notify the fire brigade of the fire.
3.  The device plugin sends the details of the fire outbreaks to WSO2 Data Analytics Server (DAS) where the data will be analyzed to derive useful information.  
    Example: Analyze the frequency of the fire outbreaks and based on it identify the reason and pattern of the outbreaks.
4.  Next, DAS sends the analyzed information to the database and the system. The information will be made available to the end user who will access it via their mobile devices using the respective app.

![]({{site.baseurl}}/assets/images/336560989.png)

#### Device access via the apps

With technology connecting individuals to devices, you are now capable of getting your devices to perform various operations for you. Consider the scenario where your gate is connected to the IoT Server.

1.  A request is sent to open the gate at your arrival through the respective mobile application.
2.  The application sends the request to the configured device plugin using the Device API.
3.  Next, the plugin communicates with the gate via a transport protocol, which can be MQTT/CoAP/XMPP/DDS/Sigfox, in order to trigger the specific event of opening the gate.

![]({{site.baseurl}}/assets/images/336560961.png)

## What's next? 

To know more about the Entgra IoT Server architecture, check out the article on [An Introduction to Entgra IoT Architecture](http://wso2.com/library/articles/2017/07/an-introduction-to-wso2-iot-architecture/).
