---
layout: default
title: Enterprise IoT Solution
parent: Quick Start Guide
nav_order: 1
---

# Enterprise IoT Solution

This section gives you a quick understanding of how to connect your device to [Entgra IoT Server](http://entgra.io/) as an enterprise IoT solution and start using it. 

---

The virtual fire alarm is a virtual device that mimics the functionality of a real fire alarm. Therefore, for this use case you do not need a real device. Once you start the virtual fire alarm, it will connect to the Entgra IoT Server and push the temperature readings it collects. The device supports [MQTT](http://mqtt.org/) and [XMPP](https://xmpp.org/) communications and is configured to use MQTT by default.

Before you begin


*   Install [Oracle Java SE Development Kit (JDK)](http://java.sun.com/javase/downloads/index.jsp) version 1.8.* and set the `JAVA_HOME` environment variable. For more information on setting up `JAVA_HOME` on your OS, see [Installing the Product](about:blank#).
*   Check out [system requirements section](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352814114/System+Requirements) and ensure that you have all the appropriate prerequisite software installed on your system.


### Download Entgra IoT Server

1.  [Download Entgra IoT Server](https://storage.googleapis.com/iot-release-public/3.4.0/entgra-iots-3.4.0.zip).

2.  Copy the downloaded file to a preferred location and unzip it. The unzipped file will be called `<IOTS_HOME>` throughout this documentation.

    *   The downloaded Entgra IoT Server file is large. Therefore, when unzipping, it might extract halfway through and stop. To avoid this, we recommend that you unzip the file via the terminal.  
        Example:

        `unzip entgra-iots-3.4.0.zip`

    *   The maximum character count supported for a file path in the Windows OS is **260**. If this count is exceeded when extracting the pack into a directory, you will get the `Error 0x80010135: Path too long` error. To overcome this issue use the commands given below:
        *   Create a substring and map the current file path to it.  
            In the example given below, the Entgra IoT Server `.zip` file is located in the `C:\Users\Administrator\Downloads\entgra` directory.

            `C:\Users\Administrator\Downloads\entgra>subst Y: C:\Users\Administrator\Downloads\entgra`

        *   Copy the IoT Server Server zip folder to the new path you created and unzip the file there.  
            Example: Unzip the file in the `Y:` drive.


### Sign into the Device Management console

Follow the instructions given below to start Entgra IoT Server, and sign into the device management console:

1.  Navigate to the Entgra IoT Server pack's samples directory and run the `device-plugins-deployer.xml` file.

    Why is this needed?

    Before enrolling devices with Entgra IoT Server you need to have the device type plugins created. The [Android](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352816232/Android), [Android Sense](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352816866/Android+Sense), [Windows](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352818773/Windows), and [Android Virtual Device](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352817149/Android+Virtual+Device) sample device type plugins are available by default on Entgra IoT Server. You need to run the command given below to create the [Raspberry Pi](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352818675/Raspberry+Pi), [Arduino](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352817394/Arduino), and [Virtual Fire Alarm](/doc/en/lb2/Enterprise-IoT-solution.html) sample plugins.

    For more information on writing your own device plugin, see [Writing Device Types](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822027/Writing+Device+Types).


    Example:

    ```
    cd <IOTS_HOME>/samples
    mvn clean install -f device-plugins-deployer.xml
    ```

2.  Start Entgra IoT Server by starting the three profiles in the following order:

    1.  Start the broker profile, which corresponds to the WSO2 Message Broker profile.

        The default port assigned for the broker is 9446.

    2.  Start the core profile, which corresponds to the WSO2 Connected Device Management Framework (WSO2 CDMF) profile.

        The default port assigned for the core is 9443.

    3.  Start the analytics profile, which corresponds to the WSO2 Data Analytics Server profile.

        The default port assigned for analytics is 9445.


3.  Access the device management console by navigating to `https://<HTTPS_HOST>:<HTTPS_PORT>/devicemgt`.  
    Example: `https://localhost:9443/devicemgt`

4.  Enter your username and password.  
    If you are new to using Entgra IoT Server, you need to first register before you can to log into the WSO2 device management console.


5.  Click **LOGIN**. 

### Start the virtual fire alarm

Follow the steps given below to start the virtual fire alarm device:
{{site.baseurl}}/assets/images/

1.  If you are a new user, click **Enroll New Device**.   
    <br/>
    <img src = "{{site.baseurl}}/assets/images/352814410.png">
    <br/>

    If you have enrolled devices before, click **Add** under **Devices.** 

2.  Click **Try** to try out the **Virtual Firealarm, **which is listed under **Virtual Device Types**.   
    <br/>
    <img src = "{{site.baseurl}}/assets/images/352814469.png">
    <br/>

3.  Download the device:
    1.  Click **Download Agent** to download the device agent.
    2.  Enter a preferred name for your device and click **DOWNLOAD NOW**.

4.  Unzip the downloaded agent file and navigate to its location via the terminal.

5.  Start the virtual fire alarm.

    ```
    ./start-device.sh --> For Linux/Mac/Solaris
    start-device.bat --> For Windows
    ```

    Once you start your virtual fire alarm, the fire alarm emulator will pop up.

    <br/>
    <img src = "{{site.baseurl}}/assets/images/352814372.png">
    <br/>
    
### Try out the virtual fire alarm

Once you start your virtual fire alarm, you can try out the following actions:

<table>
  <colgroup>
    <col style="width: 143.0px;">
    <col style="width: 1191.0px;">
  </colgroup>
  <tbody>
    <tr>
      <th>Action</th>
      <th>Description</th>
    </tr>
    <tr>
      <th><strong>View device details</strong></th>
      <td>
        <div class="content-wrapper">
          <p>Click&nbsp;the <span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail" width="30" src="{{site.baseurl}}/assets/images/352814377.png?width=30" data-image-src="attachments/352814351/352814377.png" data-unresolved-comment-count="0" data-linked-resource-id="352814377" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="WSO2-IoT-Server-Menu.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352814351" data-linked-resource-container-version="1" data-media-id="301136d4-c3ca-458e-8efd-dfab6ba4a50b" data-media-type="file"></span> icon, and click <strong>Device Management. </strong>Select the virtual fire alarm device you created from your devices and view the device details, carry out operations, and monitor real-time data.</p>
          <div class="confluence-information-macro confluence-information-macro-information"><span class="aui-icon aui-icon-small aui-iconfont-info confluence-information-macro-icon"></span>
            <div class="confluence-information-macro-body">
              <p>To view the real-time data, navigate to the WSO2 Data Analytics (WSO2 DAS) management console: <code>https://&lt;IOTS_HOST&gt;:9445/carbon</code><span class="nolink">.&nbsp;T</span><span class="nolink">he default <code>IOTS_HOST</code> is <code>localhos</code>t</span>. Now you will be able to view the real-time data for the virtual fire alarm.<span> This is not required in a production environment as trusted certificates are used.</span></p>
            </div>
          </div>
          <p><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-content-image-border" height="381" width="900" src="{{site.baseurl}}/assets/images/352814458.png?width=900" data-image-src="attachments/352814351/352814458.png" data-unresolved-comment-count="0" data-linked-resource-id="352814458" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="WSO2-IoT-Server-Viretual-Fire-Alarm-Page.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352814351" data-linked-resource-container-version="1" data-media-id="732cbcfe-b29a-4c56-90c5-df997ca093a1" data-media-type="file"></span></p>
        </div>
      </td>
    </tr>
    <tr>
      <th><strong>Ring the fire alarm</strong></th>
      <td>
        <div class="content-wrapper">
          <ol>
            <li>Click&nbsp;<strong>Control buzzer</strong>&nbsp;under&nbsp;<strong>Operations</strong>&nbsp;on the Device Details page.<br><span class="confluence-embedded-file-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-thumbnail confluence-content-image-border" width="80" src="{{site.baseurl}}/assets/images/352814399.png?width=80" data-image-src="attachments/352814351/352814399.png" data-unresolved-comment-count="0" data-linked-resource-id="352814399" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="Control_Buzzer.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352814351" data-linked-resource-container-version="1" data-media-id="75719925-50fd-4c3e-a330-19501795830d" data-media-type="file"></span></li>
            <li>
              <div>Select the&nbsp;<strong>Buzzer On</strong>&nbsp;option and click&nbsp;<strong>Send to Device</strong>, to ring the fire alarm. The&nbsp;<strong>Alarm Status&nbsp;</strong>on the&nbsp;<strong>Fire Alarm Emulator&nbsp;</strong>indicates that the fire alarm is ringing.</div><span class="confluence-embedded-file-wrapper"><img class="confluence-embedded-image" src="{{site.baseurl}}/assets/images/352814383.png" data-image-src="attachments/352814351/352814383.png" data-unresolved-comment-count="0" data-linked-resource-id="352814383" data-linked-resource-version="1" data-linked-resource-type="attachment" data-linked-resource-default-alias="WSO2-IoT-Server-Ring-Buzzer.png" data-base-url="https://entgra.atlassian.net/wiki" data-linked-resource-content-type="image/png" data-linked-resource-container-id="352814351" data-linked-resource-container-version="1" data-media-id="19c37b4c-4e84-4c82-9e7a-9cefea980175" data-media-type="file"></span><br><span style="color: rgb(0,0,0);">Similarly, select&nbsp;the </span><strong>Buzzer Off</strong> <span style="color: rgb(0,0,0);">option and click</span> <strong>Send to Device</strong><span style="color: rgb(0,0,0);">, to stop the ringing. The&nbsp;</span><strong>Alarm Status</strong> <span style="color: rgb(0,0,0);">on the&nbsp;</span><strong>Fire Alarm Emulator</strong> indicates that the fire alarm is no longer ringing<span style="color: rgb(0,0,0);">.</span>
            </li>
          </ol>
        </div>
      </td>
    </tr>
  </tbody>
</table>

### View real-time and historical data

When you adjust the temperature and humidity values in the virtual fire alarm emulator you can see the data in real-time or view historical data by following the steps given below:s

<br/>
    <img src = "{{site.baseurl}}/assets/images/352814388.png">
<br/>

### What's next?

Follow the options given below to see what you can do next:

*   Do you have an Android device? Try out the Android Sense device type supported by default on Entgra IoT Server. For more information, see [Android Sense](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352816866/Android+Sense).
*   Want to try out more devices? Connect the devices listed below to Entgra IoT Server and try them out.
    *   [Raspberry Pi](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352818675/Raspberry+Pi)
    *   [Arduino](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352817394/Arduino)
*   Need to create a new device type and connect it to Entgra IoT Server? For more information, see the [Device Manufacturer Guide](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352821869/Device+Manufacturer+Guide).

