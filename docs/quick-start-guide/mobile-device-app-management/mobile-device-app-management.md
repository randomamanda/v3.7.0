---
layout: default
title: Mobile Device App Management
parent: Quick Start Guide
grand_parent: docs
has_children: true
nav_order: 2
---

# Mobile Device App Management
---

[WSO2 Enterprise Mobility (WSO2 EMM) capabilities are bundled with Entgra IoT Server](https://entgra.io/). Therefore, it is a comprehensive open source platform that enables organizations to secure, manage, and monitor Android, iOS, Windows, and any IoT devices. This guide gives you a quick walk-through of Entgra IoT Server using the following enterprise mobility management sample scenario.

### Sample scenario

MobX is a fast-growing organization with over 500 employees. Its employees are allowed to connect their personal mobile devices to the corporate network, but each device must adhere to given policies. For example, a device must connect only to a specified network, its camera must be disabled while connected, and an employee directory application must be installed to access official contact details of the employees. MobX has decided to use Entgra IoT Server to simplify their mobile device and mobile application management complexities.

Alex is the Director of Engineering who manages development at MobX. Alex needs to enroll the Android and iOS devices with Entgra IoT Server. **Click** on a platform below to learn how Alex enrolls each device.

<br/>
    [<img src = "{{site.baseurl}}/assets/images/352814542.png">]()
    [<img src = "{{site.baseurl}}/assets/images/352814547.png">]()