---
layout: default
title: About this Release
nav_order: 3
---

---
# About this Release
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}
---

### What's new in this release

[Entgra IoT Server](http://wso2.com/iot) version 3.4.0 is the successor of [WSO2 IoT Server 3.2.0](https://docs.wso2.com/display/IoTS320/WSO2+IoT+Server+Documentation). Some prominent features and enhancements are as follows:

*   [Device Enrollment Program (DEP) for iOS devices](https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336571001/Device+Enrollment+Program).
*   Making Entgra IoT Server 3.3.0 General Data Protection Regulations (GDPR) compliant.
    *   [Removing user and device details when the user requests to be forgotten](https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336568597/General+Data+Protection+Regulation+for+WSO2+IoT+Server).
    *   [Consent management when signing in to Entgra IoT Server](https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336566430/Accessing+the+Entgra+IoT+Server+Consoles).
    *   [Consent management when enrolling devices](https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336563072/Android).
    *   Introduction of cookie policies and privacy policies in the device management console.

### Compatible versions

Entgra IoT Server is compatible with WSO2 Carbon 4.4.17 products. For more information on the products in each Carbon platform release, see the [Release Matrix](http://wso2.com/products/carbon/release-matrix/).

### Known issues

For a list of known issues in this release, see [Entgra IoT Server 3.4.0 - Known Issues](https://gitlab.com/entgra/product-iots/).

### Fixed issues

For a list of fixed issues in this release, see [Entgra IoT Server 3.4.0 - Fixed Issues](https://gitlab.com/entgra/product-iots/issues?scope=all&utf8=%E2%9C%93&state=closed).