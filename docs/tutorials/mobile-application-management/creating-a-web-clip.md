---
layout: default
title: Creating a web clip
parent: Mobile Application Management
grand_parent: Tutorials
nav_order: 2
---

# Creating a Web clip

---

In this tutorial let's take a look at how you can create a web clip using the Entgra IoT Server app publisher. 



What is a web clip?



A web clip is used to add a bookmark to a web application. It is similar to creating a shortcut to access a web page or website.  
For example, the employees at MobX need to access the Entgra website frequently. Therefore, the administrator decides to create a web clip so they can access the website in just one click.





1.  Navigate to the App Publisher using the following URL:** `https://<IoT_HOST>:9443/publisher`**
2.  [Enter the username and password, and sign in](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles#AccessingtheEntgraIoTServerConsoles-AccessingtheDeviceManagementConsole).

3.  Click **Add****New Mobile Application** that is under the** Mobile Application** drop down.
4.  Enter the application details:
    1.  Select **Web App** for **Platform** type.
    2.  Enter the URL of the webpage or website, and click **Next.**Example:  
    ![]({{site.baseurl}}/assets/images/352813401.png)
5.  Enter the following details of the app.
    *   **Name** - The full name of the app.
    *   **Display Name** - The name of the app that is displayed to the user.
    *   **Description** -  A summarized description of the app.
    *   **Recent Changes** - Optional. A summarized description of what is new in this app when compared to its previous version.
    *   **Version - **Version of the public application. If you have previously entered a different version of this app, make sure to enter a unique version number.
    *   **Category** - Select the category that this app needs to be listed under.
    *   **Visibility** - Enter the user roles that this app will be visible to.
    *   Click on the respective **+** buttons to upload the required images.
    *   *   **Banner** - Image that will appear as the app banner.
        *   **Screenshots** - Screenshots of the app so that the user can get an understanding of what the app offers. A maximum of four screenshots are allowed.
        *   **Icon file** - The image that will be used as the application icon in the Store and when the application is installed on a device.

    

    

    The recommended image extension is **`.png`** while the recommended dimensions are as follows:

    *   **Icons**: 124px x 124px
    *   **Screenshots**: 288px x 512px (landscape) and 512px x 288px (portrait)
    *   **Banners**: 705px x 344px

    You must follow the above standards when creating a webclip. A web clip is used to add a bookmark to a web application. 

    

    

    Example:  
    ![]({{site.baseurl}}/assets/images/352813413.png)

6.  Click **Submit for Review** for the web clip you created.  
    ![]({{site.baseurl}}/assets/images/352813419.png)
7.  Click **Approve > Publish**.  
    The web clip is now available in the app store for device owners to install on their devices.  
    Example:  
    ![]({{site.baseurl}}/assets/images/352813407.png)





To learn more about the mobile application life cycle, see [Mobile Application Lifecycle Management](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822358/Mobile+Application+Lifecycle+Management).









**NOTE**: You won't be able to uninstall a web clip that you installed on your device from Android 6 and above.





## What's next?

Your web clip is now published and available in the Entgra IoT Server app store. Next, take a look at [Installing an Application on a Device](/doc/en/lb2/Installing-an-Application-on-a-Device.html).
