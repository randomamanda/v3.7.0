---
layout: default
title: Creating an iOS Applications
parent: Mobile Application Management
grand_parent: Tutorials
nav_order: 1
---

# Creating an iOS Application

---

In this tutorial let's take a look at how you can create an iOS mobile application using the Entgra IoT Server app publisher. 

1.  Navigate to the App Publisher using the following URL:** `https://<IoT_HOST>:9443/publisher`**
2.  [Enter the username and password, and sign in](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles#AccessingtheEntgraIoTServerConsoles-AccessingtheDeviceManagementConsole).

3.  Click **Add ****New Mobile Application** that is under the **Mobile Application** drop down.
4.  Select **iOS** for the **Platform**. 
5.  Select the **Store Type**. The following are the available store types:  

    *   **Enterprise** - This refers to all apps that have been created by the organization.
    *   **Public** - This refers to publicly available apps in the App Store (i.e., free apps available online). 
6.  Creating mobile applications:

7.  Click **Next**,and you get the following screen.

    ![]({{site.baseurl}}/assets/images/352813344.png)  
    Enter the following details of the app.

    *   **Name** - The full name of the app.
    *   **Display Name **- The name of the app that is displayed to the user.
    *   **Description** -  A summarized description of the app.
    *   **Recent Changes** - Optional. A summarized description of what is new in this app when compared to its previous version.
    *   **Version - **Version of the public application. If you have previously entered a different version of this app, make sure to enter a unique version number.
    *   **Category** - Select the category that this app needs to be listed under.
    *   **Restrict Visibility** - Enter the user roles that this app needs to be restricted from.
    *   Click on the respective **+** buttons to upload the required images.
    *   *   **Banner** - Image that will appear as the app banner.
        *   **Screenshots** - Screenshots of the app so that the user can get an understanding of what the app offers. A maximum of four screenshots are allowed.
        *   **Icon file** - Image that will be used as the app icon in the Store. When App Manager installs a mobile app on a device, the default icon will be used instead of the uploaded icon.

    

    

    The recommended image extension is `.png` while the recommended dimensions are as follows:

    *   **Icons**: 124px x 124px
    *   **Screenshots**: 288px x 512px (landscape) and 512px x 288px (portrait)
    *   **Banners**: 705px x 344px

    

    

8.  Click **Create**. The created app will appear in the created list of apps.





After creating the mobile app, authorized users can review, approve, and publish the mobile application. For more information, see [Mobile Application Lifecycle Management](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822358/Mobile+Application+Lifecycle+Management).





## What's next?

Your iOS mobile application is now published and available in the Entgra IoT Server app store. Next, take a look at [Installing an Application on a Device](https://docs.wso2.com/display/IoTS310/Installing+an+Application+on+a+Device).