---
layout: default
title: Installing an Application on a device
parent: Mobile Application Management
grand_parent: Tutorials
nav_order: 4
---

# Installing an Application on a device

---

In this tutorial let's take a look at installing the applications you created on the device using the Entgra IoT Server app store.

Follow the steps given below:

1.  Navigate to the App Store using the following URL:  `**https://<IOTS_HOST>:9443/store**` 

2.  [Enter the username and password, and sign in](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles#AccessingtheEntgraIoTServerConsoles-AccessingtheAppStore).

3.  Click on the mobile application you want to install, and click **Install**.   
    Example:  
    ![]({{site.baseurl}}/assets/images/352813498.png)

    

    

    If you want to install the application on many devices in one go, click **Ent.** **Install**.Thenselect the user roles or users on whose devices you want to install the application, and click **Install**.

    By default, only the administrator can **Ent. Install** applications on devices. For more information on enabling the enterprise subscription for other user roles, see [Enabling Enterprise Subscriptions for Mobile Apps](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822330/Enabling+Enterprise+Subscriptions+for+Mobile+Apps).

    

    

4.  Select **Instant install**, and click **Yes**.

    

    

    If you don't want to install the application now, you can install it later on by specifying the date and time you want the application to be installed on the device.

    

    


5.  Click on your device in the pop-up menu to install and subscribe to the application.  
    A success message will be shown when you have successfully subscribed to the application.
6.  Tap **Install** on your device to finish installing the application.  
    Now you can use start using the application you installed.