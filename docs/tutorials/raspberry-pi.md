---
layout: default
title: Raspberry Pi
parent: Tutorials
grand_parent: docs
nav_order: 6
---

# Raspberry Pi

---

**In this tutorial**, you will connect a Raspberry Pi to Entgra IoT Server and manage it. The following subsections explain how you can work with the Raspberry Pi device type:



Before you begin



*   Make sure to have the following ready:
    *   A Raspberry Pi board that is Internet enabled (Wi-Fi or Ethernet).
    *   An Adafruit DHT11 temperature sensor.
    *   An LED bulb.
*   Navigate to the `<IOTS_HOME>/samples` directory and run the `device-plugins-deployer.xml` file.

    ```
    cd <IOTS_HOME>/samples
    mvn clean install -f device-plugins-deployer.xml
    ```

*   Start the Entgra IoT Server broker, core and, analytics profiles in the given order. For more information, see [how to start the Entgra IoT Server](https://docs.wso2.com/display/IoTS310/Running+the+Product#RunningtheProduct-StartingtheServer).

    ```
    cd <IOTS_HOME>/bin

    ------Linux/Mac OS/Solaris ----------
    ./broker.sh
    ./iot-server.sh
    ./analytics.sh

    -----Windows-----------
    broker.bat
    iot-server.bat
    analytics.bat
    ```





Let's get started!

### Start the Raspberry Pi

1.  Sign in to the Device Management console.

    

    

    

    Follow the instructions below to sign in to the Entgra IoT Server device management console:

    1.  If you have not started the server previously, [start the server](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819593/Running+the+Product).

    2.  Access the device management console.

        *   For access via HTTP:```js
            `http://<IOTS_HTTP_HOST>:9763/devicemgt/`
            ```

            For example:` http://localhost:9763/devicemgt/`
        *   For access via secured HTTP:   
            `https://<IOTS_HTTPS_HOST>:9443/devicemgt/` For example:` https://localhost:9443/devicemgt/ `
    3.  Enter the username and password, and click **LOGIN**.

        

        

        *   The system administrator is able to log in using `admin` for both the username and password. However, other users will have to first register with Entgra IoT Server before being able to log into the IoTS device management console. For more information on creating a new account, see [Registering with Entgra IoT Server](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819690/Registering+with+Entgra+IoT+Server).

        *   If you are signing in as a different tenant user, the username needs to be in the following format `<USERNAME>@<TENANT_DOMAIN>`.

        

        

        By logging in you agree with the Entgra IoT Server 3.3.0 cookie policy and privacy policy.  
        ![]({{site.baseurl}}/assets/images/352819653.png)

    4.  Further, you need to provide consent for the details you want Entgra IoT Server to use.  
        ![]({{site.baseurl}}/assets/images/352819626.png)

        The respective device management console will change, based on the permissions assigned to the user.

        For example, the device management console for an administrator is as follows:  
        ![]({{site.baseurl}}/assets/images/352819647.png)

    

    

    

2.  Click **Enroll New Device**.  
    ![]({{site.baseurl}}/assets/images/352818746.png)
3.  Click **Raspberry Pi** to enroll a device of the type Raspberry Pi.  
    ![]({{site.baseurl}}/assets/images/352818723.png)
4.  Connect the DHT11 Temperature Sensor and the LED to the Raspberry Pi board.

5.  Set up your Raspberry Pi device as shown in the schematic diagram. 

    

    

    Ensure that your Raspberry Pi Board is Internet enabled, either via Wi-Fi or Ethernet.

    

    

    ![]({{site.baseurl}}/assets/images/352818683.png)

6.  Download the device agent.
    1.  Click **Download Agent**.
    2.  Enter a preferred name on the download agent form.
    3.  Click **DOWNLOAD NOW** to download the device agent on your machine.
7.  Unzip the downloaded agent file and copy the unzipped file onto your Raspberry Pi board.
8.  Navigate to the downloaded agent that is on the Raspberry Pi board.  
    Example: 

    `cd <RASPBERRY_PI_BOARD>/agent`

9.  Test to confirm that the agent is working as expected.

    `./testAgent.sh`

    

    

    You can run the script on your local machine, on which the agent was downloaded.

    

    

    

    Follow the steps given below to confirm that the agent is working as expected.

    1.  You will be prompted to get the apt-get updates and continue. Enter `Yes` to get the updates or enter `No` to continue without the updates.  
        Example: Continue without receiving the apt-get updates.

        `Do you wish to run 'apt-get update' and continue? [Yes/No] No`

    2.  Enter the time interval that you require for the device to push data to Entgra IoTS, in seconds.

        Example: Push data to the IoTS every 45 seconds.

        `What's the time-interval (in seconds) between successive Data-Pushes to WSO2-IoTS (ex: '60' indicates 1 minute) > 45`

    3.  Run the test agent as a virtual agent. Enter `Yes` to run it as a virtual agent or enter `No` if you wish to run the test on the Raspberry Pi device itself.

        

        

        If the Raspberry Pi device is not connected to the IoT Server and if you enter `No`, you will run into errors as the server will not be able to find the device.

        

        

        Example: Running the agent as a virtual agent.

        `Do you want to run this as a virtual agent? (Yes/No) Yes`

    Once the virtual agent starts you can view the data being pushed to the IoT Server.

    Example:

    ```js
    RASPBERRY_STATS: Temp = 25.0 * C Humidity = 28.0 %
      send: 'POST /raspberrypi/controller/push_temperature/ HTTP/1.1\r\nHost: 10.100.7.35:9443\r\nAccept-Encoding: identity\r\nAuthorization: Bearer 88f4940a2c26ef2b7b5d02d2767182c7\r\nContent-Type: application/json\r\nContent-Length: 84\r\n\r\n' {
        "owner": "admin",
        "deviceId": "1h7ldn73eha2f",
        "reply": "10.100.7.35:5678",
        "value": "25"
      }~~~~~~~~~~~~~~~~~~~~~~~~Pushing Device - Data~~~~~~~~~~~~~~~~~~~~~~~~~
      send: '{"owner":"admin","deviceId":"1h7ldn73eha2f","reply":"10.100.7.35:5678","value":"25"}'
    reply: 'HTTP/1.1 204 No Content\r\n'
    header: Date: Wed, 20 Jan 2016 06: 01: 55 GMT
    header: Server: WSO2 Carbon Server~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      RASPBERRY_STATS: 204
    RASPBERRY_STATS: No Content
    RASPBERRY_STATS: Response Message
    Date: Wed, 20 Jan 2016 06: 01: 55 GMT
    Server: WSO2 Carbon Server
    ```

    

    

    

    

10.  Start and deploy the agent as a service on the Raspberry Pi board once you have tested the agent as explained in step 9.

    

    

    The registered service will be loaded on boot up.

    

    

    `./startService.sh`

### Control the bulb

Once you start and deploy the Raspberry Pi agent, follow the steps given below to turn on or turn off the bulb connected to the Raspberry Pi board.

1.  Click **Bulb On/Off** under **Operations** on the Device Details page.  
    ![]({{site.baseurl}}/assets/images/352818711.png)
2.  Enter **On** to switch the bulb on and click **Send to Device**. Enter **Off** to switch the bulb off.  
    ![]({{site.baseurl}}/assets/images/352818717.png)

### View device analytics

Click ![]({{site.baseurl}}/assets/images/352818734.png) to monitor real-time data via the Device Details page.

Example:

![]({{site.baseurl}}/assets/images/352818740.png)

### What's next?

Follow the options given below to see what you can do next.

*   Navigate to the Device Management page to view all the devices created by you.

    

    

    

    

    

    1.  [Sign in to the Device Management console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles).
    2.  Click the menu icon.  
        ![]({{site.baseurl}}/assets/images/352818689.png)
    3.  Click **Device Management**.  
        ![]({{site.baseurl}}/assets/images/352818694.png)

    

    

    

    

*   Select a device from your device dashboard and check the available operations and monitor real-time data.![]({{site.baseurl}}/assets/images/352818700.png)