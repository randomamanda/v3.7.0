---
layout: default
title: Getting Management API scopes
parent: Getting Started with APIs
grand_parent: Tutorials
nav_order: 2
---

# Getting Management API scopes

---

The permission to invoke the APIs are assigned via scopes. You can define all the scopes and generate an access token that can invoke all the APIs or you can generate an access token that only has the required scope to invoke a specific API. For more information on how to generate the access token, see [Obtaining the access token.](Getting-Started-with-APIs_352819318.html#GettingStartedwithAPIs-Obtainingtheaccesstoken)

Take a look at all the device management API scopes.

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Scope</th>
      <th>Description</th>
    </tr>
    <tr>
      <td><code>perm:sign-csr</code></td>
      <td>Permission to sign the iOS CSR certificate.</td>
    </tr>
    <tr>
      <td><code>perm:admin:devices:view</code></td>
      <td>Permission to get the details of a device as the administrator.</td>
    </tr>
    <tr>
      <th colspan="2">Managing user roles</th>
    </tr>
    <tr>
      <td><code>perm:roles:add</code></td>
      <td>Permission to add a user role.</td>
    </tr>
    <tr>
      <td><code>perm:roles:add-users</code></td>
      <td>Permission to add users to a user role.</td>
    </tr>
    <tr>
      <td><code>perm:roles:update</code></td>
      <td>Permission to update a user role.</td>
    </tr>
    <tr>
      <td><code>perm:roles:permissions</code></td>
      <td>Permission to define device management permissions to the user role.</td>
    </tr>
    <tr>
      <td><code>perm:roles:details</code></td>
      <td>Permission to view user role details.</td>
    </tr>
    <tr>
      <td><code>perm:roles:view</code></td>
      <td>Permission to view the list of user roles.</td>
    </tr>
    <tr>
      <td><code>perm:roles:create-combined-role</code></td>
      <td>Permission to combine two roles and create one role.</td>
    </tr>
    <tr>
      <td><code>perm:roles:delete&nbsp;</code></td>
      <td>Permission to delete a user role.</td>
    </tr>
    <tr>
      <th colspan="2">Managing devices</th>
    </tr>
    <tr>
      <td>
        <p><code>perm:get-activity</code></p>
      </td>
      <td>Permission to get the activity details of a device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:devices:delete</code></p>
      </td>
      <td>Permission to delete a device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:devices:applications</code></p>
      </td>
      <td>Permission to get the details of the applications installed on a device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:devices:effective-policy</code></p>
      </td>
      <td>Permission to get the details of the policy that is enforced on a device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:devices:compliance-data</code></p>
      </td>
      <td>Permission to get the policy compliance details of a device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:devices:features</code></p>
      </td>
      <td>Permission to get the feature on the device. The operations and policies will be applied to these features.<br>For example, the camera restriction policy will be applied to the device's camera. In this context, the device's camera is a feature.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:devices:operations</code></p>
      </td>
      <td>Permission to get the device operation details.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:devices:search</code></p>
      </td>
      <td>Permission to search devices.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:devices:details</code></p>
      </td>
      <td>Permission to get the details of a device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:devices:update</code></p>
      </td>
      <td>Permission to update the device name.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:devices:view</code></p>
      </td>
      <td>Permission to get the details of registered devices.</td>
    </tr>
    <tr>
      <th colspan="2">Managing device policies</th>
    </tr>
    <tr>
      <td>
        <p><code>perm:policies:remove</code></p>
      </td>
      <td>Permission to remove one or multiple policies.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:policies:priorities</code></p>
      </td>
      <td>Permission to update the policy priority.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:policies:deactivate</code></p>
      </td>
      <td>Permission to deactivate an already active policy.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:policies:get-policy-details</code></p>
      </td>
      <td>Permission to get all the details of a specific policy.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:policies:manage</code></p>
      </td>
      <td>Permission to add a new policy.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:policies:activate</code></p>
      </td>
      <td>Permission to activate a deactivated policy.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:policies:update</code></p>
      </td>
      <td>Permission to update the policy details.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:policies:changes</code></p>
      </td>
      <td>Permission to publish the policy changes to the existing devices.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:policies:get-details</code></p>
      </td>
      <td>Permission to get the details of all the policies.</td>
    </tr>
    <tr>
      <th colspan="2">Managing users</th>
    </tr>
    <tr>
      <td>
        <p><code>perm:users:add</code></p>
      </td>
      <td>Permission to add a new user.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:users:details</code></p>
      </td>
      <td>Permission to get the details of a user.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:users:count</code></p>
      </td>
      <td>Permission to get the total number of users.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:users:delete</code></p>
      </td>
      <td>Permission to delete a user.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:users:roles</code></p>
      </td>
      <td>Permission to get the role details of a user.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:users:user-details</code></p>
      </td>
      <td>Permission to get the details of all the user.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:users:credentials</code></p>
      </td>
      <td>Permission to change the user password.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:users:search</code></p>
      </td>
      <td>Permission to search for a user.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:users:is-exist</code></p>
      </td>
      <td>Permission to get the details on whether the user exists or not.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:users:update</code></p>
      </td>
      <td>Permission to update the user details.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:users:send-invitation</code></p>
      </td>
      <td>Permission to invite users to enroll their devices.</td>
    </tr>
    <tr>
      <th colspan="2">Managing device groups</th>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:devices</code></p>
      </td>
      <td>Permission to get the list of devices in a group.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:update</code></p>
      </td>
      <td>Permission to update a device group.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:add</code></p>
      </td>
      <td>Permission to add a new device group.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:device</code></p>
      </td>
      <td>Permission to get the list of groups that have a specific device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:devices-count</code></p>
      </td>
      <td>Permission to get the number of devices in a group.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:remove</code></p>
      </td>
      <td>Permission to delete a device group.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:groups</code></p>
      </td>
      <td>Permission to get the list of groups that a user has access to.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:groups-view</code></p>
      </td>
      <td>Permission to get the details of a group.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:share</code></p>
      </td>
      <td>Permission to share a group with users.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:count</code></p>
      </td>
      <td>Permission to get the total number of device groups.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:roles</code></p>
      </td>
      <td>Permission to get the details of the user roles who can access the group.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:devices-remove</code></p>
      </td>
      <td>Permission to remove a device from a group.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:devices-add</code></p>
      </td>
      <td>Permission to add a device to a group.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:groups:assign</code></p>
      </td>
      <td>Permission to assign a device to a group.</td>
    </tr>
    <tr>
      <th colspan="2">Managing device types</th>
    </tr>
    <tr>
      <td>
        <p><code>perm:device-types:features</code></p>
      </td>
      <td>Permission to get the feature details of a device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:device-types:types</code></p>
      </td>
      <td>Permission to get the supported device platforms.</td>
    </tr>
    <tr>
      <th colspan="2">Managing mobile applications</th>
    </tr>
    <tr>
      <td>
        <p><code>perm:applications<strong>:</strong>install</code></p>
      </td>
      <td>Permission to install a mobile application. This invokes an internal API.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:applications:uninstall</code></p>
      </td>
      <td>Permission to uninstall a mobile application.This invokes an internal API.</td>
    </tr>
    <tr>
      <th colspan="2">Managing Notifications</th>
    </tr>
    <tr>
      <td>
        <p><code>perm:notifications:mark-checked</code></p>
      </td>
      <td>Permission to update the status of a notification sent to a device,</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:notifications:view</code></p>
      </td>
      <td>Permission to view all the notifications sent to a device.</td>
    </tr>
    <tr>
      <th colspan="2">Managing mutual SSL certificate</th>
    </tr>
    <tr>
      <td>
        <p><code>perm:admin:certificates:delete</code></p>
      </td>
      <td>Permission to delete a certificate.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:admin:certificates:details</code></p>
      </td>
      <td>Permission to get details of an SSL certificate.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:admin:certificates:view</code></p>
      </td>
      <td>Permission to get the details of all the uploaded mutual SSL certificates.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:admin:certificates:add</code></p>
      </td>
      <td>Permission to add a new certificate.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:admin:certificates:verify</code></p>
      </td>
      <td>Permission to verify the SSL certificate.</td>
    </tr>
    <tr>
      <th colspan="2">Managing iOS devices</th>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:enroll</code></p>
      </td>
      <td>Permission to enroll an iOS device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:view-device</code></p>
      </td>
      <td>Permission to view the enrolled iOS device details.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:apn</code></p>
      </td>
      <td>Permission to add an Apple Push Notification (APN).</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:ldap</code></p>
      </td>
      <td>Permission to add a Lightweight Directory Access Protocol (LDAP).</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:enterprise-app</code></p>
      </td>
      <td>Permission to install applications that are developed and published by the organization.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:store-application</code></p>
      </td>
      <td>Permission to install applications from the App Store.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:remove-application</code></p>
      </td>
      <td>Permission to uninstall an application.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:app-list</code></p>
      </td>
      <td>Permission to get the list of applications installed on a device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:profile-list</code></p>
      </td>
      <td>Permission to add restrictions or configurations to the device as policies. The device stores the policy restriction or configurations as profiles.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:lock</code></p>
      </td>
      <td>Permission to lock an iOS device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:enterprise-wipe</code></p>
      </td>
      <td>Permission to delete the&nbsp;enterprise-related data on an iOS device and unregister the device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:device-info</code></p>
      </td>
      <td>Permision to get the device information.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:restriction</code></p>
      </td>
      <td>Permission to enforce restrictions on the device via the restriction policy.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:email</code></p>
      </td>
      <td>Permission to add an email.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:cellular</code></p>
      </td>
      <td>Permission to add an Apple cellular payload.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:applications</code></p>
      </td>
      <td>Permission to view the applications installed on an iOS device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:wifi</code></p>
      </td>
      <td>Permission to configure the Wi-Fi settings on the device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:ring</code></p>
      </td>
      <td>Permission to ring the device,</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:location</code></p>
      </td>
      <td>Permission to get the device location.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:notification</code></p>
      </td>
      <td>Permission to send a notification to a device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:airplay</code></p>
      </td>
      <td>Permission to configure settings for connecting to AirPlay destinations.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:caldav</code></p>
      </td>
      <td>Permission to configure the settings for connecting to CalDAV servers.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:cal-subscription</code></p>
      </td>
      <td>Permission to configure settings for calendar subscriptions.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:passcode-policy</code></p>
      </td>
      <td>Permission to configure the password policy</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:webclip</code></p>
      </td>
      <td>Permission to add web clip or a shortcut to a web page.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:vpn</code></p>
      </td>
      <td>Permission to specify the VPN settings.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:per-app-vpn</code></p>
      </td>
      <td>Permission to specify the per app VPN settings.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:app-to-per-app-vpn</code></p>
      </td>
      <td>Permission to specify the app to per app VPN settings.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:app-lock</code></p>
      </td>
      <td>Permission to add an application lock.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:clear-passcode</code></p>
      </td>
      <td>Permission to clear the password on the iOS device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:remove-profile</code></p>
      </td>
      <td>Permission to remove the restrictions that were pushed to the device via the policies. The device stores the policy restriction or configurations as profiles.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:get-restrictions</code></p>
      </td>
      <td>Permission to get the list of restriction that has enforced on the device via the restriction policy.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:ios:wipe-data</code></p>
      </td>
      <td>Permission to format the device.</td>
    </tr>
    <tr>
      <th colspan="2">Managing Android devices</th>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:enroll</code></p>
      </td>
      <td>Permission to enroll an Android device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:wipe</code></p>
      </td>
      <td>Permission to carry out a factory reset operation on the device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:ring</code></p>
      </td>
      <td>Permission to ring the device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:lock-devices</code></p>
      </td>
      <td>Permission to lock the device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:configure-vpn</code></p>
      </td>
      <td>Permission to configure the VPN settings.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:configure-wifi</code></p>
      </td>
      <td>Permission to configure the Wi-Fi settings.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:uninstall-application</code></p>
      </td>
      <td>Permission to uninstall an application that is on a device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:manage-configuration</code></p>
      </td>
      <td>Permission to manage the Android platform configurations.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:location</code></p>
      </td>
      <td>Permission to get the device location.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:install-application</code></p>
      </td>
      <td>Permission to install applications on a device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:mute</code></p>
      </td>
      <td>Permission to mute a device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:change-lock-code</code></p>
      </td>
      <td>Permission to change the device's password.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:blacklist-applications</code></p>
      </td>
      <td>Permissong to blacklist applications.Blacklisting prevents you from using the defined applications. For Android operation systems before Lollipop, when a blacklisted application is clicked a screen is displayed to prevent you from using the app. For the Lollipop Android operating systems and after, the blacklisted apps will be hidden. Blacklisting can be used on both BYOD and COPE devices.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:set-password-policy</code></p>
      </td>
      <td>Permission to define a password policy.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:encrypt-storage</code></p>
      </td>
      <td>Permission to encrypt data on the device, when the device is locked and make it readable when the passcode is entered.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:clear-password</code></p>
      </td>
      <td>Permission to clear the device's password.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:enterprise-wipe</code></p>
      </td>
      <td>Permission to unregister a device</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:info</code></p>
      </td>
      <td>Permission to get the details of a device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:view-configuration</code></p>
      </td>
      <td>Permission to view Android platform configurations.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:upgrade-firmware</code></p>
      </td>
      <td>Permission to upgrade the firmware of the device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:set-webclip</code></p>
      </td>
      <td>Permission to create a web clip or a shortcut to a web page.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:send-notification</code></p>
      </td>
      <td>Permission to send a message to a device or devices.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:disenroll</code></p>
      </td>
      <td>Permission to unregister an Android device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:update-application</code></p>
      </td>
      <td>Permssion to update an application that is installed on the device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:unlock-devices</code></p>
      </td>
      <td>Permission to unlock the device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:control-camera</code></p>
      </td>
      <td>Permission to create a policy to control the device's camera.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:reboot</code></p>
      </td>
      <td>Permission to restart the device.</td>
    </tr>
    <tr>
      <td>
        <p><code>perm:android:logcat</code></p>
      </td>
      <td>Permission to push logcat of the device to the analytics server. Logcat displays messages in real time and keeps a history so you can view the old messages.</td>
    </tr>
    <tr>
      <th colspan="2">Other</th>
    </tr>
    <tr>
      <td>
        <p><code>perm:admin-groups:count</code></p>
      </td>
      <td>Permission to get the total number of device groups as an administrator.</td>
    </tr>
    <tr>
      <td><code>perm:admin-groups:view</code></td>
      <td>Permission to get the device group details as an administrator.</td>
    </tr>
    <tr>
      <td><code>perm:admin</code></td>
      <td>Permission to create OAuth application and to publish and subscribe to APIs.</td>
    </tr>
    <tr>
      <td><code>perm:admin-users:view</code></td>
      <td>Permission to get the details of all the users as an administrator.</td>
    </tr>
    <tr>
      <td><code>perm:view-configuration</code></td>
      <td>Permission to view all the platform configurations.</td>
    </tr>
    <tr>
      <td><code>perm:manage-configuration</code></td>
      <td>Permission to update the platform configurations.</td>
    </tr>
  </tbody>
</table>