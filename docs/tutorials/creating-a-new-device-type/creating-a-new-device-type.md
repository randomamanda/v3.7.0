---
layout: default
title: Creating a Device Type
parent: Tutorials
grand_parent: docs
has_children: true
nav_order: 6
---

# Creating a new Device Type

None of our samples meet your requirement? Want to create your own device type in Entgra IoT Server? If yes, try out the following methods. You no longer need to write complex code to create a new device type. Simply create one via the Entgra IoT Server's device management console or use the Entgra APIs. 