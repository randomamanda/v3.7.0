---
layout: default
title: Creating Device Type via APIs
parent: Creating a Device Type
grand_parent: Tutorials
nav_order: 2
---

# Creating a Device Type via APIs

---

The APIs in Entgra IoT Server are extended as JAX-RS APIs. It allows you to create a device type via APIs. A device type author can consume these APIs to write their own developer friendly APIs and deploy it on an external server or they can directly use the exposed APIs.

There are two types of APIs that are exposed.

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>Device management APIs</th>
      <td>
        <p>Device management APIs are used by the user or application to communicate with the device type or the server. These APIs are used to:</p>
        <ul>
          <li><a href="#CreatingaNewDeviceTypeviaAPIs-Creatinganewdevicetype">Create a new device type</a>.</li>
          <li><a href="#CreatingaNewDeviceTypeviaAPIs-Send_Operations">Send an operation to the device</a>.</li>
          <li><a href="#CreatingaNewDeviceTypeviaAPIs-Retrieve_Data">Retrieve data from the device</a>.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <th>Device agent APIs</th>
      <td>
        <p>Device agent APIs are used by the device to communicate with the server. These APIs are used to:</p>
        <ul>
          <li><a href="#CreatingaNewDeviceTypeviaAPIs-Enrollingadevice">Enroll a device</a>.</li>
          <li><a href="#CreatingaNewDeviceTypeviaAPIs-Retrieve_Operations">Retrieve device operations</a>.</li>
          <li><a href="#CreatingaNewDeviceTypeviaAPIs-Respond_to_Operations">Respond back to a device operation</a>.</li>
          <li><a href="/doc/en/lb2/Creating-a-New-Device-Type-via-APIs.html">Publish data from the device</a>.</li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

**In this tutorial**, you are going to create a new device type using the Entgra IoT Server APIs.



Before you begin



You need to start the Entgra IoT Server core and analytics profiles.

```
cd <IOTS_HOME>/bin  
------Linux/Mac OS/Solaris ----------
./iot-server.sh
./analytics.sh

-----Windows-----------
iot-server.bat
analytics.bat
```





Let's get started!

### Creating a new device type

Follow the steps given below to create a new device type via the REST APIs:

1.  Obtain the access token to create a new device type:

    

    

    

    1.  Encode the client credentials as follows:

    2.  Generate the Client ID and the Secret ID.

    3.  Encode the client credentials as follows:

    4.  Generate the access token using the following command:

        

        

        The access token you generated expires in an hour. After it expires [you can generate a new access token using the refresh token](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822301/Generating+the+Access+Token#GeneratingtheAccessToken-Generatinganewaccesstokenfromtherefreshtoken).

        

        

    

    

2.  Create a device type:

3.  Create the event stream to gather the sensor readings or data from the device.

### Enrolling a new device

Follow the steps given below to enroll the fire alarm using the REST APIs.

1.  Obtain the access token to enroll a new device.

    

    

    

    1.  Encode the client credentials as follows:

    2.  Generate the Client ID and the Secret ID.

    3.  Encode the client credentials as follows:

    4.  Generate the access token using the following command:

        

        

        The access token you generated expires in an hour. After it expires [you can generate a new access token using the refresh token](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822301/Generating+the+Access+Token).

        

        

    

    

2.  Add a new device for the device type you created using the REST APIs.

### Try out more operations

*   **Sending an operation to the device via the REST APIs.**

*   **Retrieve the device data within a given time period.**

*   **Device retrieving pending operations from the server.**  
    By default Entgra IoT Server supports HTTP and MQTT transport protocols. Therefore,  in the HTTP flow, the device polls the server for pending operations and in the MQTT flow, the device receives the operation if it is subscribed to the topic.

*   **Device responding to an operation sent by the server**.

*   **Publishing data from a device.**

## What's next?

*   Want to see other ways you can write your device plugin? See [Writing Device Types](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822027/Writing+Device+Types).
*   Haven't written APIs yet for your device? See [Writing Device APIs](https://docs.wso2.com/display/IoTS310/Writing+Device+APIs).
*   Have you tried adding a UI for your device type? See [Writing UI Extensions](https://docs.wso2.com/display/IoTS310/Writing+UI+Extensions).
*   Does your device type require a device agent? If yes, see [Writing Device Agents](https://docs.wso2.com/display/IoTS310/Writing+Device+Agents).
*   Write the analytics extensions for your device type. For more information, see [Writing Analytics](https://docs.wso2.com/display/IoTS310/Writing+Analytics).