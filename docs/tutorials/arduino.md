---
layout: default
title: Arduino
parent: Tutorials
grand_parent: docs
nav_order: 9
---

# Arduino

---

**In this tutorial**, you will connect an Arduino UNO board to the Entgra IoT Server and visualize the data gathered. The following subsections explain how you can work with the Arduino device type:



Before you begin



*   Make sure to have the following ready:
    *   An Arduino UNO Board.
    *   An Adafruit Wi-Fi Shield for Arduino.
    *   An LED bulb connected to Pin 13\. If you do not have an LED bulb, use the one on the Arduino board. The one on the board is connected to pin 13 by default.
    *   A Resistor (example: 330 ohms)
*   [Download the Arduino software](https://www.arduino.cc/en/Main/Software).
*   Navigate to the `<IOTS_HOME>/plugins` directory and run the `device-plugins-deployer.xml` file.

    ```
    cd <IOTS_HOME>/samples
    mvn clean install -f device-plugins-deployer.xml
    ```

*   Start the Entgra IoT Server broker, core and, analytics profiles in the given order. For more information, see [how to start the Entgra IoT Server](https://docs.wso2.com/display/IoTS310/Running+the+Product#RunningtheProduct-StartingtheServer).

    ```
    cd <IOTS_HOME>/bin

    ------Linux/Mac OS/Solaris ----------
    ./broker.sh
    ./iot-server.sh
    ./analytics.sh

    -----Windows-----------
    broker.bat
    iot-server.bat
    analytics.bat
    ```





Let's get started!

### Starting the Arduino

1.  Sign in to the Device Management console.

    

    

    

    Follow the instructions below to sign in to the Entgra IoT Server device management console:

    1.  If you have not started the server previously, [start the server](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819593/Running+the+Product).

    2.  Access the device management console.

        *   For access via HTTP:```js
            `http://<IOTS_HTTP_HOST>:9763/devicemgt/`
            ```

            For example:` http://localhost:9763/devicemgt/`
        *   For access via secured HTTP:   
            `https://<IOTS_HTTPS_HOST>:9443/devicemgt/` For example:` https://localhost:9443/devicemgt/ `
    3.  Enter the username and password, and click **LOGIN**.

        

        

        *   The system administrator is able to log in using `admin` for both the username and password. However, other users will have to first register with Entgra IoT Server before being able to log into the IoTS device management console. For more information on creating a new account, see [Registering with Entgra IoT Server](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819690/Registering+with+Entgra+IoT+Server).

        *   If you are signing in as a different tenant user, the username needs to be in the following format `<USERNAME>@<TENANT_DOMAIN>`.

        

        

        By logging in you agree with the Entgra IoT Server 3.3.0 cookie policy and privacy policy.  
        ![]({{site.baseurl}}/assets/images/352819653.png)

    4.  Further, you need to provide consent for the details you want Entgra IoT Server to use.  
        ![]({{site.baseurl}}/assets/images/352819626.png)

        The respective device management console will change, based on the permissions assigned to the user.

        For example, the device management console for an administrator is as follows:  
        ![]({{site.baseurl}}/assets/images/352819647.png)

    

    

    

2.  Click **Enroll New Device**.  
    ![]({{site.baseurl}}/assets/images/352817439.png)
3.  Click **Arduino** to enroll a device of the type Arduino.  
    ![]({{site.baseurl}}/assets/images/352817444.png)
4.  Mount the Wi-Fi shield onto the Arduino-UNO board.
5.  Connect the LED bulb to the Arduino as shown below.  
    ![]({{site.baseurl}}/assets/images/352817411.png)
6.  Download the Arduino agent:
    1.  Click **Download Sketch**.
    2.  Enter a preferred name on the download agent form.
    3.  Click **DOWNLOAD NOW**.
7.  Create a folder by the name `ArduinoBoardSketch` and move the downloaded agent and the source files to it.
8.  Configure the following fields in the `ArduinoBoardSketch.h` file, which is in the Arduino agent file you downloaded.
    *   `WLAN_SSID` - Provide the SSID of your Wi-Fi network.

        

        

        The `WLAN_SSID` cannot be longer than 32 characters.

        

        

    *   `WLAN_PASS` - Provide the password of your Wi-Fi network.
    *   `SERVICE_PORT` - Provide the `http` port of the Entgra IoT Server.  
        Example: 9763
    *   `server` - Provide the IP address of the Entgra IoT Server.
    *   `deviceIP` - Provide the static IP address of Arduino.
9.  Burn the sketch onto your Arduino board and run the program.

    1.  [Download the Arduino software](https://www.arduino.cc/en/Main/Software).
    2.  Open the Arduino agent file using the Arduino software.
    3.  Add the `Adafruit_CC3000_Library-master` library. For more information, see [Adafruit_CC3000_Library](https://github.com/adafruit/Adafruit_CC3000_Library).
    4.  Click the upload button to run the program.

When the Arduino starts it will publish the internal temperature to the Entgra IoT Server. 

### Control the bulb

Once you start your Arduino, follow the steps given below to turn on or turn off the bulb connected to the Arduino board.

1.  Click **Control Bulb** under **Operations** on the Device Details page.  
    ![]({{site.baseurl}}/assets/images/352817422.png)
2.  Enter **On** to switch the control bulb on and click **Send to Device**. Enter **Off** to switch the bulb off.  
    ![]({{site.baseurl}}/assets/images/352817450.png)

### View device analytics

Click ![]({{site.baseurl}}/assets/images/352817405.png) to monitor real-time data via the Device Details page.

Example:

![]({{site.baseurl}}/assets/images/352817456.png)

### What's next

Follow the options given below to see what you can do next:

*   Navigate to the Device Management page to view all the devices created by you.

    

    

    

    

    

    1.  [Sign in to the Device Management console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles).
    2.  Click the menu icon.  
    3.  Click **Device Management**.  

    

    

    

    

*   Select a device from your device dashboard to check the available operations and monitor real-time data.  
    ![]({{site.baseurl}}/assets/images/352817433.png)