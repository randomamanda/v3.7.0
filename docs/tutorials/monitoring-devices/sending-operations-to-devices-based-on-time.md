---
layout: default
title: Sending Operations to Devices based on time
parent: Monitoring Devices
grand_parent: Tutorials
nav_order: 3
---

# Sending Operations to Devices based on time

---

In this tutorial, you are configuring Entgra IoT Server to disable the camera on the Android devices that belong to the admin user after 8 AM and enable the camera on the device after 6 PM. The admin user is available by default in Entgra IoT Server. 

MobX wants to disable the camera on the admin user's devices during working hours and enable the camera on the device after working hours. This is required because the admin has access to confidential data.

**Let's get started!**

### Create the event stream

Follow the steps given below to create the `org.wso2.iot.triggerStream` stream that is required for the execution plan that generates two events.

1.  Start Entgra IoT Server:

    1.  Start the Entgra IoT Server broker profile.

        ```
        cd <IOTS_HOME>/bin
        sh broker.sh
        ```

    2.  Start the Entgra IoT Server core profile.

        ```
        cd <IOTS_HOME>/bin
        sh iot-server.sh
        ```

    3.  Next, start the Entgra IoT Server analytics profile.

        ```
        cd <IOTS_HOME>/bin
        sh analytics.sh
        ```

2.  Access the Entgra IoT Server's analytics management console.
    1.  For access via secured HTTP: `https://<IOTS_HTTPS_HOST>:9445/carbon/` For example: `https://localhost:9445/carbon/`

    2.  For access via HTTP: ` `http://<IOTS_HTTP_HOST>:9765/carbon/` `

        For example: `http://localhost:9765/carbon/`
3.  On the **Main** tab click **Streams** that is under Events.

4.  Click **+ Add Events Streams**.

5.  Enter the following values:

    <table style="width: 98.937%;">
      <colgroup>
        <col style="width: 20.5069%;">
        <col style="width: 79.4931%;">
      </colgroup>
      <tbody>
        <tr>
          <th>Event Stream Name</th>
          <td>
            <p><code>org.wso2.iot.triggerStream</code></p>
          </td>
        </tr>
        <tr>
          <th>Event Stream Version</th>
          <td>
            <p><code>1.0.0</code></p>
          </td>
        </tr>
        <tr>
          <th>Payload Data Attributes</th>
          <td>Add a payload data attribute by entering the details given below and clicking <strong>Add</strong>.<ul>
              <li><strong>Attribute Name</strong> : <code>isLocked</code></li>
              <li>
                <p>Attribute Type: Select <strong>bool</strong> from the drop-down list</p>
              </li>
            </ul>
          </td>
        </tr>
      </tbody>
    </table>

6.  Click **Add Event Stream**.

### Create the execution plan to generate two events for 8 AM and 6 PM

You need to create an execution plan to disable and enable the camera on the devices during the specified time. Follow the steps given below:

1.  On the **Main** tab, click **Execution Plans**.
2.  Click **Add Execution Plan**.
3.  Copy the execution plan that is given below and replace the sample content that is in the text box.

    ```
    @Plan:name('TimedTriggers')

    /* define streams/tables and write queries here ... */
    define trigger cronLockTriggerStream at '0 0 18 * * ?' ; /* Disable the camera on the device at 18:00 '0 0 18 * * ?' */
    define trigger cronUnlockTriggerStream at '0 0 8 * * ?' ; /* Enable the camera on the device at 08:00 '0 0 8 * * ?' */

    @Export('org.wso2.iot.triggerStream:1.0.0')
    define stream dataOut (isLocked bool);

    /* Send the camera disable operation at 18:00 */
    from cronLockTriggerStream
    select true as isLocked
    insert into dataOut;

    /* Send the camera enable operation at 8:00 */
    from cronUnlockTriggerStream
    select false as isLocked
    insert into dataOut;
    ```

4.  Click **Add Execution Plan**.

### Create the execution plan to retrieve the active devices that belong to the admin user

The above execution plan is not aware of the devices that belong to the admin user. Therefore, you need to write another execution plan to get the admin users devices so that the execution plan you created above can be executed on the filtered devices. This can be done using the [Entgra IoT Server Siddhi Extensions](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822814/Entgra++IoT+Server+Siddhi+Extensions).

Follow the steps given below:

1.  On the **Main** tab, click **Execution Plans**.
2.  Click **Add Execution Plan**.
3.  Copy the execution plan that is given below and replace the sample content that is in the text box.

    ```
    @Plan:name('TimedTasks_operations')

    @Import('org.wso2.iot.triggerStream:1.0.0')
    define stream dataIn (isLocked bool);

    @Export('org.wso2.iot.operation:1.0.0')
    define stream dataOut (meta_deviceIdentifiers string, meta_deviceType string, code string, type string, isEnabled bool, payLoad string);

    /* Filter the amin users devices to enable and disable the camera */
    from dataIn[device:hasDevicesOfUser("shavi", "android", "ACTIVE")]
    select device:getDevicesOfUser("shavi", "android", "ACTIVE") as meta_deviceIdentifiers, "android" as meta_deviceType, "CAMERA" as code, "COMMAND" as type, not isLocked as isEnabled, "{}" as payLoad
    insert into dataOut;
    ```

    

    

    Want to know more about what happens in the execution plan?

    

    

    

    *   First, you need to define the import the stream where you get the data from and export the stream to which the data is inserted.

        ```
        @Import('org.wso2.iot.triggerStream:1.0.0')
        define stream dataIn (isLocked bool);

        @Export('org.wso2.iot.operation:1.0.0')
        define stream dataOut (meta_deviceIdentifiers string, meta_deviceType string, code string, type string, isEnabled bool, payLoad string);
        ```

    *   Next, you need to filter out the devices that belong to the admin user. This is done using the [`device:hasDevicesOfUser`](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822814/Entgra++IoT+Server+Siddhi+Extensions#EntgraIoTServerSiddhiExtensions-Confirmingifauserhasaspecificdevicetypeenrolled) extension.
    *   Then you convert the data you want to send to the output stream to match the data types of the exported output stream.  
        For example, the output stream does not recognize `CAMERA`. Therefore, it is equaled to `code` using the term `as`.

        `"CAMERA" as code`

    *   After converting the data to the format that the output stream identifies, you need to insert the data to the output stream.

    

    

    

    

4.  Click **Add Execution Plan**.

**Congratulations!** You have successfully configured Entgra IoT Server to enable the camera on the admin user's device after 6 PM and disable it after 8 AM.

### Try it out

Follow the steps given below to try out the scenario given above.

1.  [Sign in to the Entgra IoT Server device management console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles#AccessingtheEntgraIoTServerConsoles-AccessingtheDeviceManagementConsole) using `admin` as the username and `admin` as the password.

2.  Enroll an Android or iOS device.

    

    

    *   For more information on how to enroll an Android device, see [Android](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352816232/Android).
    *   For more information on how to enroll an iOS device, see [iOS](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352817480/iOS).

    

    

3.  Change the time on the `TimedTriggers` execution plan to match your current time. If you want the camera to be disabled at 12.50 PM, define `0 50 12 * * ?` for the `cronLockTriggerStream` property.  
    Example:

    `define trigger cronLockTriggerStream at '0 50 12 * * ?' ; /* Disable the camera on the device at 12:50 '0 50 12 * * ?' */`

    Now, you see that the camera can't be accessed at 12.50 PM.  
    ![]({{site.baseurl}}/assets/images/352819473.png)

4.  Similarly, change the time on the `TimedTriggers` execution plan to match your current time. If you want to enable the camera on the device at 1 PM, define `0 0 13 * * ?` for the `cronUnlockTriggerStream` property.  
    Example:

    `define trigger cronUnlockTriggerStream at '0 0 13 * * ?' ; /* Enable the camera on the device at 13:00 '0 0 13 * * ?' */`

    Now, you see that you can access the camera after 1 PM.

### What's next?

*   Want to know more about the Entgra IoT Server Siddhi extensions? See [Entgra IoT Server Siddhi Extensions](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822814/Entgra++IoT+Server+Siddhi+Extensions).
*   Want to try out other tutorials that allow you to monitor devices?
    *   [Applying Policies on Devices Based on Geofencing](/doc/en/lb2/Applying-Policies-on-Devices-Based-on-Geofencing.html)
    *   [Analyzing the Data Gathered from Devices](/doc/en/lb2/Analyzing-the-Data-Gathered-from-Devices.html)