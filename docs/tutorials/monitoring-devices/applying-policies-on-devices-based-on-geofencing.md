---
layout: default
title: Applying policies on devices based on geofencing
parent: Monitoring Devices
grand_parent: Tutorials
nav_order: 2
---

# Applying policies on devices based on geofencing

---

In this tutorial, you are writing an execution plan using the [Entgra IoT Server Siddhi extensions](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822814/Entgra++IoT+Server+Siddhi+Extensions) to disable the camera once the device enters a specified geo area and to enable the camera one the device leaves the specified geo area.

Entgra IoT Server's analytics profile that runs on WSO2 Data Analytics Server (WSO2 DAS) uses execution plans to store the event processing logic. An execution plan is bound to an instance of the Siddhi Data Analytics Server runtime, which is responsible for the actual processing of events.

MobX employees bring their own devices to work. To ensure the security of corporate data, MobX wants the camera on the mobile devices to be disabled once the devices are within the office premise. Therefore, the MobX administrators write an execution plan using Entgra IoT Server to disable the camera on the devices once the devices enter the office premise and to enable the camera on the devices once they leave the office premise.

**Let's get started!**

### Setting up geofencing 

1.  [Download the Entgra IoT Server](http://wso2.com/iot).

    

    

    If you are running Entgra IoT Server, stop the server before proceeding to the next step.

    

    

2.  Navigate to the `<IOTS_HOME>/wso2/analytics/scripts` directory via the terminal and run the following command. This installs the geo extension feature to Entgra IoT Server.

    `mvn clean install -f siddhi-geo-extention-deployer.xml`

3.  Open the `<IOTS_HOME>/conf/`cdm-config.xml`` file and enable the `GeoLocationConfiguration` and `PublishLocationResponse` configurations.

    This enables the geo extension feature in Entgra IoT Server.

    ```
    <GeoLocationConfiguration>
        <Enabled>true</Enabled>
    </GeoLocationConfiguration>
    <OperationAnalyticsConfiguration>
        <PublishLocationResponse>true</PublishLocationResponse>
        <PublishDeviceInfoResponse>false</PublishDeviceInfoResponse>
        <PublishOperationResponse>
            <Enabled>false</Enabled>
            <Operations>
                <!-- Publish specific operation responses -->
                <!--
                    <Operation>BATTERY_LEVEL</Operation>
                    <Operation>CHECK_LOCK_STATUS</Operation>
                -->
                <!-- use wildcard '*' to publish all responses -->
                <Operation>*</Operation>
            </Operations>
        </PublishOperationResponse>
    </OperationAnalyticsConfiguration>
    ```

4.  Start the Entgra IoT Server core, and analytics profiles, and sign in to the Entgra IoT Server Device Management Console.

    

    

    

    

    1.  Start the Entgra IoT Server core profile.

        ```
        cd <IOTS_HOME>/bin
        sh iot-server.sh
        ```

    2.  Next, start the Entgra IoT Server analytics profile.

        ```
        cd <IOTS_HOME>/bin
        sh analytics.sh
        ```

    3.  Access the device management console.

        *   For access via secured HTTPS: `https://<IOTS_HTTPS_HOST>:<IOTS_HTTPS_PORT>/devicemgt/ `

        *   For example:` https://localhost:9443/devicemgt/ `
    4.  Enter the username and password, and sign in.

        

        

        The system administrator will be able to log in using `admin` for both the username and password. However, other users will have to first register with Entgra IoT Server before being able to log into the IoT Server device management console. For more information on creating a new account, see [Registering with Entgra IoT Server](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819690/Registering+with+Entgra+IoT+Server).

        

        

        ![]({{site.baseurl}}/assets/images/352822708.png)

    5.  Click **LOGIN**. The respective device management console will change, based on the permissions assigned to the user.  
        For example, the device management console for an administrator is as follows:

        ![]({{site.baseurl}}/assets/images/352822702.png)

    

    

    

    

    Deploying Analytics Artifacts

    

    The geo analytics artifacts needs to be deploy per tenant manually. 

    1.  Log in to the device management console using admin credentials.

    2.  Click the![]({{site.baseurl}}/assets/images/2.png) button and select **Configuration Management > Platform Configurations**.

    3.  Click the **Deploy Geo Analytics Artifacts** button. If required, you can use this button to re-deploy the geo analytics artifacts in the super tenant mode.  
        ![]({{site.baseurl}}/assets/images/352822690.png)

    

    

### Creating the execution plan

In this step, you are creating the execution plan that contains the instructions on disabling and enabling the camera once the device enters and leaves the geo area respectively.

1.  Access the Entgra IoT Server's analytics management console: `https://<IOTS_HTTPS_HOST>:9445/carbon` For example:` https://localhost:9445/carbon/`
2.  Enter the Administrator credentials and sign in. The default administrator uses `admi`n as the username and `admin` as the password.
3.  On the **Main** tab, click **Execution Plans**.
4.  Click **Add Execution Plan**.
5.  Copy the execution plan that is given below and replace the sample content that is in the text box.

    

    

    Want to know what happens in the execution plan?

    

    

    

    Android and iOS devices that are enrolled with Entgra IoT Server publishes the location updates to the `org.wso2.iot.LocationStream`. The execution plan that is given below is implemented to check the devices that belong to the group that has the ID 1\. By default, in Entgra IoT Server the Bring Your Own Device (BYOD) group has the group ID 1\. If the device belongs to this group the device or devices are monitored to know if they enter or leave the geofence area.

    If the device is inside the geofence, the camera on the device is disabled as the camera disable policy is enforced by the execution plan into the `org.wso2.iot.operation` stream. Similarly, if the device leaves the geofenced area, the camera is enabled as the camera enable policy is enforced by the execution plan into the `org.wso2.iot.operation` stream.

    Further, the device is notified of the camera enable and disable status using the notification operation.

    

    

    

    

    **GeoFence_OfficeArea_operations execution plan**

    ```
    @Plan:name('GeoFence_OfficeArea_operations')

    @Plan:statistics('false')

    @Import('org.wso2.iot.LocationStream:1.0.0')
    define stream dataIn (meta_deviceId string, meta_deviceType string, timeStamp long, latitude double, longitude double);

    @Export('org.wso2.iot.operation:1.0.0')
    define stream dataOut (meta_deviceIdentifiers string, meta_deviceType string, code string, type string, isEnabled bool, payLoad string);

    define stream deviceWithinFenceStream (meta_deviceId string, meta_deviceType string, isWithin bool);
    define stream deviceWithinFencePatternedStream (meta_deviceIdentifiers string, meta_deviceType string, isWithin bool);

    /* Check if the device is within the geofence. */
    from dataIn [device:isInGroup(1, meta_deviceId, meta_deviceType)]
    select meta_deviceId, meta_deviceType, geo:within(longitude,latitude,"{'type':'Polygon','coordinates':[[[79.85213577747345, 6.909673257977737],[79.85266149044037,6.909673257977737],[79.85266149044037,6.91003538888127],[79.85213577747345,6.91003538888127],[79.85213577747345,6.909673257977737]]]}") as isWithin
    insert into deviceWithinFenceStream;

    /* Notify if the device moves across the geofence. */
    partition with ( meta_deviceId of deviceWithinFenceStream )
    begin
        from every fs1=deviceWithinFenceStream, fs2=deviceWithinFenceStream[fs1.isWithin != isWithin]
        select json:getArray(fs2.meta_deviceId) as meta_deviceIdentifiers, fs2.meta_deviceType, fs2.isWithin
        insert into deviceWithinFencePatternedStream;
    end;

    /* Push the camera policy to the device if the device enters the geofenced area. */
    from deviceWithinFencePatternedStream
    select meta_deviceIdentifiers, meta_deviceType, "CAMERA" as code, "COMMAND" as type, not isWithin as isEnabled, "{}" as payLoad
    insert into dataOut;

    /* Send a notification to the device indicating that the camera is disabled. */
    from deviceWithinFencePatternedStream [isWithin == true]
    select meta_deviceIdentifiers, meta_deviceType, "NOTIFICATION" as code, "PROFILE" as type, true as isEnabled, "{'messageText':'Your are in a restricted area that does not allows your to use the camera.','messageTitle':'Camera Disabled'}" as payLoad
    insert into dataOut;

    /* Send a notification to the device indicating that the camera is enabled.  */
    from deviceWithinFencePatternedStream [isWithin == false]
    select meta_deviceIdentifiers, meta_deviceType, "NOTIFICATION" as code, "PROFILE" as type, true as isEnabled, "{'messageText':'You able to use the camera as you are no longer in the restricted area.','messageTitle':'Camera Enabled'}" as payLoad
    insert into dataOut;
    ```

6.  Click **Add Execution Plan**.

**Congratulations**, you have successfully configured Entgra IoT Server to enable and disable the camera based on the geo area!

### Try it out

To try this tutorial out you need to enroll an Android Virtual Device, and follow the steps given below:

1.  Enroll an Android Virtual Device as a Bring Your Own Device (BYOD). By default, the BYOD device group has the group ID 1.

    

    

    For more information, see [Android Virtual Device](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352817149/Android+Virtual+Device).

    

    

2.  Updating the location of the device so that it is inside the geofenced area:

    1.  After the Android virtual device starts and it is enrolled with Entgra IoT Server, click the more button.  
        ![]({{site.baseurl}}/assets/images/352819431.png)

    2.  Click **Location**, define the **Longitude** as `79.8525` and **Latitude** as `6.9099`, and click **Send**.  
        These coordinates are within the geofenced area that is defined in the [execution plan](about:blank#ApplyingPoliciesonDevicesBasedonGeofencing-execution-plan).

        

        

        In order to save the device's energy, the Entgra IoT Server's Android agent sends the device location to the server after five minutes from the last location update. Therefore, it takes a maximum of five minutes to update the location change. To verify if the location is updated, go to the enrolled device's page and click the location tab after five minutes.

        

        

        ![]({{site.baseurl}}/assets/images/352819437.png)

        Five minutes after your last location update you see a notification on the Android Virtual Device mentioning that the device's camera is disabled.  
        The [execution plan you created](about:blank#ApplyingPoliciesonDevicesBasedonGeofencing-execution-plan), disables the device's camera when you enter the geofenced area.

        ![]({{site.baseurl}}/assets/images/352819419.png)

        Now, when you try to access the camera you see the following message:

        ![]({{site.baseurl}}/assets/images/352819425.png)

3.  Updating the location of the device so that it is outside the geofenced area:
    1.  Click the more button on the Android Virtual Device.  
        ![]({{site.baseurl}}/assets/images/352819431.png)
    2.  Click **Location**, define the **Longitude** as `78.8525` and **Latitude** as `6.9099`, and click **Send**. These coordinates are outside of the geofenced area that is defined in the [execution plan](about:blank#ApplyingPoliciesonDevicesBasedonGeofencing-execution-plan).

        Five minutes after your last location update you see a notification on the Android Virtual Device mentioning that the device's camera is enabled.  
        The [execution plan you created](about:blank#ApplyingPoliciesonDevicesBasedonGeofencing-execution-plan), enables the device's camera when you move out of the geofenced area.  
        ![]({{site.baseurl}}/assets/images/352819413.png)

    3.  Try to access the device camera. You see that you are able to access it. 

        

        

        

        If the Android virtual device is unable to connect to your machine's camera, you get a message saying **Camera Error**. It is not caused by Entgra IoT Server.

        

        

### What's next?

*   Want to know more about the Entgra IoT Server Siddhi extensions? See [Entgra IoT Server Siddhi Extensions](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352822814/Entgra++IoT+Server+Siddhi+Extensions).
*   Try out the tutorials listed below and see how the extensions described above are put into use.

    *   [Sending Operations to Devices Based on Time](/doc/en/lb2/Sending-Operations-to-Devices-Based-on-Time.html)

    *   [Analyzing the Data Gathered from Devices](/doc/en/lb2/Analyzing-the-Data-Gathered-from-Devices.html)