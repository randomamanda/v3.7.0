---
layout: default
title: iOS
parent: Tutorials
grand_parent: docs
has_children: true
nav_order: 4
---

# iOS

---

You can enroll an iOS device with Entgra IoT Server with or without the iOS agent. Note that the device ring, send messages, and the get device location operations require the Entgra IoT Server iOS agent, else they don't work. 