---
layout: default
title: Enrolling DEP devices
parent: iOS
grand_parent: Tutorials
nav_order: 2
---

# Enrolling DEP devices

---



Before you begin!



Make sure that the EMM administrator has set up Entgra IoT Server's Enterprise Mobility Manager (EMM) solution to enroll Device Enrollment Program (DEP) devices. For more information, see [Device Enrollment Program](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352824389/Device+Enrollment+Program).





*   All you need to do is start up the iOS device that was given to you by your organization and your device will be enrolled with Entgra EMM as a DEP device.
*   If your organization has configured the DEP profile to prompt for your username and password, you need to enter the username and password that is used within the organization.