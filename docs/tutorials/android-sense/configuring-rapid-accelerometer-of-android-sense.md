---
layout: default
title: Configuring Rapid Accelerometer of Android Sense
parent: Android Sense
grand_parent: Tutorials
nav_order: 3
---

# Configuring Rapid Accelerometer of Android Sense

---

This section will guide you on how to configure the Android Sense to get alerts on rapid accelerations and device turns. The accelerometer sensor on the Android Sense device type helps the user identify sudden vigorous movements of the device if it exceeds 60 kph (kilometer per hour), and notify the user of such incidents via email. 

**Prerequisite**



*   Clone the Carbon Device Management Plugins repository. It will be referred to as `<CDM-PLUGINS>` throughout this document.

    `git clone https://github.com/wso2/carbon-device-mgt-plugins.git`

*   Create an email account (e.g., [no-reply@foo.com](mailto:no-reply@foo.com)) to configure the output email adapter. You can use the same email account that was used for the [Entgra IoTS email configurations](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819965/Configuring+the+Email+Settings).



Follow the steps given below to configure the accelerometer sensor: 

1.  Configure the turn and speed subscriber emails address.

    

    

    When the Android Sense detects that the device has exceeded the acceleration of 60 kph it notifies Entgra IoTS, which then notifies the user via email. The email will be sent to the email address that is configured here.

    

    

    1.  Navigate to the ```
        <CDM-PLUGINS>/components/iot-plugins/iot-analytics/org.wso2.carbon.device.mgt.iot.analytics/src/main/resources/carbonapps/speed_sensor/speed_publisher/  
        speed_publisher.xml
        ``` file and configure the email settings.

        

        

        

        

        ```
        <eventPublisher xmlns="http://wso2.org/carbon/eventpublisher" name="speed_publisher" statistics="disable" trace="disable">
           <from streamName="org.wso2.iot.devices.speed" version="1.0.0" />
           <mapping customMapping="disable" type="text" />
           <to eventAdapterType="email">
              <property name="email.subject">Email Alerts Speed</property>
              <property name="email.address">{ENTER YOUR EMAIL ADDRESS}</property>
              <property name="email.type">text/html</property>
           </to>
        </eventPublisher>
        ```

        Example:

        ```
        <eventPublisher xmlns="http://wso2.org/carbon/eventpublisher" name="speed_publisher" statistics="disable" trace="disable">
           <from streamName="org.wso2.iot.devices.speed" version="1.0.0" />
           <mapping customMapping="disable" type="text" />
           <to eventAdapterType="email">
              <property name="email.subject">Email Alerts Speed</property>
              <property name="email.address">bob@gmail.com</property>
              <property name="smtp">text/html</property>
           </to>
        </eventPublisher>
        ```

        

        

        

    2.  Navigate to the ```
        <CDM-PLUGINS>/components/iot-plugins/iot-analytics/org.wso2.carbon.device.mgt.iot.analytics/src/main/resources/carbonapps/turn_sensor/turn_publisher/  
        turn_publisher.xml
        ``` file and configure the email settings.

        

        

        

        

        ```
        <eventPublisher xmlns="http://wso2.org/carbon/eventpublisher" name="turn_publisher" statistics="disable" trace="disable">
           <from streamName="org.wso2.iot.devices.turn" version="1.0.0" />
           <mapping customMapping="disable" type="text" />
           <to eventAdapterType="email">
              <property name="email.subject">Email Alerts Turn</property>
              <property name="email.address">{DEFINE THE EMAIL ADDRESS}</property>
              <property name="email.type">text/html</property>
           </to>
        </eventPublisher>
        ```

        Example:

        ```
        <eventPublisher xmlns="http://wso2.org/carbon/eventpublisher" name="turn_publisher" statistics="disable" trace="disable">
           <from streamName="org.wso2.iot.devices.turn" version="1.0.0" />
           <mapping customMapping="disable" type="text" />
           <to eventAdapterType="email">
              <property name="email.subject">Email Alerts Turn</property>
              <property name="email.address">user@gmail.com</property>
              <property name="email.type">text/html</property>
           </to>
        </eventPublisher>
        ```

        <table>
          <tbody>
            <tr>
              <th>Adapter Property</th>
              <th>Description</th>
              <th>Configurationfileproperty</th>
              <th>Example</th>
            </tr>
            <tr>
              <td><strong>Email Address</strong></td>
              <td>Email address of the client. Register the publisher for multiple email IDs' by separating them with commas.</td>
              <td>
                
              </td>
              <td>
                <p><code><a href="mailto:user@gmail.com" class="external-link" rel="nofollow">user@gmail.com</a></code></p>
              </td>
            </tr>
            <tr>
              <td><strong>Subject</strong></td>
              <td>The subject of the email to be sent to the defined email address</td>
              <td>
                
              </td>
              <td>
                <p><code>This is a test mail.</code></p>
              </td>
            </tr>
            <tr>
              <td><strong>Email Type</strong></td>
              <td>
                <p>The email format to be sent to the defined email address.</p>
                
                  
                    <p>If you select <code>text/html</code> for this parameter, the message body should be in a valid HTML format.</p>
                  
                
              </td>
              <td>
                
              </td>
              <td><code>text/plain</code></td>
            </tr>
          </tbody>
        </table>

        

        

        

2.  Navigate to the `<CDM-PLUGINS>/components/`iot`-plugins/`iot`-analytics` directory via the terminal and build it.

    `mvn clean install`

3.  Copy the `speed.car` file and `turn.car` files from the `<CDM_PLUGINS>/components/iot-plugins/iot-analytics/org.wso2.carbon.device.mgt.iot.analytics/target/carbonapps` directory.
4.  Paste the copied .car files in the `<IoTS_HOME>/repository/deployment/server/carbonapps` directory.
5.  Configure the email adapter settings that is under `<adapterConfig type="email">` in the `<IoTS_HOME>/conf/output-event-adapters.xml` file.

    

    

    You may need to enable the "Allow less secure apps" option in the Gmail [account security settings](https://myaccount.google.com/security), to connectaccount to Entgra products.

    

    

    

    

    

    

    ```
    <adapterConfig type="email">
       <!-- Comment mail.smtp.user and mail.smtp.password properties to support connecting SMTP servers which use trust
            based authentication rather username/password authentication -->
       <property key="mail.smtp.from">abcd@gmail.com</property>
       <property key="mail.smtp.user">abcd</property>
       <property key="mail.smtp.password">xxxx</property>
       <property key="mail.smtp.host">smtp.gmail.com</property>
       <property key="mail.smtp.port">587</property>
       <property key="mail.smtp.starttls.enable">true</property>
       <property key="mail.smtp.auth">true</property>
       <!-- Thread Pool Related Properties -->
       <property key="minThread">8</property>
       <property key="maxThread">100</property>
       <property key="keepAliveTimeInMillis">20000</property>
       <property key="jobQueueSize">10000</property>
    </adapterConfig>
    ```

    *   `MinThreads` : Defines the minimum number of threads that needs to be available in the underlying thread pool when the email sender functionality is initialized.

    *   `MaxThreads` : Defines the maximum number of threads that should serve email sending at any given time.

    *   `KeepAliveTimeInMillis` : Defines the duration a connection should be kept alive. If the thread pool has initialized more connections than what was defined in `MinThreads`, and they have been idle for more than the `KeepAliveDuration`, those idle connections will be terminated

    *   `JobQueueSize` : Defines the maximum concurrent email sending tasks that can be queued up.

    Example:

    ```
    <adapterConfig type="email">
       <!-- Comment mail.smtp.user and mail.smtp.password properties to support connecting SMTP servers which use trust
            based authentication rather username/password authentication -->
       <property key="mail.smtp.from">no-reply@foo.com</property>
       <property key="mail.smtp.user">foo</property>
       <property key="mail.smtp.password">$foo1234</property>
       <property key="mail.smtp.host">smtp.gmail.com</property>
       <property key="mail.smtp.port">587</property>
       <property key="mail.smtp.starttls.enable">true</property>
       <property key="mail.smtp.auth">true</property>
       <!-- Thread Pool Related Properties -->
       <property key="minThread">8</property>
       <property key="maxThread">100</property>
       <property key="keepAliveTimeInMillis">20000</property>
       <property key="jobQueueSize">10000</property>
    </adapterConfig>
    ```

    

    

    

6.  Restart Entgra IoTS.

    `./wso2server.sh`

7.  [Install the Android Sense `.apk` and publish data](/doc/en/lb2/Android-Sense.html).