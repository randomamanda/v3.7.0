---
layout: default
title: Android Sense
parent: Tutorials
grand_parent: docs
has_children: true
nav_order: 2
---

# Android Sense

---

**In this tutorial**, you will connect an Android device to Entgra IoT Server and monitor the sensor data.





Entgra IoT Server supports devices on Android version 4.2.x to 7.0 (Android Jelly Beans to Nougat)





The following subsections explain how you can work with the Android Sense device type:



Before you begin



 Start the Entgra IoT Server broker, core and, analytics profiles in the given order. For more information, see [how to start the Entgra IoT Server](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819593/Running+the+Product#RunningtheProduct-StartingtheServer).



```
cd <IOTS_HOME>/bin

------Linux/Mac OS/Solaris ----------
./broker.sh
./iot-server.sh
./analytics.sh

-----Windows-----------
broker.bat
iot-server.bat
analytics.bat
```







### Enroll Android Sense

1.  Sign in to the Device Management console.

    

    

    

    Follow the instructions below to sign in to the Entgra IoT Server device management console:

    1.  If you have not started the server previously, [start the server](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819593/Running+the+Product).

    2.  Access the device management console.

        *   For access via HTTP:```js
            `http://<IOTS_HTTP_HOST>:9763/devicemgt/`
            ```

            For example:` http://localhost:9763/devicemgt/`
        *   For access via secured HTTP:   
            `https://<IOTS_HTTPS_HOST>:9443/devicemgt/` For example:` https://localhost:9443/devicemgt/ `
    3.  Enter the username and password, and click **LOGIN**.

        

        

        *   The system administrator is able to log in using `admin` for both the username and password. However, other users will have to first register with Entgra IoT Server before being able to log into the IoTS device management console. For more information on creating a new account, see [Registering with Entgra IoT Server](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819690/Registering+with+Entgra+IoT+Server).

        *   If you are signing in as a different tenant user, the username needs to be in the following format `<USERNAME>@<TENANT_DOMAIN>`.

        

        

        By logging in you agree with the Entgra IoT Server 3.3.0 cookie policy and privacy policy.  
        ![]({{site.baseurl}}/assets/images/352819653.png)

    4.  Further, you need to provide consent for the details you want Entgra IoT Server to use.  
        ![]({{site.baseurl}}/assets/images/352819626.png)

        The respective device management console will change, based on the permissions assigned to the user.

        For example, the device management console for an administrator is as follows:  
        ![]({{site.baseurl}}/assets/images/352819647.png)

    

    

    

2.  Click **Enroll New Device**.  
    ![]({{site.baseurl}}/assets/images/352816972.png)

    

    

    If you are an **Admin** user, click the ![]({{site.baseurl}}/assets/images/352816890.png)menu icon, and select **DEVICE MANAGEMENT**, to access the **ENROLL DEVICE** button.

    ![]({{site.baseurl}}/assets/images/352816948.png)

    

3.  Click **Android Sense** to enroll a device of the type Android Sense.  
    ![]({{site.baseurl}}/assets/images/352816884.png)
4.  Enroll the device.

5.  Open the downloaded file, tap **NEXT**, and tap **INSTALL**.  
    ![]({{site.baseurl}}/assets/images/352817049.png)

6.  Tap **OPEN**, once the Entgra-SenseAgent is installed.
7.  Enter your details and tap **REGISTER DEVICE**.
    *   Username: Your Entgra IoT Server username
    *   Password: The password you used to sign into Entgra IoT Server
    *   Server URL: Provide the server URL in the `https://<IOTS_HOST>:<IOTS_HTTPS_PORT>` format.  
        Example:  `https://10.100.4.98:8243`![]({{site.baseurl}}/assets/images/10.png)
8.  Try out the Android Sense application.  
    ![]({{site.baseurl}}/assets/images/352817007.png)  

    *   If you want to add sensors, tap add sensor ![]({{site.baseurl}}/assets/images/352816960.png), select the sensors you wish to add, and tap **OK**.

        

        

        For more information on configuring the **accelerometer** to work with Android Sense, see [Configuring the Rapid Accelerometer of the Android Sense](/doc/en/lb2/Configuring-the-Rapid-Accelerometer-of-the-Android-Sense.html).

        

        

        ![]({{site.baseurl}}/assets/images/352816954.png)

    *   If you want to push the data to the Entgra IoT Server so you can view the data in real time or check out the historical data, tap push data to device![]({{site.baseurl}}/assets/images/352816966.png).

### View device analytics

To monitor real-time device statistics,

1.  Click on the device icon available on the Devices page, and navigate to Device Details page.  
    ![]({{site.baseurl}}/assets/images/352816931.png)
2.  To view location-based device statistics, follow the steps below: 
    1.  Install the geo extension feature to Entgra IoT Server, by navigating to `<IoTS_HOME>/wso2/analytics/scripts` directory and running the following command. 

        `mvn clean install -f siddhi-geo-extention-deployer.xml`

    2.  Enable the geo extension feature in Entgra IoT Server, by opening the `cdm-config.xml` file in the `<IoTS_HOME>/conf` directory and setting the following properties under the `<GeoLocationConfiguration>` tag to true. 

        ```
        <GeoLocationConfiguration>      
        	<isEnabled>true</isEnabled>
            <PublishLocationOperationResponse>true</PublishLocationOperationResponse>
        </GeoLocationConfiguration>
        ```

    3.  Restart the Entgra IoT Server broker, core and, analytics profiles in the given order 

        ```
        cd <IOTS_HOME>/bin

        ------Linux/Mac OS/Solaris ----------
        ./broker.sh
        ./iot-server.sh
        ./analytics.sh

        -----Windows-----------
        broker.bat
        iot-server.bat
        analytics.bat
        ```

    4.  Click on the Device Location tab. 

        ![]({{site.baseurl}}/assets/images/352816937.png)

        

        

        For more information on location-based services, see [Monitoring Devices Using Location Based Services](https://docs.wso2.com/display/IoTS310/Monitoring+Devices+Using+Location+Based+Services). 

        

        

### What's next

Follow the options given below to see what you can do next:

*   Try out building the Android Sense source. For more information, see [Building the Android Sense Agent](/doc/en/lb2/Building-the-Android-Sense-Agent.html).

*   Navigate to the Device Management page to view all the created devices.

    

    

    

    

    

    1.  [Sign in to the Device Management console](https://entgra.atlassian.net/wiki/spaces/IoTS370/pages/352819602/Accessing+the+Entgra+IoT+Server+Consoles).
    2.  Click the menu icon.  
    3.  Click **Device Management**.  

    

    

    

    

*   Select a device from your device dashboard and check the available operations and monitor real-time data.
