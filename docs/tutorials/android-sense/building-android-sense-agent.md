---
layout: default
title: Building Android Sense agent
parent: Android Sense
grand_parent: Tutorials
nav_order: 2
---

# Building Android Sense agent

---

You will want to make changes to the Android Application Package (APK) of the Android Sense, to suite your business or organization requirements. For example, white labelling the Entgra IoTS Android Sense agent, debugging the agent or building the agent manually. In such situations you need to make the required changes and create a new APK file. 

### Prerequisites

1.  Download, and install Android Studio.

    

    

    For more information, see [installing Android Studio](http://developer.android.com/intl/vi/sdk/installing/index.html?pkg=studio).

    

    

2.  Entgra IoTS supports API levels 16 to 22\. Therefore, install the Android API levels 16 to 22 on Android Studio.

1.  1.  Open Android Studio.
    2.  Click **SDK Manager**. **![]({{site.baseurl}}/assets/images/2.png)**
    3.  Click the **SDK Platform **tab, and select the 16 and 22 API levels.  
        ![]({{site.baseurl}}/assets/images/352817126.png)
    4.  Click **Apply.**

### Creating a new APK

1.  Clone the `carbon-device-mg-plugins` GIT repository.

    `git clone https://github.com/wso2/carbon-device-mgt-plugins.git`

2.  Open the `org.wso2.carbon.device.mgt.iot.androidsense.agent` folder that is in the `<CARBON-DEVICE-MGT-PLUGIN>/components/iots-plugin` directory via Android Studio, and do the necessary changes you wish to make.
3.  Build the project to create a new APK file that has all the changes you made.

4.  Rename the created `.apk` file to `androidsense.apk`.
5.  Copy the renamed file, and replace it instead of the existing `android-agent.apk`  file that is in the ```
    <CARBON-DEVICE-MGT-PLUGIN>/components/iot-plugins/androidsense-plugin/org.wso2.carbon.device.mgt.iot.androidsense.ui/src/main/resources/jaggeryapps/devicemgt  
    /app/units/cdmf.unit.device.type.android_sense.type-view/public/asset
    ``` directory