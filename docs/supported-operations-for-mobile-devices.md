---
layout: default
title: Supported Operations for Mobile Devices
nav_order: 6

---
# Supported Operations for Mobile Devices
{: .no_toc }
---

[Entgra IoT Server](http://entgra.io/) currently supports iOS, Android, and Windows mobile devices. However, the device configuration features will vary based on the mobile OS. The device configuration features that are available as listed below.

TODO: ADDRESS ISSUE WITH CONFLUENCE MACROS

<!-- <div class="confluence-information-macro confluence-information-macro-information">

<div class="confluence-information-macro-body"> -->

For more information on the available mobile device management policies, see [Available Mobile Device Management Policies](/doc/en/lb2/Available-Mobile-Device-Management-Policies.html).
