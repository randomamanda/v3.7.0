---
layout: default
title: Installing the product
parent: Installation Guide
grand_parent: Using Entgra IoT Server
nav_order: 2
---

# Installing the product
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

# Installing on Linux or OS X

**Before you begin**, [please see our compatibility matrix](https://docs.wso2.com/display/compatibility/Tested+Operating+Systems) to find out if this version of the product is fully tested on Linux or OS X.


Follow the instructions below to install the required applications and the Entgra product on Linux or OS X.

## Install the required applications

1.  Log in to the command line (Terminal on Mac).
2.  Ensure that your system meets the [prerequisites](https://entgra.atlassian.net/wiki/spaces/IoTS360/pages/352781222/System+Requirements). Java Development Kit (JDK) is essential to run the product.  

### Installing Entgra IoTS

1.  If you have not done so already, download the latest version of the product as described in [Downloading the Product](/doc/en/lb2/Downloading-the-Product.html).
2.  Extract the archive file to a dedicated directory for the product, which will hereafter be referred to as `<PRODUCT_HOME>`.

### Setting up JAVA_HOME

You must set your `JAVA_HOME` environment variable to point to the directory where the Java Development Kit (JDK) is installed on the computer.

Environment variables are global system variables accessible by all the processes running under the operating system.


1.  In your home directory, open the `BASHRC` file (`.bash_profile` file  on Mac) using editors such as, `vi`, `emacs`, `pico` or `mcedit`.
2.  Add the following two lines at the bottom of the file, replacing `/usr/java/jdk1.8.0 `with the actual directory where the JDK is installed.  
    Example:

    ```
    On Linux:
    export JAVA_HOME=/usr/java/jdk1.8.0
    export PATH=${JAVA_HOME}/bin:${PATH}

    On OS X:
    export JAVA_HOME=/System/Library/Java/JavaVirtualMachines/1.8.0.jdk/Contents/Home
    ```

3.  Save the file.

If you do not know how to work with text editors in a Linux SSH session, run the following command:

  `cat >> .bashrc`

  Paste the string from the clipboard and press "Ctrl+D."



4.  To verify that the `JAVA_HOME `variable is set correctly, execute the following command:

    ```
    On Linux:
    echo $JAVA_HOME

    On OS X:
    which java

    If the above command gives you a path like /usr/bin/java, then it is a symbolic link to the real location. To get the real location, run the following:
    ls -l `which java`
    ```

### Setting system properties

If you need to set additional system properties when the server starts, you can take the following approaches:

*   **Set the properties from a script** Setting your system properties in the startup script is ideal, because it ensures that you set the properties every time you start the server. To avoid having to modify the script each time you upgrade, the best approach is to create your own startup script that wraps the Entgra startup script and adds the properties you want to set, rather than editing the Entgra startup script directly.
*   **Set the properties from an external registry** If you want to access properties from an external registry, you could create Java code that reads the properties at runtime from that registry. Be sure to store sensitive data such as, username and password to connect to the registry in a properties file, instead of in the Java code and secure the properties file with the [secure vault](https://docs.wso2.org/display/Carbon420/WSO2+Carbon+Secure+Vault).


SUSE Linux

When using SUSE Linux, it ignores `/etc/resolv.conf` and only looks at the `/etc/hosts` file. This means that the server will throw an exception on startup if you have not specified anything besides localhost. To avoid this error, add the following line above `127.0.0.1 localhost` in the `/etc/hosts` file.  
`<ip_address>` `<machine_name> localhost`

---


# Installing on Solaris

**Before you begin**, [please see our compatibility matrix](https://docs.wso2.com/display/compatibility/Tested+Operating+Systems) to find out if this version of the product is fully tested on Solaris.


Follow the instructions below to install the required applications and the product on Solaris.

## Installing the supporting applications

1.  Establish an SSH connection to the Solaris machine or log in on the text console.
2.  Be sure your system meets the [prerequisites](https://entgra.atlassian.net/wiki/spaces/IoTS360/pages/352781222/System+Requirements). Java Development Kit (JDK) is essential to run the product.

## Installing Entgra IoTS

1.  If you have not done so already, download the latest version of the product as described in [downloading the product](/doc/en/lb2/Downloading-the-Product.html).
2.  Extract the archive file to a dedicated directory for the product, which will hereafter be referred to as `<PRODUCT_HOME>`.

## Setting JAVA_HOME

You must set your `JAVA_HOME` environment variable to point to the directory where the Java Development Kit (JDK) is installed on the computer.

Environment variables are global system variables accessible by all the processes running under the operating system.

1.  In your home directory, open the BASHRC file in your favorite text editor, such as vi, emacs, pico or mcedit.
2.  Add the following two lines at the bottom of the file, replacing `/usr/java/jdk1.8.0` with the actual directory where the JDK is installed.

    ```
    export JAVA_HOME=/usr/java/jdk1.8.0
    export PATH=${JAVA_HOME}/bin:${PATH}
    ```

    For example, the file should now look like this:

    ![]({{site.baseurl}}/assets/images/352786884.png)

3.  Save the file.

    If you do not know how to work with text editors in an SSH session, run the following command:

    `cat >> .bashrc`

    Paste the string from the clipboard and press "Ctrl+D."

4.  To verify that the `JAVA_HOME` variable is set correctly, execute the following command: `echo $JAVA_HOME`  
    Example:

    ![]({{site.baseurl}}/assets/images/352786889.png)

    The system returns the JDK installation path.

## Setting system properties

If you need to set additional system properties when the server starts, you can take the following approaches:

*   **Set the properties from a script** Setting your system properties in the startup script is ideal, because it ensures that you set the properties every time you start the server. To avoid having to modify the script each time you upgrade, the best approach is to create your own startup script that wraps the Entgra startup script and adds the properties you want to set, rather than editing the Entgra startup script directly.  

*   **Set the properties from an external registry** If you want to access properties from an external registry, you could create Java code that reads the properties at runtime from that registry. Be sure to store sensitive data such as, username and password to connect to the registry, in a properties file instead of in the Java code and secure the properties file with the [secure vault](https://docs.wso2.org/display/Carbon420/WSO2+Carbon+Secure+Vault).

---

# Installing on Windows

**Before you begin**, [please see our compatibility matrix](https://docs.wso2.com/display/compatibility/Tested+Operating+Systems) to find out if this version of the product is fully tested on Windows.


Follow the instructions below to install Entgra IoTS on Windows.

### Installing the required applications

*   Be sure your system meets the [prerequisites](https://entgra.atlassian.net/wiki/spaces/IoTS360/pages/352781222/System+Requirements) . Java Development Kit (JDK) is essential to run the product.
*   Be sure that the `PATH` environment variable is set to "`C:\Windows\System32`", because the `findstr` windows exe is stored in this path.

### Installing and setting up snappy-java


Why is this needed?


Entgra IoT Server's analytics profile runs on WSO2 Data Analytics Server (DAS) that uses Apache Spark for batch analytics. Therefore, you need to install snappy-java as it is a prerequisite when using Spark in Windows. Else, you will run into a `NoClassDefFoundError` error when starting Entgra IoT Server.


1.  Download the `snappy-java_1.1.1.7.jar` from [here](http://mvnrepository.com/artifact/org.xerial.snappy/snappy-java/1.1.1.7).
2.  Copy the jar to `<IOTS_HOME>\lib` directory.
3.  If Entgra IoT Server is currently running, restart it to apply the changes.

### Installing Entgra IoTS

1.  If you have not done so already, download the latest version of the product as described in [downloading the product](/doc/en/lb2/Downloading-the-Product.html).
2.  Extract the archive file to a dedicated directory for the product, which will hereafter be referred to as `<PRODUCT_HOME>`.

### Setting JAVA_HOME

You must set your `JAVA_HOME` environment variable to point to the directory where the Java Development Kit (JDK) is installed on the computer. Typically, the JDK is installed in the  `C:\Program Files\Java\` directory, such as `C:\Program Files\Java\jdk1.8.0`. If you have multiple versions installed, choose the latest one, which you can find by sorting by date.


Environment variables are global system variables accessible by all the processes running under the operating system. You can define an environment variable as a system variable, which applies to all users, or as a user variable, which applies only to the user who is currently logged in.


You can set `JAVA_HOME` using the system properties, as described below. Alternatively, if you just want to set `JAVA_HOME` temporarily in the current command prompt window, [set it at the command prompt](about:blank#InstallingonWindows-cmd).

#### Setting JAVA_HOME using the System Properties

1.  Right-click the "My Computer" icon on the desktop and choose **Properties**.

    ![]({{site.baseurl}}/assets/images/352786927.png)

2.  In the System Properties window, click the **Advanced** tab, and then click the **Environment Variables **button.

    ![]({{site.baseurl}}/assets/images/352786917.png)

3.  Click the **New **button under "System variables" (for all users) or under "User variables" (just for the user who is currently logged in).

    ![]({{site.baseurl}}/assets/images/352786912.png)

4.  Enter the following information:  

    *   In the "Variable name" field, enter: `JAVA_HOME`
    *   In the "Variable value" field, enter the installation path of the Java Development Kit, such as: `c:\Program Files\Java` `jdk1.8.0`
5.  Click **OK**.

The `JAVA_HOME` variable is now set and will apply to any subsequent command prompt windows you open. If you have existing command prompt windows running, you must close and reopen them for the `JAVA_HOME` variable to take effect, or manually set the `JAVA_HOME` variable in those command prompt windows as described in the next section. To verify that the `JAVA_HOME` variable is set correctly, open a command window (from the **Start** menu, click **Run**, and then type `CMD` and click **Enter**) and execute the following command:

`set JAVA_HOME`

The system returns the JDK installation path.

#### Setting JAVA_HOME temporarily using the Windows command prompt (CMD)

You can temporarily set the `JAVA_HOME` environment variable within a Windows command prompt window (CMD). This is useful when you have an existing command prompt window running and you do not want to restart it.

1.  In the command prompt window, enter the following command where `<JDK_INSTALLATION_PATH>` is the JDK installation directory and press **Enter**:  
    `set JAVA_HOME=<JDK_INSTALLATION_PATH>`

    For example, `set JAVA_HOME=c:\Program Files\java\jdk1.8.0`

    The `JAVA_HOME` variable is now set for the current CMD session only.

2.  To verify that the `JAVA_HOME `variable is set correctly, execute the following command:`set JAVA_HOME`

    The system returns the JDK installation path.

### Setting system properties

If you need to set additional system properties when the server starts, you can take the following approaches:

*   **Set the properties from a script**  
    Setting your system properties in the startup script is ideal, because it ensures that you set the properties every time you start the server. To avoid having to modify the script each time you upgrade, the best approach is to create your own startup script that wraps the Entgra startup script and add the properties you want to set, rather than editing the Entgra startup script directly.  

*   **Set the properties from an external registry** If you want to access properties from an external registry, you could create Java code that reads the properties at runtime from that registry. Be sure to store sensitive data such as, username and password to connect to the registry in a properties file, instead of in the Java code and secure the properties file with the [secure vault](https://docs.wso2.org/display/Carbon420/WSO2+Carbon+Secure+Vault).

---

# Installing as a Linux Service

Follow the sections below to run Entgra IoTS product as a Linux service: 

## Prerequisites

Install Oracle JDK 1.8 and set up the `JAVA_HOME` environment variable. 

## Running the product as a Linux service

1.  To run the product as a service, create a startup script and add it to the boot sequence. The basic structure of the startup script has three parts (i.e., start, stop and restart) as follows:

    ```
    #!/bin/bash

    case “$1″ in
    start)
       echo “Starting the Service”
    ;;
    stop)
       echo “Stopping the Service”
    ;;
    restart)
       echo “Restarting the Service”
    ;;
    *)
       echo $”Usage: $0 {start|stop|restart}”
    exit 1
    esac
    ```

    Given below is a sample startup script for IoT Server Core profile. For broker profile create new script by replacing **iot-server.sh **with **broker.sh** and for analytics create new script by replacing **iot-server.sh **with **analytics.sh**.

    ```
    #! /bin/sh
    export JAVA_HOME="/usr/lib/jvm/jdk1.8.0_191"

    startcmd='<PRODUCT_HOME>/bin/iot-server.sh start > /dev/null &'
    restartcmd='<PRODUCT_HOME>/bin/iot-server.sh restart > /dev/null &'
    stopcmd='<PRODUCT_HOME>/bin/iot-server.sh stop > /dev/null &'

    case "$1" in
    start)
       echo "Starting the Entgra IoT Server ..."
       su -c "${startcmd}" user1
    ;;
    restart)
       echo "Re-starting the Entgra IoT Server ..."
       su -c "${restartcmd}" user1
    ;;
    stop)
       echo "Stopping the Entgra IoT Server ..."
       su -c "${stopcmd}" user1
    ;;
    *)
       echo "Usage: $0 {start|stop|restart}"
    exit 1
    esac
    ```

    In the above script, the server is started as a user by the name user1 rather than the root user. For example, `su -c "${startcmd}" user1 `

2.  Add the script to `/etc/init.d/` directory.

    <div class="confluence-information-macro confluence-information-macro-information">

    <div class="confluence-information-macro-body">

    If you want to keep the scripts in a location other than `/etc/init.d/` folder, you can add a symbolic link to the script in `/etc/init.d/` and keep the actual script in a separate location. Say your script name is prodserver and it is in `/opt/WSO2/` folder, then the commands for adding a link to `/etc/init.d/` is as follows:

    *   Make executable: `sudo chmod a+x /opt/entgra/prodserver`

    *   Add a link to `/etc/init.d/`: `sudo ln -snf /opt/entgra/prodserver /etc/init.d/prodserver`

    </div>

    </div>

3.  Install the startup script to respective runlevels using the [`update-rc.d`](http://manpages.ubuntu.com/manpages/raring/man8/update-rc.d.8.html) command. For example, give the following command for the sample script shown in step1:

    `sudo update-rc.d prodserver defaults `

    The `defaults` option in the above command makes the service to start in runlevels 2,3,4 and 5 and to stop in runlevels 0,1 and 6.

    A **runlevel** is a mode of operation in Linux (or any Unix-style operating system). There are several runlevels in a Linux server and each of these runlevels is represented by a single digit integer. Each runlevel designates a different system configuration and allows access to a different combination of processes.

4.  You can now st art, stop and restart the server using `service <service name> ``{start|stop|restart}`command. You will be prompted for the password of the user (or root) who was used to start the service.

---

# Installing as a Windows Service

Entgra IoTS is a WSO2 Carbon-based product and can be run as a Windows service as described in the official WSO2 Carbon documentation.

[https://docs.wso2.com/display/shared/Installing+as+a+Windows+Service](https://docs.wso2.com/display/shared/Installing+as+a+Windows+Service)
