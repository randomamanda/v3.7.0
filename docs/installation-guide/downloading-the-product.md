---
layout: default
title: Downloading the Product
parent: Installation Guide
grand_parent: Using Entgra IoT Server
nav_order: 1
---
# Downloading the Product

Follow the instructions below to download the product.

1.  [Download Entgra IoT Server](https://storage.googleapis.com/iot-release-public/3.4.0/entgra-iots-3.4.0.zip).

2.  Copy the downloaded file to a preferred location and unzip it. The unzipped file will be called `<IOTS_HOME>` throughout this documentation.

---
***NOTE***

*   The downloaded Entgra IoT Server file is large. Therefore, when unzipping, it might extract halfway through and stop. To avoid this, we recommend that you unzip the file via the terminal.  
    Example:

    `unzip entgra-iots-3.4.0.zip`

*   The maximum character count supported for a file path in the Windows OS is **260**. If this count is exceeded when extracting the pack into a directory, you will get the `Error 0x80010135: Path too long` error. To overcome this issue use the commands given below:
    *   Create a substring and map the current file path to it.  
        In the example given below, the Entgra IoT Server `.zip` file is located in the `C:\Users\Administrator\Downloads\entgra` directory.

        `C:\Users\Administrator\Downloads\entgra>subst Y: C:\Users\Administrator\Downloads\entgra`

    *   Copy the IoT Server Server zip folder to the new path you created and unzip the file there.  
        Example: Unzip the file in the `Y:` drive.

---
