---
layout: default
title: System Requirements
nav_order: 4
---

---
# System Requirements
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}
---

Prior to installing the product, it is necessary to have the appropriate prerequisite software installed on your system. Verify that the computer has the supported operating system and development platforms before starting the installation.

### System requirements

Entgra IoT Server comes as a composition of three components namely broker, core, and analytics. All of these components are WSO2 Carbon based products. Following are the minimum system requirements to run all the IoT components.

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <td>
        <p><strong>Memory</strong></p>
      </td>
      <td>
        <ul>
          <li>~ 8 GB minimum (16 GB is recommended for a production environment.)</li>
          <li>~ 1024 MB heap size. This is generally sufficient to process typical SOAP messages but the requirements vary with larger message sizes and&nbsp;the number of messages processed concurrently.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>Disk</strong></p>
      </td>
      <td>
        <ul>
          <li>~ 5 GB, excluding space allocated for log files, mobile applications, and databases.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td><strong>Processor</strong></td>
      <td>
        <ul>
          <li><span>Quad core processor<br></span></li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

### Environment compatibility

<table>
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <td>
        <p><strong>Operating Systems / Databases</strong></p>
      </td>
      <td>
        <ul>
          <li>All WSO2 Carbon-based products are Java applications that can be run on <strong>any platform that is JDK 8 compliant</strong>. Also, we <strong>do not recommend or support OpenJDK</strong>.</li>
          <li>All WSO2 Carbon-based products are generally compatible with most common DBMSs. The embedded H2 database is suitable for development, testing, and some production environments. For most enterprise production environments, however, we recommend you use an industry-standard RDBMS such as Oracle, PostgreSQL, MySQL, MS SQL, etc.&nbsp;For more information, see&nbsp;Working with Databases. Additionally, we do not recommend the H2 database as a user store.</li>
          <li>It is&nbsp;<strong>not recommended to use Apache DS</strong>&nbsp;in a production environment due to scalability issues. Instead, use an LDAP like OpenLDAP for user management.</li>
          <li>For environments that WSO2 products are tested with, see&nbsp;<a href="https://docs.wso2.com/display/compatibility/Compatibility+of+WSO2+Products" rel="nofollow" class="external-link">Compatibility of WSO2 Products</a>.</li>
          <li>If you have difficulty in setting up any WSO2 product on a specific platform or database,&nbsp;please <a href="http://wso2.com/community" class="external-link" rel="nofollow">contact us</a>.</li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

### Required applications

The following applications are required for running the product and its samples or for building from the source code. Mandatory installs are marked with an asterisk ***** .

<table>
  <colgroup>
    <col>
    <col>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <th>
        <p>Application</p>
      </th>
      <th>
        <p>Purpose</p>
      </th>
      <th>
        <p>Version</p>
      </th>
      <th>Download Links</th>
    </tr>
    <tr>
      <td>
        <p><strong>*&nbsp;Oracle Java SE Development Kit (JDK)</strong></p>
      </td>
      <td>
        <ul>
          <li>To launch the product as each product is a Java application.</li>
          <li>To&nbsp;<a href="https://wso2.github.io/" class="external-link" rel="nofollow">build the product from the source distribution</a> (both JDK and Apache Maven are required).</li>
          <li>To run Apache Ant.</li>
        </ul>
      </td>
      <td>
        <p>JDK 8</p>
        <p>Oracle 1.8 is also supported when running (not building) WSO2 products.</p>
      </td>
      <td>
        <p><a rel="nofollow" href="http://java.sun.com/javase/downloads/index.jsp" class="external-link">http://java.sun.com/javase/downloads/index.jsp</a></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>Apache ActiveMQ</strong><br><strong>JMS Provider</strong></p>
      </td>
      <td>
        <p>To enable the product's <a class="unresolved" href="#">JMS transport</a> and try out JMS samples if any. The ActiveMQ client libraries must be installed in the product's classpath before you can enable the JMS transport.</p>
      </td>
      <td>
        <p>5.5.0 or later<br class="atl-forced-newline"><br class="atl-forced-newline">If you use any other JMS provider (e.g., Apache Qpid), install any necessary libraries and/or components.</p>
      </td>
      <td>
        <p><a href="http://activemq.apache.org/" class="external-link" rel="nofollow">http://activemq.apache.org/</a></p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>Apache Ant</strong></p>
      </td>
      <td>
        <p>To compile and run the product samples.</p>
      </td>
      <td>
        <p>1.7.0 or later</p>
      </td>
      <td>
        <p><a href="http://ant.apache.org/" class="external-link" rel="nofollow">http://ant.apache.org/</a> &nbsp;</p>
      </td>
    </tr>
    <tr>
      <td>
        <p><strong>Apache Maven</strong></p>
      </td>
      <td>
        <p>To&nbsp;<a href="https://wso2.github.io/" class="external-link" rel="nofollow">build the product from the source distribution</a> &nbsp;(both JDK and Apache Maven are required).</p>
        <p>You also need it to deploy the Entgra IoT Server plugins.</p>
        <p>If you are installing by downloading and extracting the binary distribution instead of building from the source code, you do <strong>not</strong> need to install Maven.</p>
      </td>
      <td>
        <p class="confluence-link">3.0.x</p>
      </td>
      <td>
        <p><a href="http://maven.apache.org/" class="external-link" rel="nofollow">http://maven.apache.org/</a> &nbsp;</p>
      </td>
    </tr>
    <tr>
      <td><strong>Git</strong></td>
      <td>
        <p>To&nbsp;checkout the source, in order to <a href="https://wso2.github.io/" class="external-link" rel="nofollow">build the product from the source distribution</a>.</p>
      </td>
      <td>1.8.*</td>
      <td><a href="http://git-scm.com/" class="external-link" rel="nofollow">http://git-scm.com/</a></td>
    </tr>
    <tr>
      <td>
        <p><strong>Web Browser</strong></p>
      </td>
      <td>
        <ul>
          <li>To access the product's <a href="https://entgra.atlassian.net/wiki/spaces/IoTS350/pages/336566421/Running+the+Product" data-linked-resource-id="336566421" data-linked-resource-version="1" data-linked-resource-type="page">Management Console</a>. The Web Browser must be JavaScript enabled to take full advantage of the Management console.</li>
          <li><strong>NOTE:</strong> On Windows Server 2003, you must not go below the medium security level in Internet Explorer 6.x.</li>
        </ul>
      </td>
      <td>
        <p><br></p>
      </td>
      <td><br></td>
    </tr>
  </tbody>
</table>