<!-- <p align="right">
    <a href="https://badge.fury.io/rb/just-the-docs"><img src="https://badge.fury.io/rb/just-the-docs.svg" alt="Gem version"></a> <a href="https://travis-ci.com/pmarsceill/just-the-docs"><img src="https://travis-ci.com/pmarsceill/just-the-docs.svg?branch=master" alt="Build status"></a>
</p>
<br><br> -->
<p align="center">
    <h1 align="center">Entgra Documentation</h1>
    <p align="center">Entgra Documentation has been given a fresh new look! <br>It has been rebuilt and hosted on GitLab Pages with few dependencies.</p>
    <!-- <p align="center"><strong><a href="https://pmarsceill.github.io/just-the-docs/">See it in action!</a></strong></p>
    <br><br><br> -->
</p>
<!-- 
![jtd](https://user-images.githubusercontent.com/896475/47384541-89053c80-d6d5-11e8-98dc-dba16e192de9.gif) -->

<!-- ## Installation

Add this line to your Jekyll site's Gemfile:

```ruby
gem "just-the-docs"
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: just-the-docs
```

And then execute:

    $ bundleThe theme is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

Or install it yourself as:

    $ gem install just-the-docs -->

## Theme specific usage

[View the theme documentation](https://pmarsceill.github.io/just-the-docs/) for usage information.

## Contributing

Issue reports and suggestions follow the same procedure as all other Entgra repositories.
A new Merge Request can be created at the discovery of an error or suggestion and the relevant change pushed accordingly. Contributors are expected to maintain set standards and adhere to the [Entgra Developer]() code of conduct.

All Merge Requests will be vetted and checked prior to being added to the main documentation.

### Submitting code changes:

- Open a Pull Request
- Ensure all CI tests pass
- Await code review
- Merged if accepted

## Development

To set up your environment to develop this theme, run;


```
bundle install
```
 To test changes run the following command;

 ```
bundle exec jekyll serve
```
Open your browser at `http://localhost:4000`. This starts a local Jekyll server that hosts the documentation. Thereafter add pages, documents, data, etc. like normal. As you make modifications to your to the documentation the site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When the theme is released, only the files in `_layouts`, `_includes`, and `_sass` tracked with Git will be released.

## License

The theme used in the documentation is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
